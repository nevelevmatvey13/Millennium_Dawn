add_namespace = RWA



news_event = {
	id = RWA.0
	title = RWA.0.t
	desc = RWA.0.d
	picture = GFX_RWA_startup_2000

	immediate = { log = "[GetDateText]: [Root.GetName]: event RWA.70" }

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.0.a
		log = "[GetDateText]: [This.GetName]: RWA.0.a executed"
		ai_chance = {
			factor = 1
		}
		hidden_effect = {
			country_event = RWA.1
		}
	}
}

news_event = {
	id = RWA.1
	title = RWA.1.t
	desc = RWA.1.d
	picture = GFX_great_african_war


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.1.a
		log = "[GetDateText]: [This.GetName]: RWA.1.a executed"
		ai_chance = {
			factor = 1
		}
		ai_chance = { factor = 100 }
	}
}

country_event = {
	id = RWA.2
	title = RWA.2.t
	desc = RWA.2.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.2.a
		log = "[GetDateText]: [This.GetName]: RWA.2.a executed"
		ai_chance = {
			factor = 1
		}
		random_list = {
			80 = {
				DRC = {
					custom_effect_tooltip = RWA_kabila_assassination_tt
					hidden_effect = {
						meta_effect = {
							text = {
								set_country_flag = [META_SET_RULING_PARTY]
							}
							META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
						}
						set_leader = yes
					}
				}
			}
			20 = {
				DRC = {
					add_war_support = 0.05
				}
			}
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = RWA.2.b
		log = "[GetDateText]: [This.GetName]: RWA.2.b executed"
		ai_chance = { factor = 0 }
		add_political_power = -5
	}

}

country_event = {
	id = RWA.3
	title = RWA.3.t
	desc = RWA.3.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.3.a
		log = "[GetDateText]: [This.GetName]: RWA.3.a executed"
		UGA = {
			country_event = RWA.4
		}
		ai_chance = { factor = 90 }
	}
}

country_event = {
	id = RWA.4
	title = RWA.4.t
	desc = RWA.4.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.4.a
		log = "[GetDateText]: [This.GetName]: RWA.4.a executed"
		RWA = {
			country_event = RWA.5
		}
		ai_chance = { factor = 90 }
	}

	option = {
		name = RWA.4.b
		log = "[GetDateText]: [This.GetName]: RWA.4.b executed"
		ai_chance = { factor = 10 }
	}

}

country_event = {
	id = RWA.5
	title = RWA.5.t
	desc = RWA.5.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.5.a
		log = "[GetDateText]: [This.GetName]: RWA.5.a executed"
		DRC = {
			country_event = RWA.6
		}
		ai_chance = {
			factor = 90
			modifier = {
				factor = 8
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}

	option = {
		name = RWA.5.b
		log = "[GetDateText]: [This.GetName]: RWA.5.b executed"
		DRC = {
			country_event = RWA.7
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 4000
				DRC = {
					NOT = {
						has_full_control_of_state = 303
					}
				}
			}
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}

	option = {
		name = RWA.5.c
		log = "[GetDateText]: [This.GetName]: RWA.5.c executed"
		DRC = {
			country_event = RWA.8
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 4000
				DRC = {
					NOT = {
						has_full_control_of_state = 302
					}
				}
			}
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}
}

country_event = {
	id = RWA.6
	title = RWA.6.t
	desc = RWA.6.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.6.a
		log = "[GetDateText]: [This.GetName]: RWA.6.a executed"
		add_political_power = -100
		add_war_support = -0.20
		white_peace = RCD
		white_peace = RWA
		white_peace = UGA
		white_peace = MLC
		annex_country = {
			target = RCD
		}
		annex_country = {
			target = MLC
		}
		20 = {
			remove_core_of = RCD
			remove_core_of = MLC
			remove_claim_by = UGA
		}
		302 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		303 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		304 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		306 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		307 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		308 = {
			remove_core_of = RCD
			remove_core_of = MLC
			remove_claim_by = RWA
		}
		309 = {
			remove_core_of = RCD
			remove_core_of = MLC
			remove_claim_by = RWA
		}
		310 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		311 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		312 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		313 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		314 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		RWA = {
			add_political_power = 50
			add_war_support = -0.15
		}
		UGA = {
			add_political_power = 50
			add_war_support = -0.15
		}


		ai_chance = { factor = 100 }
	}

	option = {
		name = RWA.6.b
		log = "[GetDateText]: [This.GetName]: RWA.6.b executed"
		RWA = {
			country_event = RWA.15??
		}
		ai_chance = { factor = 0 }
	}

}

country_event = {
	id = RWA.7
	title = RWA.7.t
	desc = RWA.7.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.7.a
		log = "[GetDateText]: [This.GetName]: RWA.7.a executed"
		add_political_power = -150
		add_war_support = -0.10
		add_stability -0.10
		white_peace = RCD
		white_peace = RWA
		white_peace = UGA
		white_peace = MLC
		transfer_state = 311
		transfer_state = 310
		annex_country = {
			target = MLC
		}
		20 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		302 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		303 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		304 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		306 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		307 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		308 = {
			remove_core_of = MLC
			remove_claim_by = RWA
		}
		309 = {
			remove_core_of = MLC
			remove_claim_by = RWA
		}
		310 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		311 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		312 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		313 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		314 = {
			remove_core_of = RCD
			remove_core_of = MLC
		}
		RWA = {
			transfer_state = 308
			add_political_power = 75
			add_war_support = -0.10
		}
		UGA = {
			transfer_state = 20
			add_political_power = 75
			add_war_support = -0.10
		}
		RCD = {
			transfer_state = 309
			set_capital = 309
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = RWA.7.b
		log = "[GetDateText]: [This.GetName]: RWA.7.b executed"
		add_war_support = 0.05
		add_stability = -0.05
		ai_chance = { factor = 0 }
	}
}

country_event = {
	id = RWA.8
	title = RWA.8.t
	desc = RWA.8.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.8.a
		log = "[GetDateText]: [This.GetName]: RWA.8.a executed"
		RWA = {
			country_event = RWA.9
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = RWA.8.b
		log = "[GetDateText]: [This.GetName]: RWA.8.b executed"
		ai_chance = { factor = 0 }
	}

}

country_event = {
	id = RWA.9
	title = RWA.9.t
	desc = RWA.9.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.9.a
		log = "[GetDateText]: [This.GetName]: RWA.9.a executed"
		RWA = {
			add_ideas = 	idea_eac_member_state
			transfer_state = 308
			add_ideas = Western_Sanctions
		}
		UGA = {
			transfer_state = 20
			transfer_state = 309
			add_ideas = Western_Sanctions
		}
		RCD = {
			transfer_state = 307
			transfer_state = 306
			transfer_state = 304
		}
		MLC = {
			transfer_state = 313
			transfer_state = 311
		}
		DRC = {
			add_political_power = -150
			add_war_support = -0.10
			add_stability -0.10
			white_peace = RCD
			white_peace = RWA
			white_peace = UGA
			white_peace = MLC
		}
		ai_chance = { factor = 100 }
	}
}


country_event = {
	id = RWA.12
	title = RWA.12.t
	desc = RWA.12.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.12.a
		log = "[GetDateText]: [This.GetName]: RWA.12.a executed"
		DRC = {
			country_event = RWA.13
		}
		ai_chance = {
			factor = 90
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}

	option = {
		name = RWA.12.b
		log = "[GetDateText]: [This.GetName]: RWA.12.b executed"
		DRC = {
			country_event = RWA.14
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 4000
				DRC = {
					NOT = {
						has_full_control_of_state = 303
					}
				}
			}
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}

	option = {
		name = RWA.12.c
		log = "[GetDateText]: [This.GetName]: RWA.12.c executed"
		DRC = {
			country_event = RWA.15
		}
		ai_chance = {
			factor = 10
			modifier = {
				factor = 4000
				DRC = {
					NOT = {
						has_full_control_of_state = 302
					}
				}
			}
			modifier = {
				factor = 2
				DRC = {
					has_country_leader = { name = "Joseph Kabila" ruling_only = yes }
				}
			}
		}
	}
}

country_event = {
	id = RWA.20
	title = RWA.20.t
	desc = RWA.20.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.20.a
		log = "[GetDateText]: [This.GetName]: RWA.20.a executed"
		RWA = {
			add_ideas = idea_eac_member_state
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = RWA.20.b
		log = "[GetDateText]: [This.GetName]: RWA.20.b executed"
		ai_chance = { factor = 0 }
	}

}

country_event = {
	id = RWA.21
	title = RWA.21.t
	desc = RWA.21.d
	picture = GFX_EAF_event_ken_tnz_uga

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.21.a
		log = "[GetDateText]: [This.GetName]: RWA.21.a executed"
		RWA = {
			add_ideas = commonwealth_of_nations_member
		}
		ai_chance = { factor = 100 }
	}

	option = {
		name = RWA.21.b
		log = "[GetDateText]: [This.GetName]: RWA.21.b executed"
		ai_chance = { factor = 0 }
	}

}

news_event = {
	id = RWA.22
	title = RWA.22.t
	desc = RWA.22.d
	picture = GFX_RWA_startup_2000
	immediate = { log = "[GetDateText]: [Root.GetName]: event RWA." }

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.22.a
		log = "[GetDateText]: [This.GetName]: RWA.22.a executed"
		hidden_effect = {
			country_event = RWA.11
		}
	}
}

country_event = {
	id = RWA.23
	title = RWA.23.t
	desc = RWA.23.d
	picture = GFX_EAF_event_ken_tnz_uga


	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = RWA.23.a
		log = "[GetDateText]: [This.GetName]: RWA.23.a executed"
		RCD = {
			transfer_state = 306
			transfer_state = 307
			set_capital = 306
		}
		DRC = {
			transfer_state = 302
			transfer_state = 303
			transfer_state = 304
			transfer_state = 313
			transfer_state = 314
		}
		MLC = {
			transfer_state = 312
			transfer_state = 311
			transfer_state = 20
			transfer_state = 309
			transfer_state = 310
			transfer_state = 308
		}
		ai_chance = { factor = 100 }
	}
}