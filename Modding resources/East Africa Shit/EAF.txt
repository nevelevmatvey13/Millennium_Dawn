EAF_federation_decisions = {

	EAF_capital_decision = {

		allowed = {
			OR = {
				tag = UGA
				tag = KEN
				tag = TNZ
			}
		}

		available = {
			owns_state = 255
			owns_state = 256
			owns_state = 242
			owns_state = 249
			owns_state = 250
			owns_state = 251
			has_full_control_of_state = 255
			has_full_control_of_state = 256
			has_full_control_of_state = 242
			has_full_control_of_state = 249
			has_full_control_of_state = 250
			has_full_control_of_state = 251
		}

		ai_will_do = {
			factor = 200
		}

		visible = {
			has_full_control_of_state = 256
		}

		fire_only_once = no

		complete_effect = {
			visually_display_opinion_rise_farmers = yes
			set_country_flag = current_farmers
			increase_internal_faction_opinion = yes
			set_cosmetic_tag = EAF_UNIFIED
		}
	}

	EAF_form_east_african_federation_decision = {

		icon = generic_form_nation

		allowed = {
			OR = {
				original_tag = TNZ
				original_tag = KEN
				original_tag = UGA
				original_tag = BUR
				original_tag = RWA
				original_tag = SSU
			}
		}



		visible = {

		}

		complete_effect = {
			set_cosmetic_tag = EAF_UNIFIED
			hidden_effect = {
				set_global_flag = form_east_africa_flag
			}
		}

		ai_will_do = {
			factor = 200
			modifier = {
				factor = 0
				is_historical_focus_on = yes
			}
		}
	}
}

















