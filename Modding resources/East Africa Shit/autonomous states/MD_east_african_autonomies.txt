#For East Africa, but allowed to be used if want to emulate a confederation + federation system
autonomy_state = {
	id = autonomy_confederate_state
	
	default = no
	
	is_puppet = yes
	
	min_freedom_level = 0.0
	
	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = no
	}
	
	modifier = {
		cic_to_overlord_factor = 0
		mic_to_overlord_factor = 0.2
		can_master_build_for_us = 0
		
		autonomy_manpower_share = 0.1
		
		extra_trade_to_overlord_factor = 0.8
		overlord_trade_cost_factor = -0.8

		research_sharing_per_country_bonus_factor = -0.3
		
		#autonomy_gain = 1
	}
	
	ai_subject_wants_higher = {
		factor = 0.0
	}
	
	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		OR = {	
			original_tag = TNZ
			original_tag = KEN
			original_tag = UGA
			original_tag = RWA
			original_tag = BUR
			original_tag = SSU
			original_tag = COM
			original_tag = ZAN
		}
	}
	
	can_take_level = {
		always = no
	}

	can_lose_level = {
		always = no
	}
}

autonomy_state = {
	id = autonomy_federal_state
	
	default = no
	
	is_puppet = yes
	
	min_freedom_level = 0.0
	
	rule = {
		can_not_declare_war = yes
		can_decline_call_to_war = no
		units_deployed_to_overlord = yes
	}
	
	modifier = {
		cic_to_overlord_factor = 0
		mic_to_overlord_factor = 0.8
		can_master_build_for_us = 0
		
		autonomy_manpower_share = 0.8
		
		extra_trade_to_overlord_factor = 0.8
		overlord_trade_cost_factor = -0.8

		research_sharing_per_country_bonus_factor = -0.3
		
		#autonomy_gain = 1
	}
	
	ai_subject_wants_higher = {
		factor = 0.0
	}
	
	ai_overlord_wants_lower = {
		factor = 0.0
	}

	ai_overlord_wants_garrison = {
		always = yes
	}

	allowed = {
		OR = {	
			original_tag = TNZ
			original_tag = KEN
			original_tag = UGA
			original_tag = RWA
			original_tag = BUR
			original_tag = SSU
			original_tag = COM
			original_tag = ZAN
		}
	}
	
	can_take_level = {
		always = no
	}

	can_lose_level = {
		always = no
	}
}