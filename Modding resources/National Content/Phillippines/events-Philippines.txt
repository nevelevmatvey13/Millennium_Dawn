add_namespace = milf
add_namespace = philippines
add_namespace = philippines_news

##Standardized Event Idea Swappers (triggers every week or so)
#Low Stability good
country_event = {
	id = milf.1
	immediate = {
		log = "[GetDateText]: [Root.GetName]: event milf.1"
		remove_ideas = PHI_milf_high_effect
		remove_ideas = PHI_milf_medium_effect
		add_ideas = PHI_milf_low_effect
	}
	mean_time_to_happen = {
		days = 7
	}
	trigger = {
		check_variable = { PHI_strength_of_milf < 20 }
		has_country_flag = phi_milf_exists
	}
	hidden = yes
}
#Medium Stability eh
country_event = {
	id = milf.2
	immediate = {
		log = "[GetDateText]: [Root.GetName]: event milf.2"
		remove_ideas = PHI_milf_high_effect
		remove_ideas = PHI_milf_low_effect
		add_ideas = PHI_milf_medium_effect
	}
	mean_time_to_happen = {
		days = 7
	}

	trigger = {
		AND = {
			check_variable = { PHI_strength_of_milf > 20 }
			check_variable = { PHI_strength_of_milf < 60 }
		}
		has_country_flag = phi_milf_exists
	}
	hidden = yes
}
#High Stability bad
country_event = {
	id = milf.3
	immediate = {
		log = "[GetDateText]: [Root.GetName]: event milf.3"
		remove_ideas = PHI_milf_low_effect
		remove_ideas = PHI_milf_medium_effect
		add_ideas = PHI_milf_high_effect
	}
	mean_time_to_happen = {
		days = 7
	}
	trigger = {
		check_variable = { PHI_strength_of_milf > 60 }
		has_country_flag = phi_milf_exists
	}
	hidden = yes
}
