#Author(s): Angriest_Bird


# temp_change = the change number
# Purpose: PHI_change_strength_of_milf is to change the full strength of the Moro Islamic Liberation Front
PHI_change_strength_of_milf = {
	custom_effect_tooltip = PHI_change_milf_strength_tt
	add_to_variable = { PHI_strength_of_milf = temp_change }
	clamp_variable = {
		min = 0
		max = 100
		var = PHI_strength_of_milf
	}
	PHI_update_the_effects_of_islamic_strength = yes
}

PHI_update_the_effects_of_islamic_strength = {
	if = { limit = { NOT = { has_dynamic_modifier = { modifier = PHI_islamism_dynamic_modifier } } }
		add_dynamic_modifier = { modifier = PHI_islamism_dynamic_modifier }
		set_country_flag = phi_milf_exists
		if = {
			limit = { NOT = { has_variable = PHI_strength_of_milf } }
			set_variable = { var = PHI_strength_of_milf value = 35 }
		}
	}

	set_temp_variable = { opinion_modifier = PHI_strength_of_milf }

	set_variable = { var = PHI_fascism_drift_effect value = opinion_modifier }
	set_variable = { var = PHI_stability_factor_effect value = opinion_modifier }
	set_variable = { var = PHI_political_power_factor_effect value = opinion_modifier }

	multiply_variable = { var = PHI_fascism_drift_effect value = 0.003 }
	multiply_variable = { var = PHI_stability_factor_effect value = -0.004 }
	multiply_variable = { var = PHI_political_power_factor_effect value = -0.004 }

	force_update_dynamic_modifier = yes
}