add_namespace = rojava

###########################
# Rojava Events
###########################

#USAID re-offer
country_event = {
	id = rojava.1

	title = rojava.1.t
	desc = rojava.1.d

	is_triggered_only = yes

	immediate = { log = "[GetDateText]: [Root.GetName]: event rojava.1" }

	trigger = {
		NOT = { has_idea = USA_usaid }
	}

	# yes
	option = {
		name = rojava.1.option.a
		log = "[GetDateText]: [This.GetName]: rojava.1.oA executed"
		add_ideas = USA_usaid
		generic_grant_5_percent_influence_of_USA = yes
	}

	# no
	option = {
		name = rojava.1.option.b
		log = "[GetDateText]: [This.GetName]: rojava.1.oB executed"
	}
}