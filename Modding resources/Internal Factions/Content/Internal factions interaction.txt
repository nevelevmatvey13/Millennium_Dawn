############################################################################################################
#	Internal factions interaction
############################################################################################################

ideas = {
	country = {
		GIFI_mischevous_failure = {
				
			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}
			
			removal_cost = -1

			#picture = WIP

			modifier = {
				drift_defence_factor = -1.0
				political_power_factor = -0.7
				stability_factor = -0.2
			}
		}
	}
}