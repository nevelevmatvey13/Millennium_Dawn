#Exclude self contained cosmetic change
TAG_LIST = {"ALB","ARM","AUS","AZE","BEL","BLR","BOS","BUL","CAT","CRO","CZE","DEN","ENG","EST","FIN",
            "FYR","GER","GRE","GEO",
            "HOL","IRE","LAT","LIT","MLV","NOR","POR","ROM","SWE","SWI","UKR","TUR", "SPR","SLV","SLO","HUN"}
EXCLUDE = "USoE"

for TAG in TAG_LIST:
    print("	#"+TAG)
    print("	"+TAG+"_reset_flag = {")
    print("		icon = GFX_decision_generic_form_nation")
    print("		cost = 0")
    print("		ai_will_do = { factor = 1}")
    print("		allowed = { original_tag = "+TAG+"}")
    print("		visible = {")
    print("			original_tag = "+TAG)
    print("			has_elections = yes")
    print("			NOT = {")
    print("				has_cosmetic_tag = "+TAG)
    print("				has_country_tag = "+EXCLUDE)
    print("				AND = {")
    print("					has_civil_war = yes")
    print("					OR = {")
    print("						NOT = {tag = "+TAG+"}")
    print("						has_cosmetic_tag = "+TAG+"_REB")
    print("						has_cosmetic_tag = "+TAG+"_REB_S")
    print("					}")
    print("				}")
    print("			}")
    print("		}")
    print("		complete_effect = {")
    print('			log = "[GetDateText]: [Root.GetName]: Decision '+TAG+'_reset_flag"')
    print("			set_cosmetic_tag = "+TAG)
    print("			custom_effect_tooltip = tooltip_change_flag")
    print("		}")
    print("	}")
    print("")
          
    print("	"+TAG+"_Autocracy_flag = {")
    print("		icon = GFX_decision_generic_form_nation")
    print("		cost = 0")
    print("		ai_will_do = { factor = 1}")
    print("		allowed = { original_tag = "+TAG+"}")
    print("		visible = {")
    print("			original_tag = "+TAG)
    print("			has_elections = no")
    print("			OR = {")
    print("				has_country_leader_with_trait = western_Western_Autocracy")
    print("				has_country_leader_with_trait = emerging_Autocracy")
    print("				has_country_leader_with_trait = neutrality_Neutral_Autocracy")
    print("				has_country_leader_with_trait = nationalist_Nat_Autocracy")
    print("			}")
    print("			NOT = {")
    print("				has_cosmetic_tag = "+TAG+"_AUTH")
    print("				has_country_tag = "+EXCLUDE)
    print("				AND = {")
    print("					has_civil_war = yes")
    print("					OR = {")
    print("						has_country_leader_with_trait = nationalist_Nat_Autocracy")
    print("						AND = {")
    print("							NOT = {tag = "+TAG+"}")
    print("							"+TAG+" = {")
    print("								OR = {")
    print("									has_country_leader_with_trait = western_Western_Autocracy")
    print("									has_country_leader_with_trait = emerging_Autocracy")
    print("									has_country_leader_with_trait = neutrality_Neutral_Autocracy")
    print("								}")
    print("							}")
    print("						}")
    print("					}")
    print("				}")
    print("			}")
    print("		}")
    print("		complete_effect = {")
    print('			log = "[GetDateText]: [Root.GetName]: Decision '+TAG+'_Autocracy_flag"')
    print("			set_cosmetic_tag = "+TAG+"_AUTH")
    print("			custom_effect_tooltip = tooltip_change_flag")
    print("		}")
    print("	}")
    print("")
	
    print("	"+TAG+"_Special_flag = {")
    print("		icon = GFX_decision_generic_form_nation")
    print("		cost = 0")
    print("		ai_will_do = { factor = 1}")
    print("		allowed = { original_tag = "+TAG+"}")
    print("		visible = {")
    print("			original_tag = "+TAG)
    print("			has_elections = no")
    print("			NOT = {")
    print("				has_cosmetic_tag = "+TAG+"_AUTH_S")
    print("				has_country_tag = "+EXCLUDE)
    print("			}")    
    print("			OR = {")
    print("				has_country_leader_with_trait = salafist_Kingdom")
    print("				has_country_leader_with_trait = nationalist_Monarchist")			
    print("				AND = {")
    print("					OR = {")
    print("						has_country_leader_with_trait = emerging_Communist-State")
    print("						has_country_leader_with_trait = neutrality_Neutral_Communism")
    print("					}")
    print("					OR = {")
    print("						has_civil_war = no")
    print("						AND = {")
    print("							has_civil_war = yes")
    print("							tag = "+TAG)
    print("						}")
    print("					}")
    print("				}")
    print("			}")
    print("		}")
    print("		complete_effect = {")
    print('			log = "[GetDateText]: [Root.GetName]: Decision '+TAG+'_Special_flag"')
    print("			set_cosmetic_tag = "+TAG+"_AUTH_S")
    print("			custom_effect_tooltip = tooltip_change_flag")
    print("		}")
    print("	}")
    print("")
	
    print("	"+TAG+"_More_Special_flag = {")
    print("		icon = GFX_decision_generic_form_nation")
    print("		cost = 0")
    print("		ai_will_do = { factor = 1}")
    print("		allowed = { original_tag = "+TAG+"}")
    print("		visible = {")
    print("			original_tag = "+TAG)
    print("			has_elections = no")
    print("			NOT = {")
    print("				has_cosmetic_tag = "+TAG+"_AUTH_SS")
    print("				has_country_tag = "+EXCLUDE)
    print("			}")
    print("			OR = {")
    print("				has_country_leader_with_trait = nationalist_Nat_Fascism")
    print("				has_country_leader_with_trait = emerging_Vilayat_e_Faqih")
    print("				has_country_leader_with_trait = emerging_Mod_Vilayat_e_Faqih")
    print("				has_country_leader_with_trait = salafist_Caliphate")
    print("			}")
    print("		}")
    print("		complete_effect = {")
    print('			log = "[GetDateText]: [Root.GetName]: Decision '+TAG+'_More_Special_flag"')
    print("			set_cosmetic_tag = "+TAG+"_AUTH_SS")
    print("			custom_effect_tooltip = tooltip_change_flag")
    print("		}")
    print("	}")
    print("")

    print("	"+TAG+"_Rebel_flag = {")
    print("		icon = GFX_decision_generic_form_nation")
    print("		cost = 0")
    print("		ai_will_do = { factor = 1}")
    print("		allowed = { original_tag = "+TAG+"}")
    print("		visible = {")
    print("			original_tag = "+TAG)
    print("			has_civil_war = yes")
    print("			"+TAG+" = { has_cosmetic_tag = "+TAG+" }")	
    print("			NOT = {")
    print("				has_cosmetic_tag = "+TAG+"_REB")
    print("				tag = "+TAG)
    print("				has_country_leader_with_trait = emerging_Communist-State")
    print("				has_country_leader_with_trait = neutrality_Neutral_Communism")
    print("				has_country_leader_with_trait = nationalist_Nat_Autocracy")
    print("				has_country_leader_with_trait = nationalist_Nat_Fascism")
    print("				has_country_leader_with_trait = emerging_Vilayat_e_Faqih")
    print("				has_country_leader_with_trait = emerging_Mod_Vilayat_e_Faqih")
    print("				has_country_leader_with_trait = salafist_Kingdom")
    print("				has_country_leader_with_trait = salafist_Caliphate")
    print("				has_country_leader_with_trait = nationalist_Monarchist")
    print("				has_country_leader_with_trait = western_Western_Autocracy")
    print("				has_country_leader_with_trait = emerging_Autocracy")
    print("				has_country_leader_with_trait = neutrality_Neutral_Autocracy")
    print("			}")
    print("		}")
    print("		complete_effect = {")
    print('			log = "[GetDateText]: [Root.GetName]: Decision '+TAG+'_Rebel_flag"')
    print("			set_cosmetic_tag = "+TAG+"_REB")
    print("			custom_effect_tooltip = tooltip_change_flag")
    print("		}")
    print("	}")
    print("")
    
    print("	"+TAG+"_Special_Rebel_flag = {")
    print("		icon = GFX_decision_generic_form_nation")
    print("		cost = 0")
    print("		ai_will_do = { factor = 1}")
    print("		allowed = { original_tag = "+TAG+"}")
    print("		visible = {")
    print("			original_tag = "+TAG)
    print("			has_civil_war = yes")
    print("			NOT = { has_cosmetic_tag = "+TAG+"_REB_S }")
    print("			OR = {")
    print("				has_country_leader_with_trait = nationalist_Nat_Autocracy")
    print("				AND = {")
    print("					OR = {")
    print("						has_country_leader_with_trait = emerging_Communist-State")
    print("						has_country_leader_with_trait = neutrality_Neutral_Communism")
    print("					}")
    print("					NOT = {tag = "+TAG+"}")
    print("				}")
    print("			}")
    print("		}")
    print("		complete_effect = {")
    print('			log = "[GetDateText]: [Root.GetName]: Decision '+TAG+'_Special_Rebel_flag"')
    print("			set_cosmetic_tag = "+TAG+"_REB_S")
    print("			custom_effect_tooltip = tooltip_change_flag")
    print("		}")
    print("	}")
    print("")
