################################################## #######################
# Hungarian Election of 2006
################################################## #######################
event = {
id = 36001
random = no
country = HUN

name = "EVT_36001_NAME"
desc = "EVT_36001_DESC"
style = 0

trigger = {
government = democratic
NOT = {
ispuppet = HUN
}
}

date = { day = 15 month = may year = 2006 }

action_a = {
name = "Social-liberal Government"
ai_chance = 50
command = { type = headofgovernment which = 36011 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36015 }
command = { type = relation which = USA value = 20 }
command = { type = domestic which = political_left value = 1 }
}
action_b = {
name = "Conservative Government"
ai_chance = 50
command = { type = headofgovernment which = 36012 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36016 }
command = { type = ministerofsecurity which = 36017 }
command = { type = ministerofintelligence which = 36018 }
command = { type = domestic which = political_left value = -1 }
command = { type = relation which = USA value = -20 }
command = { type = domestic which = interventionism value = -1 }
}
}
################################################## #######################
# Hungarian Election of 2010
################################################## #######################
event = {
id = 36002
random = no
country = HUN

name = "EVT_36002_NAME"
desc = "EVT_36002_DESC"
style = 0

trigger = {
government = democratic
NOT = {
ispuppet = HUN
}
}

date = { day = 15 month = may year = 2010 }

action_a = {
name = "Social-liberal Government"
ai_chance = 50
command = { type = headofgovernment which = 36011 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36015 }
command = { type = ministerofsecurity which = 36005 }
command = { type = ministerofintelligence which = 36006 }
command = { type = relation which = USA value = 10 }
command = { type = domestic which = political_left value = 1 }
command = { type = domestic which = interventionism value = 1 }
}
action_b = {
name = "Conservative Government"
ai_chance = 50
command = { type = headofgovernment which = 36012 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36016 }
command = { type = ministerofsecurity which = 36017 }
command = { type = ministerofintelligence which = 36018 }
command = { type = domestic which = political_left value = -1 }
command = { type = relation which = USA value = -10 }
command = { type = domestic which = interventionism value = -1 }
}
}
################################################## #######################
# Hungarian Election of 2014
################################################## #######################
event = {
id = 36003
random = no
country = HUN

name = "EVT_36003_NAME"
desc = "EVT_36003_DESC"
style = 0

trigger = {
government = democratic
NOT = {
ispuppet = HUN
}
}

date = { day = 15 month = may year = 2014 }

action_a = {
name = "Social-liberal Government"
ai_chance = 50
command = { type = headofgovernment which = 36011 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36015 }
command = { type = ministerofsecurity which = 36005 }
command = { type = ministerofintelligence which = 36006 }
command = { type = relation which = USA value = 10 }
command = { type = domestic which = political_left value = 1 }
command = { type = domestic which = interventionism value = 1 }
}
action_b = {
name = "Conservative Government"
ai_chance = 50
command = { type = headofgovernment which = 36012 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36016 }
command = { type = ministerofsecurity which = 36017 }
command = { type = ministerofintelligence which = 36018 }
command = { type = domestic which = political_left value = -1 }
command = { type = relation which = USA value = -10 }
command = { type = domestic which = interventionism value = -1 }
}
}
################################################## #######################
# Hungarian Election of 2018
################################################## #######################
event = {
id = 36004
random = no
country = HUN

name = "EVT_36004_NAME"
desc = "EVT_36004_DESC"
style = 0

trigger = {
government = democratic
NOT = {
ispuppet = HUN
}
}

date = { day = 15 month = may year = 2018 }

action_a = {
name = "Social-liberal Government"
ai_chance = 50
command = { type = headofgovernment which = 36011 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36015 }
command = { type = ministerofsecurity which = 36005 }
command = { type = ministerofintelligence which = 36006 }
command = { type = relation which = USA value = 10 }
command = { type = domestic which = political_left value = 1 }
command = { type = domestic which = interventionism value = 1 }
}
action_b = {
name = "Conservative Government"
ai_chance = 50
command = { type = headofgovernment which = 36012 }
command = { type = foreignminister which = 36013 }
command = { type = armamentminister which = 36016 }
command = { type = ministerofsecurity which = 36017 }
command = { type = ministerofintelligence which = 36018 }
command = { type = domestic which = political_left value = -1 }
command = { type = relation which = USA value = -10 }
command = { type = domestic which = interventionism value = -1 }
}
}
##############################
##Head of Government resigns##
##############################
event = {
id = 36005
random = no
country = HUN

name = "EVT_36005_NAME"
desc = "EVT_36005_DESC"
style = 0

trigger = {
government = democratic
NOT = {
ispuppet = HUN
}
}

date = { day = 1 month = september year = 2004 }

action_a = {
name = "Let him resign - New Prime Minister"
ai_chance = 75
command = { type = headofgovernment which = 36011 }
command = { type = dissent value = 1 }
}
action_b = {
name = "Change The Minister"
ai_chance = 25
command = { type = armamentminister which = 36015 }
command = { type = dissent value = 1 }
}
}
#########################
##Head of State Changes##
#########################
event = {
id = 36006
random = no
country = HUN

name = "EVT_36006_NAME"
desc = "EVT_36006_DESC"
style = 0

trigger = {
government = democratic
NOT = {
ispuppet = HUN
}
}

date = { day = 10 month = june year = 2005 }

action_a = {
name = "Elect the social-party candidate"
ai_chance = 40
command = { type = headofstate which = 36010 }

}
action_b = {
name = "Elect the Independent Candidate"
ai_chance = 60
command = { type = headofstate which = 36026 }

}
}