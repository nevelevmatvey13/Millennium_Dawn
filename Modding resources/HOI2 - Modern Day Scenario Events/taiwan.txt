#########################################################
#							#
# Events for Republic of China/Taiwan (TRA)             #
#                                			#
#########################################################

#########
# Index #
#########################################################
#From	- To	#	Name 			 
#########################################################
#80501 - 80800# Main events
#80801 - 80950# Political events
#80951 - 81000# Election events	 		
#########################################################

############################################################################################################################

#############################################
###### Main events
#############################################

#############################################
###### PRC Anti-Secession Law
#############################################
event = {
         id = 80501
         random = no
         country = CHC
         trigger = {
                   government = communist
		   atwar = no
                   exists = TRA

         }
 
         name = "EVT_80501_NAME"
         desc = "EVT_80501_DESC"
         style = 0
	 picture = "taiwan"
 
         date = { day = 14 month = march year = 2005 }
 
           action_a = {
                  name = "Ratify the law"
                  ai_chance = 100
		  command = { type = domestic which = interventionism value = 1 }
		  command = { type = domestic which = defense_lobby value = 1 }
		  command = { type = belligerence value = 1 }
		  command = { type = relation which = TRA value = -10 }
		  command = { type = relation which = USA value = -5 }
		  command = { type = trigger which = 80502 }
		  command = { type = trigger which = 80503 }
           }
           action_b = {
                  name = "We don't need this law"
                  ai_chance = 0
		  command = { type = dissent value = 10 }
		  command = { type = domestic which = interventionism value = -1 }
           }

}
#############################################
###### PRC Anti-Secession Law
#############################################
event = {
         id = 80502
         random = no
         country = TRA
 
         name = "EVT_80502_NAME"
         desc = "EVT_80502_DESC"
         style = 0
	 picture = "chc"
 
           action_a = {
                  name = "They wouldn't dare to attack us"
		  command = { type = relation which = CHC value = -10 }
           }

}
#############################################
###### PRC Anti-Secession Law
#############################################
event = {
         id = 80503
         random = no
         country = USA
 
         name = "EVT_80503_NAME"
         desc = "EVT_80503_DESC"
         style = 0
	 picture = "chc"
 
           action_a = {
                  name = "They wouldn't dare to attack Taiwan"
		  command = { type = relation which = CHC value = -5 }
           }

}
#############################################
###### Taiwan independence protests
#############################################
event = {
         id = 80504
         random = no
         country = TRA
         trigger = {
                   government = democratic
		   atwar = no
                   exists = CHC

         }
 
         name = "EVT_80504_NAME"
         desc = "EVT_80504_DESC"
         style = 0
	 picture = "taiwanprotest1"
 
         date = { day = 15 month = september year = 2007 }
 
           action_a = {
                  name = "Ok"
		  command = { type = dissent value = 2 }
           }

}
#############################################
###### Frank Hsieh announces Taiwan independence
#############################################
event = {
         id = 80505
         random = no
         country = TRA
 
         name = "EVT_80505_NAME"
         desc = "EVT_80505_DESC"
         style = 0
	 picture = "taiwanprotest1"
 
           action_a = {
                  name = "We will be independent"
		  command = { type = dissent value = -1 }
		  command = { type = belligerence value = 1 }
		  command = { type = trigger which = 80506 }
		  command = { type = trigger which = 80507 }
           }

}
#############################################
###### Frank Hsieh announces Taiwan independence
#############################################
event = {
         id = 80506
         random = no
         country = CHC
 
         name = "EVT_80506_NAME"
         desc = "EVT_80506_DESC"
         style = 0
	 picture = "taiwanprotest1"
 
           action_a = {
                  name = "We will not allow this"
		  command = { type = dissent value = 3 }
		  command = { type = relation which = TRA value = -20 }
		  command = { type = relation which = USA value = -5 }
           }

}
#############################################
###### Frank Hsieh announces Taiwan independence
#############################################
event = {
         id = 80507
         random = no
         country = USA
 
         name = "EVT_80507_NAME"
         desc = "EVT_80507_DESC"
         style = 0
	 picture = "taiwanprotest1"
 
           action_a = {
                  name = "This is calling for trouble"
		  command = { type = relation which = TRA value = -10 }
           }

}
#############################################
###### Weapons deal with Taiwan
#############################################
event = {
         id = 80508
         random = no
         country = USA
         trigger = {
                   government = democratic
		   atwar = no
                   exists = CHC
		   event = 80507

         }
 
         name = "EVT_80508_NAME"
         desc = "EVT_80508_DESC"
         style = 0
	 picture = "taiwanship"
 
         date = { day = 7 month = june year = 2008 }
 
           action_a = {
                  name = "Approve weapons sales to Taiwan"
		  ai_chance = 90
		  command = { type = dissent value = 2 }
		  command = { type = belligerence value = 1 }
		  command = { type = trigger which = 80509 }
		  command = { type = trigger which = 80510 }
		  command = { type = supplies value = -2000 }
		  command = { type = money value = 600 }
           }
	   action_b = {
                  name = "We will not sell weapons to Taiwan"
		  ai_chance = 10
		  command = { type = dissent value = 2 }
		  command = { type = relation which = CHC value = 5 }
		  command = { type = relation which = TRA value = -10 }
           }

}
#############################################
###### US weapons sale
#############################################
event = {
         id = 80509
         random = no
         country = TRA
 
         name = "EVT_80509_NAME"
         desc = "EVT_80509_DESC"
         style = 0
	 picture = "taiwanship"
 
           action_a = {
                  name = "Good"
		  command = { type = dissent value = -1 }
		  command = { type = money value = -600 }
		  command = { type = domestic which = defense_lobby value = 1 }
		  command = { type = relation which = USA value = 10 }
		  command = { type = add_division value = heavy_armor when = 2 }
		  command = { type = supplies value = 1000 }
		  command = { type = gain_tech which = 4640 }
		  command = { type = gain_tech which = 4650 }
		  command = { type = gain_tech which = 4660 }
		  command = { type = gain_tech which = 4670 }
		  command = { type = gain_tech which = 4680 }
		  command = { type = gain_tech which = 4700 }
		  command = { type = gain_tech which = 4710 }
		  command = { type = gain_tech which = 4720 }
		  command = { type = gain_tech which = 4730 }
		  command = { type = gain_tech which = 4750 }
		  command = { type = gain_tech which = 4760 }
		  command = { type = gain_tech which = 4770 }
		  command = { type = gain_tech which = 4780 }
		  command = { type = gain_tech which = 4800 }
		  command = { type = gain_tech which = 4810 }
		  command = { type = gain_tech which = 4820 }
		  command = { type = gain_tech which = 4830 }
           }

}
#############################################
###### US weapons sale
#############################################
event = {
         id = 80510
         random = no
         country = CHC
 
         name = "EVT_80510_NAME"
         desc = "EVT_80510_DESC"
         style = 0
	 picture = "taiwanship"
 
           action_a = {
                  name = "How dare they"
		  command = { type = dissent value = 1 }
		  command = { type = relation which = USA value = -10 }
		  command = { type = relation which = TRA value = -10 }
           }

}
#############################################
###### Taiwan independence
#############################################
event = {
         id = 80511
         random = no
         country = TRA
         trigger = {
                   government = democratic
		   headofstate = 80514
		   event = 80505
		   atwar = no
		   exists = CHC

         }
 
         name = "EVT_80511_NAME"
         desc = "EVT_80511_DESC"
         style = 0
	 picture = "taiwanprotest2"
 
         date = { day = 11 month = december year = 2008 }
 
           action_a = {
                  name = "Declare independence"
		  ai_chance = 100
		  command = { type = dissent value = -5 }
		  command = { type = setflag which = warflag8 }
		  command = { type = belligerence value = 3 }
		  command = { type = relation which = CHC value = -200 }
		  command = { type = relation which = USA value = -20 }
		  command = { type = trigger which = 80512 }
		  command = { type = trigger which = 80513 }
		  command = { type = trigger which = 80514 }
		  command = { type = trigger which = 80515 }
           }
	   action_b = {
                  name = "One China"
		  ai_chance = 0
		  command = { type = dissent value = 5 }
		  command = { type = belligerence value = -1 }
		  command = { type = relation which = CHC value = 20 }
		  command = { type = relation which = USA value = 20 }
           }

}
#############################################
###### Taiwan independence
#############################################
event = {
         id = 80512
         random = no
         country = CHC
 
         name = "EVT_80512_NAME"
         desc = "EVT_80512_DESC"
         style = 0
	 picture = "taiwanprotest2"
 
           action_a = {
                  name = "Bastards!"
		  command = { type = dissent value = 1 }
		  command = { type = relation which = TRA value = -200 }
           }

}
#############################################
###### Taiwan independence
#############################################
event = {
         id = 80513
         random = no
         country = RUS
 
         name = "EVT_80513_NAME"
         desc = "EVT_80513_DESC"
         style = 0
	 picture = "taiwanprotest2"
 
           action_a = {
                  name = "Bastards!"
		  command = { type = relation which = TRA value = -50 }
           }

}
#############################################
###### Taiwan independence
#############################################
event = {
         id = 80514
         random = no
         country = USA
 
         name = "EVT_80514_NAME"
         desc = "EVT_80514_DESC"
         style = 0
	 picture = "taiwanprotest2"
 
           action_a = {
                  name = "We must prepare for war"
		  command = { type = ai_prepare_war which = CHC }
           }

}
#############################################
###### Taiwan independence
#############################################
event = {
         id = 80515
         random = no
         country = JAP
 
         name = "EVT_80515_NAME"
         desc = "EVT_80515_DESC"
         style = 0
	 picture = "taiwanprotest2"
 
           action_a = {
                  name = "We must prepare for war"
		  command = { type = ai_prepare_war which = CHC }
           }

}
#############################################
###### Taiwan independence: options
#############################################
event = {
         id = 80516
         random = no
         country = CHC
	 trigger = {
                   government = communist
		   event = 80512
		   atwar = no
		   exists = TRA
                        OR = {   AND = { 
		                        flag = warflag2
		                        flag = warflag3
		                        flag = warflag5
		                        flag = warflag6
		                        flag = warflag7
		                        flag = warflag8        
                                      }
                                  AND = { 
		                        flag = warflag1
		                        flag = warflag3
		                        flag = warflag5
		                        flag = warflag6
		                        flag = warflag7
		                        flag = warflag8        
                                      }
                                  AND = { 
		                        flag = warflag1
		                        flag = warflag2
		                        flag = warflag5
		                        flag = warflag6
		                        flag = warflag7
		                        flag = warflag8        
                                      }
                                  AND = { 
		                        flag = warflag1
		                        flag = warflag2
		                        flag = warflag3
		                        flag = warflag6
		                        flag = warflag7
		                        flag = warflag8        
                                      }
                                  AND = { 
		                        flag = warflag1
		                        flag = warflag2
		                        flag = warflag3
		                        flag = warflag5
		                        flag = warflag7
		                        flag = warflag8        
                                      }
                                  AND = { 
		                        flag = warflag1
		                        flag = warflag2
		                        flag = warflag3
		                        flag = warflag5
		                        flag = warflag6
		                        flag = warflag8        
                                      }
                                  AND = { 
		                        flag = warflag1
		                        flag = warflag2
		                        flag = warflag3
		                        flag = warflag5
		                        flag = warflag6
		                        flag = warflag7      
                                      } }
         }
 
         name = "EVT_80516_NAME"
         desc = "EVT_80516_DESC"
         style = 0
	 picture = "china_generals"

	 date = { day = 14 month = december year = 2008 }
 
           action_a = {
                  name = "Swift military action is the only option"
		  ai_chance = 100
		  command = { type = domestic which = interventionism value = 1 }
		  command = { type = domestic which = defense_lobby value = 1 }
		  command = { type = manpowerpool value = 4000 }
		  command = { type = domestic which = professional_army value = -3 }
		  command = { type = ai_prepare_war which = TRA }
		  command = { type = ai_prepare_war which = KOR }
		  command = { type = ai_prepare_war which = JAP }
		  command = { type = ai_prepare_war which = PHI }
		  command = { type = sleepevent which = 80800 } #alternate event
		  command = { type = setflag which = swift1 }
           }
	   action_b = {
                  name = "End this once and for all: we recognize Taiwan"
		  ai_chance = 0
		  command = { type = domestic which = interventionism value = -3 }
		  command = { type = domestic which = defense_lobby value = -3 }
		  command = { type = dissent value = 30 }
		  command = { type = domestic which = democratic value = 2 }
		  command = { type = relation which = TRA value = 200 }
		  command = { type = relation which = USA value = 50 }
		  command = { type = relation which = JAP value = 20 }
		  command = { type = sleepevent which = 80800 } #alternate event
		  command = { type = sleepevent which = 80517 } #Russian response
		  command = { type = sleepevent which = 80518 } #North Korean response
		  command = { type = sleepevent which = 80521 } #invasion of Taiwan
           }
	   action_c = {
                  name = "Postpone our plans"
		  ai_chance = 0
		  command = { type = dissent value = 1 }
		  command = { type = relation which = TRA value = 5 }
		  command = { type = relation which = USA value = 5 }
		  command = { type = relation which = JAP value = 5 }
		  command = { type = sleepevent which = 80517 } #Russian response
		  command = { type = sleepevent which = 80518 } #North Korean response
		  command = { type = sleepevent which = 80518 } #invasion of Taiwan
           }

}
#############################################
###### Chinese plan of action
#############################################
event = {
         id = 80517
         random = no
         country = RUS
	 trigger = {
		   OR = {
                   	headofstate = 69089 #Putin or Medvedev in office
		   	headofstate = 69001
			government = communist
		   }
		   OR = {
		   	flag = swift1
		   	flag = swift2
		   }
		   atwar = no
		   alliance = { country = RUS country = CHC }		   

         }
 
         name = "EVT_80517_NAME"
         desc = "EVT_80517_DESC"
         style = 0
	 picture = "china_generals"

	 date = { day = 18 month = december year = 2008 }
 
           action_a = {
                  name = "We agree with the plan"
		  ai_chance = 100
		  command = { type = sleepevent which = 80522 } #ahistoric, US has a choice
           }
	   action_b = {
                  name = "We will not be drawn into another war"
		  ai_chance = 0
		  command = { type = domestic which = interventionism value = -2 }
		  command = { type = domestic which = defense_lobby value = -1 }
		  command = { type = dissent value = 5 }
		  command = { type = relation which = CHC value = -100 }
		  command = { type = leave_alliance }
		  command = { type = end_access which = CHC }
                  command = { type = end_guarantee which = RUS where = CHC }
		  command = { type = sleepevent which = 69043 } #Russian invasion of Europe
		  command = { type = sleepevent which = 80529 } #historic, US no choice
		  command = { type = trigger which = 80519 }
           }

}
#############################################
###### Chinese plan of action
#############################################
event = {
         id = 80518
         random = no
         country = U09
	 trigger = {
		   OR = {
		   	flag = swift1
		   	flag = swift2
		   }
		   atwar = no	
		   alliance = { country = u09 country = CHC }	   

         }
 
         name = "EVT_80518_NAME"
         desc = "EVT_80518_DESC"
         style = 0
	 picture = "china_generals"

	 date = { day = 18 month = december year = 2008 }
 
           action_a = {
                  name = "We agree with the plan"
		  ai_chance = 100
		  command = { }
           }
	   action_b = {
                  name = "We will not be drawn into another war"
		  ai_chance = 0
		  command = { type = domestic which = interventionism value = -2 }
		  command = { type = domestic which = defense_lobby value = -1 }
		  command = { type = dissent value = 5 }
		  command = { type = relation which = CHC value = -100 }
		  command = { type = leave_alliance }
		  command = { type = end_access which = CHC }
                  command = { type = end_guarantee which = U09 where = CHC }
		  command = { type = trigger which = 80520 }
           }

}
#############################################
###### Russia rejects our plan
#############################################
event = {
         id = 80519
         random = no
         country = CHC
 
         name = "EVT_80519_NAME"
         desc = "EVT_80519_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "We will do it wihout them"
		  command = { type = end_access which = RUS }
                  command = { type = end_guarantee which = CHC where = RUS }
           }

}
#############################################
###### North Korea rejects our plan
#############################################
event = {
         id = 80520
         random = no
         country = CHC
 
         name = "EVT_80520_NAME"
         desc = "EVT_80520_DESC"
         style = 0
	 picture = "no"
 
           action_a = {
                  name = "We will do it wihout them"
		  command = { type = end_access which = U09 }
                  command = { type = end_guarantee which = CHC where = U09 }
           }

}
#############################################
###### WW3: invasion of Taiwan begins
#############################################
event = {
         id = 80521
         random = no
         country = CHC
         trigger = {
		   event = 80516
		   atwar = no
		   exists = TRA

         }
 
         name = "EVT_80521_NAME"
         desc = "EVT_80521_DESC"
         style = 0
	 picture = "taiwanprotest2"
 
         date = { day = 7 month = may year = 2009 }
 
           action_a = {
                  name = "Begin the invasion!"
		  command = { type = war which = TRA }
		  command = { type = trigger which = 69043 } # Russian invasion of Europe
           }

}
#############################################
###### WW3: invasion of Taiwan begins (ahistoric - no Russian involvement, US has a choice)
#############################################
event = {
         id = 80522
         random = no
         country = USA
	 trigger = {
                   government = democratic
		   war = { country = CHC country = TRA } #China and Taiwan at war
		   NOT = {
		   	alliance = { country = USA country = RUS } #Russia and China are not allied
		   	war = { country = U06 country = RUS } #Russia isn't at war against Europe
		 	war = { country = USA country = RUS } #no US-Russian war
		   }

         }
 
         name = "EVT_80522_NAME"
         desc = "EVT_80522_DESC"
         style = 0
	 picture = "invasionoftaiwan"

	 date = { day = 1 month = january year = 2004 }
	 offset = 1
	 deathdate = { day = 30 month = december year = 2019 }
 
           action_a = {
                  name = "We must defend Taiwan, this means war"
		  ai_chance = 85
		  command = { type = dissent value = 1 }
		  command = { type = alliance which = TRA }
		  command = { type = trigger which = 80523 }
		  command = { type = trigger which = 80524 }
		  command = { type = trigger which = 80525 }
		  command = { type = trigger which = 80526 }
           }
	   action_b = {
                  name = "This is not our war"
		  ai_chance = 15
		  command = { type = dissent value = 10 }
		  command = { type = domestic which = interventionism value = -2 }
		  command = { type = domestic which = defense_lobby value = -2 }
		  command = { type = relation which = TRA value = -50 }
		  command = { type = relation which = JAP value = -50 }
		  command = { type = relation which = PHI value = -50 }
		  command = { type = relation which = KOR value = -50 }
		  command = { type = trigger which = 80527 }
		  command = { type = trigger which = 80528 }
           }

}
#############################################
###### US defends Taiwan
#############################################
event = {
         id = 80523
         random = no
         country = TRA
 
         name = "EVT_80523_NAME"
         desc = "EVT_80523_DESC"
         style = 0
	 picture = "taiwantanks"
 
           action_a = {
                  name = "Fortune will grant us victory"
		  command = { type = dissent value = 1 }
           }

}
#############################################
###### US defends Taiwan
#############################################
event = {
         id = 80524
         random = no
         country = CHC
 
         name = "EVT_80524_NAME"
         desc = "EVT_80524_DESC"
         style = 0
	 picture = "taiwantanks"
 
           action_a = {
                  name = "Fortune will grant us victory"
		  command = { }
           }

}
#############################################
###### Invasion of Taiwan begins
#############################################
event = {
         id = 80525
         random = no
         country = JAP
 
         name = "EVT_80525_NAME"
         desc = "EVT_80525_DESC"
         style = 0
	 picture = "taiwantanks"
 
           action_a = {
                  name = "Soon it will spread at us"
		  command = { }
           }

}
#############################################
###### Invasion of Taiwan begins
#############################################
event = {
         id = 80526
         random = no
         country = PHI
 
         name = "EVT_80526_NAME"
         desc = "EVT_80526_DESC"
         style = 0
	 picture = "taiwantanks"
 
           action_a = {
                  name = "Soon it will spread at us"
		  command = { }
           }

}
#############################################
###### US refuses to defend us!
#############################################
event = {
         id = 80527
         random = no
         country = TRA
 
         name = "EVT_80527_NAME"
         desc = "EVT_80527_DESC"
         style = 0
	 picture = "taiwantanks"
 
           action_a = {
                  name = "Bastards"
		  command = { type = relation which = USA value = -50 }
		  command = { type = dissent value = 3 }
           }

}
#############################################
###### US stays neutral
#############################################
event = {
         id = 80528
         random = no
         country = CHC
 
         name = "EVT_80528_NAME"
         desc = "EVT_80528_DESC"
         style = 0
	 picture = "taiwantanks"
 
           action_a = {
                  name = "Fortune will grant us victory!"
		  command = { type = dissent value = -1 }
           }

}
#############################################
###### WW3: invasion of Taiwan begins (historic WW3 - US has no choice)
#############################################
event = {
         id = 80529
         random = no
         country = USA
	 trigger = {
                   government = democratic
		   war = { country = CHC country = TRA } #China and Taiwan at war
		   alliance = { country = USA country = RUS } #Russia and China allied
		   war = { country = U06 country = RUS } #Russia attacking Europe

         }
 
         name = "EVT_80522_NAME"
         desc = "EVT_80522_DESC"
         style = 0
	 picture = "invasionoftaiwan"

	 date = { day = 1 month = january year = 2004 }
	 offset = 1
	 deathdate = { day = 30 month = december year = 2019 }
 
           action_a = {
                  name = "We must defend Taiwan, this means war!"
		  command = { type = dissent value = 2 } # unpopular move, average J.Doe doesn't care about Taiwan
		  command = { type = alliance which = TRA }
		  command = { type = trigger which = 80523 }
		  command = { type = trigger which = 80524 }
		  command = { type = trigger which = 80525 }
		  command = { type = trigger which = 80526 }
           }

}
###############################################################################
###### Taiwan independence: options (alternate - insufficient tension build-up)
###############################################################################
event = {
         id = 80800
         random = no
         country = CHC
	 trigger = {
                   government = communist
		   event = 80512
		   atwar = no
		   exists = TRA

         }
 
         name = "EVT_80800_NAME"
         desc = "EVT_80800_DESC"
         style = 0
	 picture = "china_generals"

	 date = { day = 16 month = december year = 2008 }
 
           action_a = {
                  name = "Swift military action is the only option"
		  ai_chance = 20
		  command = { type = domestic which = interventionism value = 1 }
		  command = { type = domestic which = defense_lobby value = 1 }
		  command = { type = manpowerpool value = 4000 }
		  command = { type = domestic which = professional_army value = -3 }
		  command = { type = ai_prepare_war which = TRA }
		  command = { type = ai_prepare_war which = KOR }
		  command = { type = ai_prepare_war which = JAP }
		  command = { type = ai_prepare_war which = PHI }
		  command = { type = setflag which = swift2 }
           }
	   action_b = {
                  name = "End this once and for all: we recognize Taiwan"
		  ai_chance = 0
		  command = { type = domestic which = interventionism value = -3 }
		  command = { type = domestic which = defense_lobby value = -3 }
		  command = { type = dissent value = 30 }
		  command = { type = domestic which = democratic value = 2 }
		  command = { type = relation which = TRA value = 200 }
		  command = { type = relation which = USA value = 50 }
		  command = { type = relation which = JAP value = 20 }
		  command = { type = sleepevent which = 80517 } #Russian response
		  command = { type = sleepevent which = 80518 } #North Korean response
		  command = { type = sleepevent which = 80521 } #invasion of Taiwan
           }
	   action_c = {
                  name = "Postpone our plans"
		  ai_chance = 80
		  command = { type = dissent value = 1 }
		  command = { type = relation which = TRA value = 5 }
		  command = { type = relation which = USA value = 5 }
		  command = { type = relation which = JAP value = 5 }
		  command = { type = sleepevent which = 80517 } #Russian response
		  command = { type = sleepevent which = 80518 } #North Korean response
		  command = { type = sleepevent which = 80521 } #invasion of Taiwan
           }

}



#############################################
###### Election events
#############################################

#############################################
###### Taiwan elections 2004
#############################################
event = {
         id = 80951
         random = no
         country = TRA
         trigger = {
                   government = democratic
		   atwar = no
                   NOT = { 
                            ispuppet = TRA
                   }

         }
 
         name = "EVT_80951_NAME"
         desc = "EVT_80951_DESC"
         style = 0
	 picture = "elections"
 
         date = { day = 20 month = march year = 2004 }
 
           action_a = {
                  name = "Chen Shui-bian/DPP"
                  ai_chance = 80
		  command = { type = headofstate which = 80501 }
                  command = { type = headofgovernment which = 80502 }
                  command = { type = armamentminister which = 80516 }
                  command = { type = foreignminister which = 80533 }
		  command = { type = ministerofsecurity which = 80505 }
		  command = { type = set_domestic which = political_left value = 6 }
		  command = { type = set_domestic which = freedom value = 8 }
		  command = { type = set_domestic which = free_market value = 7 }
           }
           action_b = {
                  name = "Lien Chan/Kuomintang"
                  ai_chance = 20
		  command = { type = headofstate which = 80511 }
		  command = { type = headofgovernment which = 80532 }
		  command = { type = armamentminister which = 80517 }
		  command = { type = foreignminister which = 80535 }
		  command = { type = ministerofsecurity which = 80520 }
		  command = { type = set_domestic which = political_left value = 3 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 8 }
           }
	   action_c = {
                  name = "Keep current"
                  ai_chance = 0
		  command = { type = dissent value = -1 }
           }

}
#############################################
###### Taiwan elections 2008
#############################################
event = {
         id = 80952
         random = no
         country = TRA
         trigger = {
                   government = democratic
		   atwar = no
                   NOT = { 
                            ispuppet = TRA
                   }

         }
 
         name = "EVT_80952_NAME"
         desc = "EVT_80952_DESC"
         style = 0
	 picture = "elections"
 
         date = { day = 22 month = march year = 2008 }
 
           action_a = {
                  name = "Frank Hsieh/DPP"
                  ai_chance = 20
		  command = { type = headofstate which = 80514 }
                  command = { type = headofgovernment which = 80531 }
                  command = { type = armamentminister which = 80516 }
                  command = { type = foreignminister which = 80533 }
		  command = { type = ministerofsecurity which = 80505 }
		  command = { type = set_domestic which = political_left value = 6 }
		  command = { type = set_domestic which = freedom value = 8 }
		  command = { type = set_domestic which = free_market value = 7 }
           }
           action_b = {
                  name = "Ma Ying-jeou/Kuomintang"
                  ai_chance = 80
		  command = { type = headofstate which = 80529 }
		  command = { type = headofgovernment which = 80532 }
		  command = { type = armamentminister which = 80517 }
		  command = { type = foreignminister which = 80535 }
		  command = { type = ministerofsecurity which = 80520 }
		  command = { type = set_domestic which = political_left value = 3 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 8 }
           }
	   action_c = {
                  name = "Keep current"
                  ai_chance = 0
		  command = { type = dissent value = -1 }
           }

}
#############################################
###### Taiwan elections 2008 (alternate - Kosovo independent)
#############################################
event = {
         id = 80953
         random = no
         country = TRA
         trigger = {
                   government = democratic
		   atwar = no
                   NOT = { 
                            ispuppet = TRA
                   }
		   flag = warflag5

         }
 
         name = "EVT_80953_NAME"
         desc = "EVT_80953_DESC"
         style = 0
	 picture = "elections"
 
         date = { day = 22 month = march year = 2008 }
 
           action_a = {
                  name = "Frank Hsieh/DPP"
                  ai_chance = 100
		  command = { type = headofstate which = 80514 }
                  command = { type = headofgovernment which = 80531 }
                  command = { type = armamentminister which = 80516 }
                  command = { type = foreignminister which = 80533 }
		  command = { type = ministerofsecurity which = 80505 }
		  command = { type = set_domestic which = political_left value = 6 }
		  command = { type = set_domestic which = freedom value = 8 }
		  command = { type = set_domestic which = free_market value = 7 }
		  command = { type = trigger which = 80505 }
           }
           action_b = {
                  name = "Ma Ying-jeou/Kuomintang"
                  ai_chance = 0
		  command = { type = headofstate which = 80529 }
		  command = { type = headofgovernment which = 80532 }
		  command = { type = armamentminister which = 80517 }
		  command = { type = foreignminister which = 80535 }
		  command = { type = ministerofsecurity which = 80520 }
		  command = { type = set_domestic which = political_left value = 3 }
		  command = { type = set_domestic which = freedom value = 7 }
		  command = { type = set_domestic which = free_market value = 8 }
           }
	   action_c = {
                  name = "Keep current"
                  ai_chance = 0
		  command = { type = dissent value = -1 }
           }

}