#Slovenian events by Lutenet99
#########################################################################
#   Elections 2004
#########################################################################
event = {
	id = 73501
	random = no
	country = SLV
	trigger = {
		government = democratic
	}
	name = "EVT_73501_NAME"
	desc = "EVT_73501_DESC"
	style = 0
	picture = "elections"
	date = { day = 3 month = october year = 2004 }

	action_a = {
		name = "Liberal - LDS"
		ai_chance = 30
		command = { type = domestic which = political_left value = 1}
	}

	action_b = {
		name = "Conservative - SDS"
		ai_chance = 40
		command = { type = domestic which = political_left value = -1 }
		command = { type = headofgovernment which = 73511 }
		command = { type = foreignminister which = 73512 }
		command = { type = armamentminister which = 73513 }
		command = { type = ministerofsecurity which = 73514 }
		command = { type = chiefofstaff which = 73546 }
	}

	action_c = {
		name = "Social - ZLSD"
		ai_chance = 15
		command = { type = domestic which = political_left = 2 }
		command = { type = headofgovernment which = 73515 }
		command = { type = foreignminister which = 73516 }
		command = { type = armamentminister which = 73517 }
		command = { type = ministerofsecurity which = 73518 }
	}

	action_d = {
		name = "Christian - NSi"
		ai_chance = 15
		command = { type = domestic which = political_left = -2 }
		command = { type = headofgovernment which = 73520 }
		command = { type = foreignminister which = 73521 }
		command = { type = armamentminister which = 73522 }
		command = { type = ministerofsecurity which = 73514 }
	}


}

#########################################################################
#   PRESIDENTAL ELECTION 2007
#########################################################################
event = {
	id = 73502
	random = no
	country = SLV
	trigger = {
		government = democratic
	}
	name = "EVT_73502_NAME"
	desc = "EVT_73502_DESC"
	style = 0
	picture = "elections"
	date = { day = 11 month = november year = 2007 }

	action_a = {
		name = "Danilo Turk - Social"
		ai_chance = 70
		command = { type = dissent value = -3 }
		command = { type = headofstate which = 73547 }
	}

	action_b = {
		name = "Lojze Petrle - Conservative"
		ai_chance = 30
		command = { type = dissent value = -2 }
		command = { type = headofstate which = 73548 }
	}
}


