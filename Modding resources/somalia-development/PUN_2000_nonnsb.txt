﻿division_template = {
	name = "Ciidanka Daraawiishta Puntland"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
		Mech_Recce_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Militia"

	is_locked = yes

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }
		Militia_Bat = { x = 0 y = 2 }
		Militia_Bat = { x = 0 y = 3 }
			Militia_Bat = { x = 1 y = 0 }
			Militia_Bat = { x = 1 y = 1 }
	}
}

units = {
	#Puntal Dervish Force is about 10 000 strong, so ~3300 soldiers
	division = {
		name = "1. Ciidanka"
		location = 11090
		division_template = "Ciidanka Daraawiishta Puntland"
		start_experience_factor = 0.1
		start_equipment_factor = 1.0
	}
	division = {
		name = "2. Ciidanka"
		location = 3202
		division_template = "Ciidanka Daraawiishta Puntland"
		start_experience_factor = 0.1
		start_equipment_factor = 1.0
	}
	### Militias were about 15.000 at the beginning of 2000
	division = {
		name = "Somalian Militias"
		location = 1964
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 1.0
	}
	division = {
		name = "Somalian Militias"
		location = 11088
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 1.0
	}
	division = {
		name = "Somalian Militias"
		location = 765
		division_template = "Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 1.0
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 1200
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 80
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MBT_1
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = APC_1
		amount = 15
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0
		amount = 200
		producer = PUN
	}
}