influence_desicions = {
	hidden_ai_influence_decision = {
		icon = GFX_decision_reportdis_button
		cost = 25
		days_remove = 40
		days_re_enable = 15
		visible = { is_ai = yes }
		available = { check_variable = { influence_array^0 > 0 } }
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision hidden_ai_influence_decision"
			set_temp_variable = { percent_change = 5 }
			change_domestic_influence_percentage = yes
		}

		ai_will_do = {
			factor = 5
			modifier = {
				factor = 5
				check_variable = { domestic_influence_amount < 80 }
			}
			modifier = {
				factor = 2
				check_variable = { domestic_influence_amount < 60 }
			}
			modifier = {
				factor = 2
				check_variable = { domestic_influence_amount < 40 }
			}
			modifier = {
				factor = 2
				check_variable = { domestic_influence_amount < 20 }
			}
			modifier = {
				factor = 2
				check_variable = { domestic_influence_amount < 10 }
			}
			modifier = {
				factor = 0
				check_variable = { domestic_influence_amount > 90 }
			}
		}
	}
	disable_auto_influence_notification_decision = {
		icon = GFX_decision_reportdis_button
		visible = {
			is_ai = no
			NOT = { has_country_flag = disable_auto_influence_notification }
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision disable_auto_influence_notification_decision"
			set_country_flag = disable_auto_influence_notification
		}
	}
	enable_auto_influence_notification_decision = {
		icon = GFX_decision_reportagr_button
		visible = {
			is_ai = no
			has_country_flag = disable_auto_influence_notification
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision disable_auto_influence_notification_decision"
			clr_country_flag = disable_auto_influence_notification
		}
	}

	combat_foreign_influence = {
		icon = GFX_decision_combatinfl_button
		cost = 100
		days_remove = 45
		days_re_enable = 45

		available = {
			custom_trigger_tooltip = {
				tooltip = has_foreign_influence
				check_variable = { influence_array^0 > 0 }
			}
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision combat_foreign_influence"
			set_temp_variable = { percent_change = 4 }
			change_domestic_influence_percentage = yes
		}
	}

	propaganda_campaign_decision = {
		icon = GFX_decision_inlfpropaganda_button
		cost = 100
		days_remove = 100
		days_re_enable = 30

		available = {
			custom_trigger_tooltip = {
				tooltip = has_foreign_influence
				check_variable = { influence_array^0 > 0 }
			}
			NOT = {
				has_decision = combat_biggest_influencer_decision
				has_decision = combat_second_influencer_decision
				has_decision = combat_third_influencer_decision
			}
		}
		modifier = {
			political_power_factor = -0.25
			nationalist_drift = 0.10
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision propaganda_campaign_decision"
			custom_effect_tooltip = propaganda_campaign_costs_tt
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision propaganda_campaign_decision"
			add_stability = -0.05
			set_temp_variable = { percent_change = 10 }
			change_domestic_influence_percentage = yes
		}

		ai_will_do = {
			factor = 1
			modifier = {
				factor = 2
				check_variable = { domestic_influence_amount < 30 }
			}
			modifier = {
				factor = 2
				check_variable = { domestic_influence_amount < 15 }
			}
			modifier = {
				factor = 0
				NOT = {
					has_government = communism
					has_government = nationalist
					has_government = fascism
				}
			}
			modifier = {
				factor = 1.25
				has_government = communism
			}
			modifier = {
				factor = 1.75
				OR = {
					has_government = nationalist
					has_government = fascism
				}
			}
			modifier = {
				factor = 0.5
				is_ai = yes
			}
			modifier = {
				factor = 0
				check_variable = { domestic_influence_amount > 59.999 }
			}
		}
	}

	combat_biggest_influencer_decision = {
		icon = GFX_decision_antiinfl_button
		cost = 125
		days_remove = 60
		days_re_enable = 30

		visible = {
			check_variable = { influence_array^0 > 0 }
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = has_foreign_influence
				check_variable = { influence_array^0 > 0 }
			}
			NOT = {
				has_decision = propaganda_campaign_decision
				has_decision = combat_second_influencer_decision
				has_decision = combat_third_influencer_decision
			}
		}
		complete_effect = {
			set_variable = { combat_big_influencer = influence_array^0 }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision combat_biggest_influencer_decision"
			if = { limit = { check_variable = { combat_big_influencer = 0 } }
				set_temp_variable = { percent_change = -5 }
				set_temp_variable = { tag_index = influence_array^0 }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}
			else = {
				set_temp_variable = { percent_change = -5 }
				set_temp_variable = { tag_index = var:combat_big_influencer }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}

			clear_variable = combat_big_influencer
		}

		ai_will_do = {
			factor = 1
			modifier = { #Reduce down to nothing if you have high relations.
				factor = 0.15
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value > 49 }
					}
				}
			}
			modifier = {
				factor = 1.0
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value < 50 }
					}
				}
			}
			modifier = {
				factor = 1.25
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value < 25 }
					}
				}
			}
			modifier = {
				factor = 1.50
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value < 15 }
					}
				}
			}
			modifier = {
				factor = 1.75
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value < 0 }
					}
				}
			}
			modifier = {
				factor = 2.0
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value < -20 }
					}
				}
			}
			modifier = {
				factor = 2.5
				var:FROM.influence_array^0 = {
					ROOT = {
						has_opinion = { target = PREV value < -50 }
					}
				}
			}
			#Domestic Influence Calculation
			modifier = {
				set_temp_variable = { domestic_influence_amount_tmp = domestic_influence_amount }
				divide_temp_variable = { domestic_influence_amount_tmp = 10 }
				factor = domestic_influence_amount_tmp
			}
			#Should be more worthwhile if they have a high rate
			modifier = {
				factor = 2
				check_variable = { influence_array_val^0 > 19.999 }
			}
			modifier = {
				factor = 4
				check_variable = { influence_array_val^0 > 29.999 }
			}
			modifier = {
				factor = 5
				check_variable = { influence_array_val^0 > 49.999 }
			}
		}
	}

	combat_second_influencer_decision = {
		icon = GFX_decision_antiinfl_button
		cost = 125
		days_remove = 60
		days_re_enable = 30

		visible = {
			check_variable = { influence_array^1 > 0 }
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = has_foreign_influence
				check_variable = { influence_array^0 > 0 }
			}
			NOT = {
				has_decision = combat_biggest_influencer_decision
				has_decision = propaganda_campaign_decision
				has_decision = combat_third_influencer_decision
			}
		}
		complete_effect = {
			set_variable = { combat_second_big_influencer = influence_array^1 }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision combat_second_influencer_decision"
			if = { limit = { check_variable = { combat_second_big_influencer = 0 } }
				set_temp_variable = { percent_change = -5 }
				set_temp_variable = { tag_index = influence_array^1 }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}
			else = {
				set_temp_variable = { percent_change = -5 }
				set_temp_variable = { tag_index = var:combat_second_big_influencer }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}

			clear_variable = combat_second_big_influencer
		}


		ai_will_do = {
			factor = 1
			modifier = { #Reduce down to nothing if you have high relations.
				factor = 0.15
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value > 49 }
					}
				}
			}
			modifier = {
				factor = 1.0
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value < 50 }
					}
				}
			}
			modifier = {
				factor = 1.25
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value < 25 }
					}
				}
			}
			modifier = {
				factor = 1.50
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value < 15 }
					}
				}
			}
			modifier = {
				factor = 1.75
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value < 0 }
					}
				}
			}
			modifier = {
				factor = 2.0
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value < -20 }
					}
				}
			}
			modifier = {
				factor = 2.5
				var:FROM.influence_array^1 = {
					ROOT = {
						has_opinion = { target = PREV value < -50 }
					}
				}
			}
			#Domestic Influence Calculation
			modifier = {
				set_temp_variable = { domestic_influence_amount_tmp = domestic_influence_amount }
				divide_temp_variable = { domestic_influence_amount_tmp = 10 }
				factor = domestic_influence_amount_tmp
			}
			#Should be more worthwhile if they have a high rate
			modifier = {
				factor = 2
				check_variable = { influence_array_val^1 > 19.999 }
			}
			modifier = {
				factor = 4
				check_variable = { influence_array_val^1 > 29.999 }
			}
			modifier = {
				factor = 5
				check_variable = { influence_array_val^1 > 39.999 }
			}
		}
	}

	combat_third_influencer_decision = {
		icon = GFX_decision_antiinfl_button
		cost = 125
		days_remove = 60
		days_re_enable = 30

		visible = {
			check_variable = { influence_array^2 > 0 }
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = has_foreign_influence
				check_variable = { influence_array^0 > 0 }
			}
			NOT = {
				has_decision = combat_biggest_influencer_decision
				has_decision = propaganda_campaign_decision
				has_decision = combat_second_influencer_decision
			}
		}
		complete_effect = {
			set_variable = { combat_third_big_influencer = influence_array^2 }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision combat_third_influencer_decision"
			if = { limit = { check_variable = { combat_third_big_influencer = 0 } }
				set_temp_variable = { percent_change = -5 }
				set_temp_variable = { tag_index = influence_array^2 }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}
			else = {
				set_temp_variable = { percent_change = -5 }
				set_temp_variable = { tag_index = var:combat_third_big_influencer }
				set_temp_variable = { influence_target = THIS.id }
				change_influence_percentage = yes
			}

			clear_variable = combat_third_big_influencer
		}

		ai_will_do = {
			factor = 1
			modifier = { #Reduce down to nothing if you have high relations.
				factor = 0.15
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value > 49 } } }
			}
			modifier = {
				factor = 1.0
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value < 50 } } }
			}
			modifier = {
				factor = 1.25
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value < 25 } } }
			}
			modifier = {
				factor = 1.50
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value < 15 } } }
			}
			modifier = {
				factor = 1.75
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value < 0 } } }
			}
			modifier = {
				factor = 2.0
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value < -20 } } }
			}
			modifier = {
				factor = 2.5
				var:FROM.influence_array^2 = { ROOT = { has_opinion = { target = PREV value < -50 } } }
			}
			#Domestic Influence Calculation
			modifier = {
				set_temp_variable = { domestic_influence_amount_tmp = domestic_influence_amount }
				divide_temp_variable = { domestic_influence_amount_tmp = 10 }
				factor = domestic_influence_amount_tmp
			}
			#Should be more worthwhile if they have a high rate
			modifier = {
				factor = 2
				check_variable = { influence_array_val^2 > 19.999 }
			}
			modifier = {
				factor = 4
				check_variable = { influence_array_val^2 > 29.999 }
			}
		}
	}

	influence_for_autonomy_decision = {
		icon = GFX_decision_puppetdown_button
		cost = 50
		days_remove = 30
		days_re_enable = 15

		target_root_trigger = {
			num_subjects > 0
		}

		visible = {
			FROM = {
				is_subject_of = ROOT
				NOT = { has_autonomy_state = autonomy_special_administrative_region_HKG
						has_autonomy_state = autonomy_special_administrative_region_MAC
						has_autonomy_state = autonomy_special_administrative_region_TAI
						has_autonomy_state = autonomy_special_administrative_region_TIB
						has_autonomy_state = autonomy_special_administrative_region_ETK
						has_autonomy_state = autonomy_special_administrative_region_MON	}
			}
		}

		available = {
			FROM = {
				influence_higher_5 = yes
			}
		}

		target_array = subjects

		remove_effect = {
			log = "[GetDateText]: [This.GetName]: decision influence_for_autonomy_decision executed"
			set_temp_variable = { percent_change = -5 }
			set_temp_variable = { tag_index = ROOT.id }
			set_temp_variable = { influence_target = FROM.id }
			change_influence_percentage = yes
			FROM = {
				add_autonomy_ratio = {
					value = -0.08
					localization = influence_for_autonomy_decision_autonomy
				}
			}
		}

		ai_will_do = {
			factor = 0
			modifier = {
				add = 15
				FROM = {
					check_variable = { domestic_influence_amount > 20 }
				}
			}
		}
	}
}
