terror_gulf_category = {

	terror_fund_deradicalisation_programmes = {
		icon = GFX_decision_conter_deradicalisation_button
		available = { always = no }
		activation = { always = no }

		selectable_mission = yes

		days_mission_timeout = 30
		is_good = yes
		timeout_effect = {
			log = "[GetDateText]: [This.GetName]: decision terror_fund_deradicalisation_programmes executed"
			custom_effect_tooltip = terror_fund_deradicalisation_programmes_tt
			# Welfare Spending Determines Amount Lowered
			if = { limit = { has_idea = social_01 }
				set_temp_variable = { rad_change = -2 }
			}
			else_if = { limit = { has_idea = social_02 }
				set_temp_variable = { rad_change = -4 }
			}
			else_if = { limit = { has_idea = social_03 }
				set_temp_variable = { rad_change = -5 }
			}
			else_if = { limit = { has_idea = social_04 }
				set_temp_variable = { rad_change = -6 }
			}
			else_if = { limit = { has_idea = social_05 }
				set_temp_variable = { rad_change = -8 }
			}
			else_if = { limit = { has_idea = social_06 }
				set_temp_variable = { rad_change = -10 }
			}
			modify_radicalization_effect = yes

			set_temp_variable = { party_index = 11 }
			set_temp_variable = { party_popularity_increase = -0.02 }
			set_temp_variable = { temp_outlook_increase = -0.01 }
			add_relative_party_popularity = yes

			clr_country_flag = funding_derad

			ingame_update_setup = yes
		}
	}

	terror_ct_training_mission = {

		icon = GFX_decision_conter_exercises_button
		available = { always = no }
		activation = { always = no }

		selectable_mission = yes

		days_mission_timeout = 90
		is_good = yes

		timeout_effect = {
			# Math
			set_temp_variable = { foreign_advisors_math = 0.25 } # Base value per Advisors
			multiply_temp_variable = { foreign_advisors_math = foreign_advisors }
			# Foreign Advisors
			if = { limit = { has_idea = officer_advanced_training }
				add_to_temp_variable = { foreign_advisors_math = 0.50 }
			}
			else_if = { limit = { has_idea = officer_military_school }
				add_to_temp_variable = { foreign_advisors_math = 1.0 }
			}
			else_if = { limit = { has_idea = officer_military_academy }
				add_to_temp_variable = { foreign_advisors_math = 1.50 }
			}
			else_if = { limit = { has_idea = officer_international_education }
				add_to_temp_variable = { foreign_advisors_math = 2.0 }
			}
			add_to_temp_variable = { foreign_advisors_math = 1 } # Base Value of Improvement

			add_to_variable = { ct_training = foreign_advisors_math }
			clamp_variable = { var = ct_training min = 0 max = 10 }

			custom_effect_tooltip = terror_ct_training

			clr_country_flag = conducting_training
		}
	}

	terror_conduct_raid = {
		available = { always = no }
		activation = { always = no }

		selectable_mission = yes

		days_mission_timeout = 30
		is_good = yes

		timeout_effect = {
			add_command_power = -40

			custom_effect_tooltip = terror_conduct_raid_tt

			set_temp_variable = { ct_training_percentage_improvement = 5 }
			multiply_temp_variable = { ct_training_percentage_improvement = ct_training }
			add_to_temp_variable = { ct_training_percentage_improvement = 40 }
			set_temp_variable = { ct_training_failure = 100 }
			subtract_from_temp_variable = { ct_training_failure = ct_training_percentage_improvement }
			random_list = {
				ct_training_percentage_improvement = {
					country_event = { id = terror.1 }
				}
				ct_training_failure = {
					country_event = { id = terror.2 }
				}
			}

			clr_country_flag = conducting_raid
		}
	}

	terror_conduct_strike = {
		available = { always = no }
		activation = { always = no }

		selectable_mission = yes

		days_mission_timeout = 30
		is_good = yes

		timeout_effect = {
			custom_effect_tooltip = terror_conduct_strike_tt

			add_command_power = -40

			set_temp_variable = { ct_training_percentage_improvement = 5 }
			multiply_temp_variable = { ct_training_percentage_improvement = ct_training }
			add_to_temp_variable = { ct_training_percentage_improvement = 25 }
			set_temp_variable = { ct_training_failure = 100 }
			subtract_from_temp_variable = { ct_training_failure = ct_training_percentage_improvement }
			random_list = {
				ct_training_percentage_improvement = {
					country_event = { id = terror.15 }
				}
				ct_training_failure = {
					country_event = { id = terror.2 }
				}
			}

			clr_country_flag = conducting_strike
		}
	}
}