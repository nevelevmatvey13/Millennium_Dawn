UZB_karapalkstan_category = {
#
UZB_Promote_Uzbekistan_unity = {
	available = {
		UZB = {
		  NOT = {
			has_decision = UZB_Promote_Karapalkstan_identity
		  }
		}
	  }
	icon = GFX_decision_bos_new_nation_button
	visible = {
		original_tag = UZB
	}
	cost = 50
	days_remove = 45
	fire_only_once = no

	remove_effect = {
		UZB = { set_country_flag = uzbek_karap4 }
	}
	complete_effect = {
		UZB = {
			clr_country_flag = uzbek_karap4
		}
		custom_effect_tooltip = uzbek_karap_tt_effect
		custom_effect_tooltip = UZB_karapalkans_down_3
		add_to_variable = {
			var = UZB_karapalkans_opinion
			value = -3
		}
		custom_effect_tooltip = UZB_uzbeks_up_5
		add_to_variable = {
			var = UZB_uzbeks_opinion
			value = 5
		}
	}
}
#
UZB_Promote_Karapalkstan_identity = {
	available = {
		UZB = {
		  NOT = {
			has_decision = UZB_Promote_Uzbekistan_unity
		  }
		}
	  }
	icon = GFX_decision_karalp_decis
	visible = {
		original_tag = UZB
	}
	cost = 50
	days_remove = 45
	fire_only_once = no

	remove_effect = {
		UZB = { set_country_flag = uzbek_karap5 }
	}
	complete_effect = {
		UZB = {
			clr_country_flag = uzbek_karap5
		}
		custom_effect_tooltip = uzbek_karap_tt_effect
		custom_effect_tooltip = UZB_karapalkans_up_5
		add_to_variable = {
			var = UZB_karapalkans_opinion
			value = 5
		}
		custom_effect_tooltip = UZB_uzbeks_down_3
		add_to_variable = {
			var = UZB_uzbeks_opinion
			value = -3
		}
	}
}
}