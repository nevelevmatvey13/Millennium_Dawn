ai_templates_category = {
	convert_militia_to_light_inf = {
		fire_only_once = no
		days_re_enable = 300

		visible = {
			# convert 5 times = 25 units, if we still have militia then its needed for some reason
			check_variable = { militia_converter_counter < 5 }
		}

		available = {
			date > 2005.01.01
			has_war = no
			has_equipment = { Inf_equipment > 2000 }
			has_equipment = { cnc_equipment > 500 }
			num_of_military_factories > 5
			any_country_division = {
				division_has_majority_template = Militia_Bat
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision convert_militia_to_light_inf"
			if = {
				limit = {
					has_variable = militia_converter_counter
				}

				add_to_variable = { militia_converter_counter = 1 }
			}
			else = {
				set_variable = { militia_converter_counter = 1 }
			}
			every_country_division = {
				limit = { division_has_majority_template = Militia_Bat }
				random_select_amount = 5
				change_division_template = {
					division_template = "AI Light Infantry Brigade"
				}
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

	convert_l_inf_to_mot_inf = {
		fire_only_once = no
		days_re_enable = 300

		visible = {
			# convert 5 times = 25 units, if we still have l inf then its needed for some reason
			check_variable = { l_inf_converter_counter < 5 }
		}

		available = {
			date > 2005.01.01
			has_war = no
			has_equipment = { util_vehicle_equipment > 500 }
			num_of_military_factories > 10
			any_country_division = {
				division_has_majority_template = L_Inf_Bat
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision convert_l_inf_to_mot_inf"
			if = {
				limit = {
					has_variable = l_inf_converter_counter
				}

				add_to_variable = { l_inf_converter_counter = 1 }
			}
			else = {
				set_variable = { l_inf_converter_counter = 1 }
			}
			every_country_division = {
				limit = { division_has_majority_template = L_Inf_Bat }
				random_select_amount = 5
				change_division_template = {
					division_template = "AI Motorized Infantry Brigade"
				}
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

	convert_mot_to_mech_inf = {
		fire_only_once = no
		days_re_enable = 300

		visible = {
			# convert 5 times = 25 units, if we still have l inf then its needed for some reason
			check_variable = { mot_converter_counter < 5 }
		}

		available = {
			date > 2005.01.01
			has_war = no
			has_equipment = { apc_hull > 500 }
			num_of_military_factories > 20
			any_country_division = {
				division_has_majority_template = Mot_Inf_Bat
			}
		}

		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision convert_mot_to_mech_inf"
			if = {
				limit = {
					has_variable = mot_converter_counter
				}

				add_to_variable = { mot_converter_counter = 1 }
			}
			else = {
				set_variable = { mot_converter_counter = 1 }
			}
			every_country_division = {
				limit = { division_has_majority_template = Mot_Inf_Bat }
				random_select_amount = 5
				change_division_template = {
					division_template = "AI Mechanized Brigade"
				}
			}
		}

		ai_will_do = {
			factor = 1
		}
	}

	# convert_mech_to_arm_inf = {
	# 	fire_only_once = no
	# 	days_re_enable = 300

	# 	available = {
	# 		date > 2005.01.01
	# 		has_war = no
	# 		has_equipment = { ifv_hull > 500 }
	# 		num_of_military_factories > 35
	# 		any_country_division = {
	# 			division_has_majority_template = Mech_Inf_Bat
	# 		}
	# 	}

	# 	complete_effect = {
	# 		log = "[GetDateText]: [Root.GetName]: Decision convert_mech_to_arm_inf"
	# 		every_country_division = {
	# 			limit = { division_has_majority_template = Mech_Inf_Bat }
	# 			random_select_amount = 5
	# 			change_division_template = {
	# 				division_template = "AI Armored Infantry Brigade"
	# 			}
	# 		}
	# 	}

	# 	ai_will_do = {
	# 		factor = 1
	# 	}
	# }
}