migration_central_asia = {
	##Kazakhstan
	KAZ_migration_central_asia_5 = {

		icon = GFX_idea_kaz_migr

		cost = 100

		fire_only_once = yes

		allowed = {
			tag = KAZ
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kaz_200_tt
				check_variable = { gdp_total > 199 }
			}
		}

		remove_effect = {
			set_country_flag = KAZ_migration_central_asia_4
			swap_ideas = {
				remove_idea = KAZ_high_emigration_5
				add_idea = KAZ_high_emigration_4
			}
		}

		days_remove = 90
	}
	KAZ_migration_central_asia_4 = {

		icon = GFX_idea_kaz_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KAZ_migration_central_asia_4
		}
		allowed = {
			tag = KAZ
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kaz_300_tt
				check_variable = { gdp_total > 299 }
			}
		}

		remove_effect = {
			set_country_flag = KAZ_migration_central_asia_3
			swap_ideas = {
				remove_idea = KAZ_high_emigration_4
				add_idea = KAZ_high_emigration_3
			}
		}

		days_remove = 90
	}
	KAZ_migration_central_asia_3 = {

		icon = GFX_idea_kaz_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KAZ_migration_central_asia_3
		}
		allowed = {
			tag = KAZ
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kaz_400_tt
				check_variable = { gdp_total > 399 }
			}
		}

		remove_effect = {
			set_country_flag = KAZ_migration_central_asia_2
			swap_ideas = {
				remove_idea = KAZ_high_emigration_3
				add_idea = KAZ_high_emigration_2
			}
		}

		days_remove = 90
	}
	KAZ_migration_central_asia_2 = {

		icon = GFX_idea_kaz_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KAZ_migration_central_asia_2
		}
		allowed = {
			tag = KAZ
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kaz_500_tt
				check_variable = { gdp_total > 499 }
			}
		}

		remove_effect = {
			set_country_flag = KAZ_migration_central_asia_1
			swap_ideas = {
				remove_idea = KAZ_high_emigration_2
				add_idea = KAZ_high_emigration_1
			}
		}

		days_remove = 90
	}
	KAZ_migration_central_asia_1 = {

		icon = GFX_idea_kaz_mig

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KAZ_migration_central_asia_1
		}
		allowed = {
			tag = KAZ
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kaz_600_tt
				check_variable = { gdp_total > 599 }
			}
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = KAZ_high_emigration_1
				add_idea = KAZ_immigration
			}
		}

		days_remove = 90
	}
	#------------------------------------------------
	##Uzbekistan
	UZB_migration_central_asia_5 = {

		icon = GFX_idea_uzb_migr

		cost = 100

		fire_only_once = yes

		allowed = {
			tag = UZB
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_uzb_100_tt
				check_variable = { gdp_total > 99 }
			}
		}

		remove_effect = {
			set_country_flag = UZB_migration_central_asia_4
			swap_ideas = {
				remove_idea = UZB_high_emigration_5
				add_idea = UZB_high_emigration_4
			}
		}

		days_remove = 90
	}
	UZB_migration_central_asia_4 = {

		icon = GFX_idea_uzb_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = UZB_migration_central_asia_4
		}
		allowed = {
			tag = UZB
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_uzb_150_tt
				check_variable = { gdp_total > 149 }
			}
		}

		remove_effect = {
			set_country_flag = UZB_migration_central_asia_3
			swap_ideas = {
				remove_idea = UZB_high_emigration_4
				add_idea = UZB_high_emigration_3
			}
		}

		days_remove = 90
	}
	UZB_migration_central_asia_3 = {

		icon = GFX_idea_uzb_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = UZB_migration_central_asia_3
		}
		allowed = {
			tag = UZB
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_uzb_200_tt
				check_variable = { gdp_total > 199 }
			}
		}

		remove_effect = {
			set_country_flag = UZB_migration_central_asia_2
			swap_ideas = {
				remove_idea = UZB_high_emigration_3
				add_idea = UZB_high_emigration_2
			}
		}

		days_remove = 90
	}
	UZB_migration_central_asia_2 = {

		icon = GFX_idea_uzb_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = UZB_migration_central_asia_2
		}
		allowed = {
			tag = UZB
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_uzb_250_tt
				check_variable = { gdp_total > 249 }
			}
		}

		remove_effect = {
			set_country_flag = UZB_migration_central_asia_1
			swap_ideas = {
				remove_idea = UZB_high_emigration_2
				add_idea = UZB_high_emigration_1
			}
		}

		days_remove = 90
	}
	UZB_migration_central_asia_1 = {

		icon = GFX_idea_uzb_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = UZB_migration_central_asia_1
		}
		allowed = {
			tag = UZB
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_uzb_300_tt
				check_variable = { gdp_total > 299 }
			}
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = UZB_high_emigration_1
				add_idea = UZB_immigration
			}
		}

		days_remove = 90
	}
	#------------------------------------------------
	##Kyrgyzstan
	KYR_migration_central_asia_5 = {

		icon = GFX_idea_kyr_migr

		cost = 100

		fire_only_once = yes

		allowed = {
			tag = KYR
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kyr_20_tt
				check_variable = { gdp_total > 19 }
			}
		}

		remove_effect = {
			set_country_flag = KYR_migration_central_asia_4
			swap_ideas = {
				remove_idea = KYR_high_emigration_5
				add_idea = KYR_high_emigration_4
			}
		}

		days_remove = 90
	}
	KYR_migration_central_asia_4 = {

		icon = GFX_idea_kyr_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KYR_migration_central_asia_4
		}
		allowed = {
			tag = KYR
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kyr_35_tt
				check_variable = { gdp_total > 34 }
			}
		}

		remove_effect = {
			set_country_flag = KYR_migration_central_asia_3
			swap_ideas = {
				remove_idea = KYR_high_emigration_4
				add_idea = KYR_high_emigration_3
			}
		}

		days_remove = 90
	}
	KYR_migration_central_asia_3 = {

		icon = GFX_idea_kyr_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KYR_migration_central_asia_3
		}
		allowed = {
			tag = KYR
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kyr_50_tt
				check_variable = { gdp_total > 49 }
			}
		}

		remove_effect = {
			set_country_flag = KYR_migration_central_asia_2
			swap_ideas = {
				remove_idea = KYR_high_emigration_3
				add_idea = KYR_high_emigration_2
			}
		}

		days_remove = 90
	}
	KYR_migration_central_asia_2 = {

		icon = GFX_idea_kyr_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KYR_migration_central_asia_2
		}
		allowed = {
			tag = KYR
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kyr_65_tt
				check_variable = { gdp_total > 64 }
			}
		}

		remove_effect = {
			set_country_flag = KYR_migration_central_asia_1
			swap_ideas = {
				remove_idea = KYR_high_emigration_2
				add_idea = KYR_high_emigration_1
			}
		}

		days_remove = 90
	}
	KYR_migration_central_asia_1 = {

		icon = GFX_idea_kyr_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = KYR_migration_central_asia_1
		}
		allowed = {
			tag = KYR
		}
		available = {
			custom_trigger_tooltip = {
				tooltip = gdp_kyr_80_tt
				check_variable = { gdp_total > 79 }
			}
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = KYR_high_emigration_1
				add_idea = KYR_immigration
			}
		}

		days_remove = 90
	}
	#------------------------------------------------
	##Tajikistan
	TAJ_migration_central_asia_5 = {

		icon = GFX_idea_taj_migr

		cost = 100

		fire_only_once = yes

		allowed = {
			tag = TAJ
		}
		available = {
			offices > 7
			num_of_civilian_factories > 10
		}

		remove_effect = {
			set_country_flag = TAJ_migration_central_asia_4
			swap_ideas = {
				remove_idea = TAJ_high_emigration_5
				add_idea = TAJ_high_emigration_4
			}
		}

		days_remove = 90
	}
	TAJ_migration_central_asia_4 = {

		icon = GFX_idea_taj_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = TAJ_migration_central_asia_4
		}
		allowed = {
			tag = TAJ
		}
		available = {
			offices > 12
			num_of_civilian_factories > 15
		}

		remove_effect = {
			set_country_flag = TAJ_migration_central_asia_3
			swap_ideas = {
				remove_idea = TAJ_high_emigration_4
				add_idea = TAJ_high_emigration_3
			}
		}

		days_remove = 90
	}
	TAJ_migration_central_asia_3 = {

		icon = GFX_idea_taj_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = TAJ_migration_central_asia_3
		}
		allowed = {
			tag = TAJ
		}
		available = {
			offices > 17
			num_of_civilian_factories > 20
		}

		remove_effect = {
			set_country_flag = TAJ_migration_central_asia_2
			swap_ideas = {
				remove_idea = TAJ_high_emigration_3
				add_idea = TAJ_high_emigration_2
			}
		}

		days_remove = 90
	}
	TAJ_migration_central_asia_2 = {

		icon = GFX_idea_taj_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = TAJ_migration_central_asia_2
		}
		allowed = {
			tag = TAJ
		}
		available = {
			offices > 22
			num_of_civilian_factories > 25
		}

		remove_effect = {
			set_country_flag = TAJ_migration_central_asia_1
			swap_ideas = {
				remove_idea = TAJ_high_emigration_2
				add_idea = TAJ_high_emigration_1
			}
		}

		days_remove = 90
	}
	TAJ_migration_central_asia_1 = {

		icon = GFX_idea_taj_migr

		cost = 100

		fire_only_once = yes

		visible = {
			has_country_flag = TAJ_migration_central_asia_1
		}
		allowed = {
			tag = TAJ
		}
		available = {
			offices > 27
			num_of_civilian_factories > 30
		}
		remove_effect = {
			swap_ideas = {
				remove_idea = TAJ_high_emigration_1
				add_idea = TAJ_immigration
			}
		}

		days_remove = 90
	}
}