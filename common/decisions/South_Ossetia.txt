SOO_revoult_south_caucas = {
	##Azerbaijan
	SOO_influence_on_azer = {
		icon = GFX_decision_kavkaz_revol
		fire_only_once = no
		visible = {
			country_exists = AZE
			emerging_communist_state_are_in_power = yes
			NOT = { has_country_flag = SOO_azer }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_influence_on_azer"
			set_country_flag = SOO_azer
			clr_country_flag = SOO_aze_close
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_revolution_close4 = {
		fire_only_once = no
		icon = GFX_decision_kavkaz_revol
		visible = {
			country_exists = AZE
			has_country_flag = SOO_azer
			NOT = { has_country_flag = SOO_aze_close }
			NOT = { has_country_flag = reniwed_relations_with_azerbaijan }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_cis_revolution_close4"
			clr_country_flag = SOO_azer
			set_country_flag = SOO_aze_close
		}
		ai_will_do = { factor = 10 }
	}

	SOO_annex4 = {
		icon = GFX_decision_connection_kom
		cost = 50
		fire_only_once = yes
		days_remove = 10
		available = {
			AZE = {	is_subject_of = SOO	}
			AZE = { has_communist_government = yes }
		}
		visible = {
			country_exists = AZE
			has_country_flag = SOO_azer
			NOT = { has_country_flag = SOO_aze_close }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_annex4"
			AZE = { every_core_state = { add_core_of = SOO } }
			annex_country = { target = AZE transfer_troops = yes }
			SOO = {
				add_to_variable = { treasury = AZE.treasury }
				add_to_variable = { debt = AZE.debt }
				add_to_variable = { int_investments = AZE.int_investments }
			}
			set_country_flag = reniwed_relations_with_azerbaijan
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_of_local_military24 = {
		icon = GFX_decision_category_SOO_great_patriotic_war
		cost = 40
		fire_only_once = no
		days_remove = 55
		visible = {
			country_exists = AZE
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_azer
			NOT = { has_country_flag = SOO_aze_close }
			NOT = { has_country_flag = reniwed_relations_with_azerbaijan }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_of_local_military24"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			AZE = {
				add_to_variable = { party_pop_array^4 = 0.04 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.04
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_corrupt_local_officials24 = {
		icon = GFX_decision_zsr_parlament
		cost = 20
		fire_only_once = no
		days_remove = 70
		available = {
			AZE = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = AZE
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_azer
			NOT = { has_country_flag = SOO_aze_close }
			NOT = { has_country_flag = reniwed_relations_with_azerbaijan }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_corrupt_local_officials24"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			AZE = {
				add_to_variable = { party_pop_array^4 = 0.09 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.09
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_protest_financing24 = {
		icon = GFX_decision_zsr_revol
		cost = 15
		fire_only_once = no
		days_remove = 65
		available = {
			AZE = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = AZE
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_azer
			NOT = { has_country_flag = SOO_aze_close }
			NOT = { has_country_flag = reniwed_relations_with_azerbaijan }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_protest_financing24"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2.5 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			AZE = {
				add_to_variable = { party_pop_array^4 = 0.08 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.08
				}
				add_stability = -0.03
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_promotion_of_communist_ideology_in_government24 = {
		icon = GFX_decision_zsr_propaganda
		cost = 45
		fire_only_once = no
		days_remove = 45
		available = {
			AZE = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = AZE
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_azer
			NOT = { has_country_flag = SOO_aze_close }
			NOT = { has_country_flag = reniwed_relations_with_azerbaijan }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_promotion_of_communist_ideology_in_government24"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = AZE }
			change_influence_percentage = yes
			AZE = {
				add_to_variable = { party_pop_array^4 = 0.05 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.05
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}
	##Georgia
	SOO_influence_on_georgia = {
		icon = GFX_decision_kavkaz_revol
		fire_only_once = no
		visible = {
			country_exists = GEO
			emerging_communist_state_are_in_power = yes
			NOT = { has_country_flag = SOO_geor }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_influence_on_georgia"
			set_country_flag = SOO_geor
			clr_country_flag = SOO_geo_close
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_revolution_close5 = {
		fire_only_once = no
		icon = GFX_decision_kavkaz_revol
		visible = {
			country_exists = GEO
			has_country_flag = SOO_geor
			NOT = { has_country_flag = SOO_geo_close }
			NOT = { has_country_flag = reniwed_relations_with_georgia }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_cis_revolution_close5"
			clr_country_flag = SOO_geor
			set_country_flag = SOO_geo_close
		}
		ai_will_do = { factor = 10 }
	}

	SOO_annex5 = {
		icon = GFX_decision_connection_kom
		cost = 50
		fire_only_once = yes
		days_remove = 10
		available = {
			GEO = {	is_subject_of = SOO	}
			GEO = { has_communist_government = yes }
		}
		visible = {
			country_exists = GEO
			has_country_flag = SOO_geor
			NOT = { has_country_flag = SOO_geo_close }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_annex5"
			GEO = { every_core_state = { add_core_of = SOO } }
			annex_country = { target = GEO transfer_troops = yes }
			SOO = {
				add_to_variable = { treasury = GEO.treasury }
				add_to_variable = { debt = GEO.debt }
				add_to_variable = { int_investments = GEO.int_investments }
			}
			set_country_flag = reniwed_relations_with_georgia
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_of_local_military25 = {
		icon = GFX_decision_category_SOO_great_patriotic_war
		cost = 40
		fire_only_once = no
		days_remove = 55
		available = {
		}
		visible = {
			country_exists = GEO
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_geor
			NOT = { has_country_flag = SOO_geo_close }
			NOT = { has_country_flag = reniwed_relations_with_georgia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_of_local_military25"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			GEO = {
				add_to_variable = { party_pop_array^4 = 0.04 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.04
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_corrupt_local_officials25 = {
		icon = GFX_decision_zsr_parlament
		cost = 20
		fire_only_once = no
		days_remove = 70
		available = {
			GEO = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = GEO
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_geor
			NOT = { has_country_flag = SOO_geo_close }
			NOT = { has_country_flag = reniwed_relations_with_georgia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_corrupt_local_officials25"
			set_temp_variable = { treasury_change = -2.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			GEO = {
				add_to_variable = { party_pop_array^4 = 0.09 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.09
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_protest_financing25 = {
		icon = GFX_decision_zsr_revol
		cost = 15
		fire_only_once = no
		days_remove = 65
		available = {
			GEO = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = GEO
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_geor
			NOT = { has_country_flag = SOO_geo_close }
			NOT = { has_country_flag = reniwed_relations_with_georgia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_protest_financing25"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2.5 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			GEO = {
				add_to_variable = { party_pop_array^4 = 0.08 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.08
				}
				add_stability = -0.03
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_promotion_of_communist_ideology_in_government25 = {
		icon = GFX_decision_zsr_propaganda
		cost = 45
		fire_only_once = no
		days_remove = 45
		available = {
			GEO = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = GEO
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_geor
			NOT = { has_country_flag = SOO_geo_close }
			NOT = { has_country_flag = reniwed_relations_with_georgia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_promotion_of_communist_ideology_in_government25"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = GEO }
			change_influence_percentage = yes
			GEO = {
				add_to_variable = { party_pop_array^4 = 0.05 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.05
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}
	##Abkhazia
	SOO_influence_on_abkhazia = {
		icon = GFX_decision_kavkaz_revol
		fire_only_once = no
		visible = {
			country_exists = ABK
			emerging_communist_state_are_in_power = yes
			NOT = { has_country_flag = SOO_abkh }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_influence_on_abkhazia"
			set_country_flag = SOO_abkh
			clr_country_flag = SOO_abk_close
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_revolution_close = {
		fire_only_once = no
		icon = GFX_decision_kavkaz_revol
		visible = {
			country_exists = ABK
			has_country_flag = SOO_abkh
			NOT = { has_country_flag = SOO_abk_close }
			NOT = { has_country_flag = reniwed_relations_with_abkhazia }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_cis_revolution_close"
			clr_country_flag = SOO_abkh
			set_country_flag = SOO_abk_close
		}
		ai_will_do = { factor = 10 }
	}

	SOO_annex = {
		icon = GFX_decision_connection_kom
		cost = 50
		fire_only_once = yes
		days_remove = 10
		available = {
			ABK = {	is_subject_of = SOO	}
			ABK = { has_communist_government = yes }
		}
		visible = {
			country_exists = ABK
			has_country_flag = SOO_abkh
			NOT = { has_country_flag = SOO_abk_close }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_annex"
			ABK = { every_core_state = { add_core_of = SOO } }
			annex_country = { target = ABK transfer_troops = yes }
			SOO = {
				add_to_variable = { treasury = ABK.treasury }
				add_to_variable = { debt = ABK.debt }
				add_to_variable = { int_investments = ABK.int_investments }
			}
			set_country_flag = reniwed_relations_with_abkhazia
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_of_local_military = {
		icon = GFX_decision_category_SOO_great_patriotic_war
		cost = 40
		fire_only_once = no
		days_remove = 55
		available = {
		}
		visible = {
			country_exists = ABK
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_abkh
			NOT = { has_country_flag = SOO_abk_close }
			NOT = { has_country_flag = reniwed_relations_with_abkhazia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_of_local_military"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ABK }
			change_influence_percentage = yes
			ABK = {
				add_to_variable = { party_pop_array^4 = 0.04 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.04
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_corrupt_local_officials = {
		icon = GFX_decision_zsr_parlament
		cost = 20
		fire_only_once = no
		days_remove = 70
		available = {
			ARM = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = ABK
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_abkh
			NOT = { has_country_flag = SOO_abk_close }
			NOT = { has_country_flag = reniwed_relations_with_abkhazia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_corrupt_local_officials"
			set_temp_variable = { treasury_change = -2.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ABK }
			change_influence_percentage = yes
			ABK = {
				add_to_variable = { party_pop_array^4 = 0.09 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.09
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_protest_financing = {
		icon = GFX_decision_zsr_revol
		cost = 15
		fire_only_once = no
		days_remove = 65
		available = {
			ABK = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = ABK
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_abkh
			NOT = { has_country_flag = SOO_abk_close }
			NOT = { has_country_flag = reniwed_relations_with_abkhazia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_protest_financing"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 1.5 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ABK }
			change_influence_percentage = yes
			ABK = {
				add_to_variable = { party_pop_array^4 = 0.08 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.08
				}
				add_stability = -0.03
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_promotion_of_communist_ideology_in_government = {
		icon = GFX_decision_zsr_propaganda
		cost = 45
		fire_only_once = no
		days_remove = 45
		available = {
			ABK = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = ABK
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_abkh
			NOT = { has_country_flag = SOO_abk_close }
			NOT = { has_country_flag = reniwed_relations_with_abkhazia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_promotion_of_communist_ideology_in_government"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ABK }
			change_influence_percentage = yes
			ABK = {
				add_to_variable = { party_pop_array^4 = 0.05 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.05
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}
	##Armenia
	SOO_influence_on_armenia = {
		icon = GFX_decision_kavkaz_revol
		fire_only_once = no
		visible = {
			country_exists = ARM
			emerging_communist_state_are_in_power = yes
			NOT = { has_country_flag = SOO_arme }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_influence_on_armenia"
			set_country_flag = SOO_arme
			clr_country_flag = SOO_arm_close
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_revolution_close6 = {
		fire_only_once = no
		icon = GFX_decision_kavkaz_revol
		visible = {
			country_exists = ARM
			has_country_flag = SOO_arme
			NOT = { has_country_flag = SOO_arm_close }
			NOT = { has_country_flag = reniwed_relations_with_armenia }
		}
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_cis_revolution_close6"
			clr_country_flag = SOO_arme
			set_country_flag = SOO_arm_close
		}
		ai_will_do = { factor = 10 }
	}

	SOO_annex6 = {
		icon = GFX_decision_connection_kom
		cost = 50
		fire_only_once = yes
		days_remove = 10
		available = {
			ARM = {	is_subject_of = SOO	}
			ARM = { has_communist_government = yes }
		}
		visible = {
			country_exists = ARM
			has_country_flag = SOO_arme
			NOT = { has_country_flag = SOO_arm_close }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_annex6"
			ARM = { every_core_state = { add_core_of = SOO } }
			annex_country = { target = ARM transfer_troops = yes }
			SOO = {
				add_to_variable = { treasury = ARM.treasury }
				add_to_variable = { debt = ARM.debt }
				add_to_variable = { int_investments = ARM.int_investments }
			}
			set_country_flag = reniwed_relations_with_armenia
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_of_local_military26 = {
		icon = GFX_decision_category_SOO_great_patriotic_war
		cost = 40
		fire_only_once = no
		days_remove = 55
		available = {
		}
		visible = {
			country_exists = ARM
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_arme
			NOT = { has_country_flag = SOO_arm_close }
			NOT = { has_country_flag = reniwed_relations_with_armenia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_of_local_military26"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				add_to_variable = { party_pop_array^4 = 0.04 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.04
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_bribing_corrupt_local_officials26 = {
		icon = GFX_decision_zsr_parlament
		cost = 20
		fire_only_once = no
		days_remove = 70
		available = {
			ARM = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = ARM
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_arme
			NOT = { has_country_flag = SOO_arm_close }
			NOT = { has_country_flag = reniwed_relations_with_armenia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_bribing_corrupt_local_officials26"
			set_temp_variable = { treasury_change = -2.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				add_to_variable = { party_pop_array^4 = 0.09 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.09
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_protest_financing26 = {
		icon = GFX_decision_zsr_revol
		cost = 15
		fire_only_once = no
		days_remove = 65
		available = {
			ARM = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = ARM
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_arme
			NOT = { has_country_flag = SOO_arm_close }
			NOT = { has_country_flag = reniwed_relations_with_armenia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_protest_financing26"
			set_temp_variable = { treasury_change = -1.5 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 1.5 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				add_to_variable = { party_pop_array^4 = 0.08 }
				custom_effect_tooltip = FRA_communist_rise_10_TT
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.08
				}
				add_stability = -0.03
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}

	SOO_cis_promotion_of_communist_ideology_in_government26 = {
		icon = GFX_decision_zsr_propaganda
		cost = 45
		fire_only_once = no
		days_remove = 45
		available = {
			ARM = { emerging_communist_state_are_in_power = no }
		}
		visible = {
			country_exists = ARM
			emerging_communist_state_are_in_power = yes
			has_country_flag = SOO_arme
			NOT = { has_country_flag = SOO_arm_close }
			NOT = { has_country_flag = reniwed_relations_with_armenia }
		}
		remove_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision remove SOO_cis_promotion_of_communist_ideology_in_government26"
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
			set_temp_variable = { percent_change = 2 }
			set_temp_variable = { tag_index = SOO }
			set_temp_variable = { influence_target = ARM }
			change_influence_percentage = yes
			ARM = {
				add_to_variable = { party_pop_array^4 = 0.05 }
				recalculate_party = yes
				add_popularity = {
					ideology = communism
					popularity = 0.05
				}
			}
		}
		ai_will_do = {
			factor = 20
			modifier = {
				factor = 20
				is_historical_focus_on = yes
			}
		}
	}
}
SOO_greater_skifia_decisions_category = {
	SOO_influence_russia = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		available = {
			SOV = {
				influence_higher_50 = yes
				NOT = { has_government = nationalist }
			}
			SOV = {
				check_variable = { influence_array^0 = SOO }
				nationalist > 0.15
			}
		}
		cost = 180
		days_remove = 220
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_influence_russia"
			SOV = { country_event = sov_other.37 }
		}
	}

	SOO_influence_ukraine = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		available = {
			UKR = { influence_higher_50 = yes }
			UKR = {
				check_variable = { influence_array^0 = SOO }
				nationalist > 0.15
			}
		}
		cost = 180
		days_remove = 220
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_influence_ukraine"
			custom_effect_tooltip = Ukr_scyth_reb
			hidden_effect = {
				add_opinion_modifier = {
					modifier = declaration_of_friendship
					target = UKR
				}
				reverse_add_opinion_modifier = {
					modifier = declaration_of_friendship
					target = UKR
				}
				diplomatic_relation = {
					country = UKR
					relation = puppet
					active = yes
				}
				UKR = {
					hidden_effect = {
						clear_array = ruling_party
						clear_array = gov_coalition_array
						add_to_array = { ruling_party = 21 }
						update_government_coalition_strength = yes
						update_party_name = yes
						set_coalition_drift = yes
						meta_effect = {
							text = {
								set_country_flag = [META_SET_RULING_PARTY]
							}
							META_SET_RULING_PARTY = "[meta_set_ruling_leader]"
						}
						set_leader = yes
					}
					set_politics = {
						ruling_party = nationalist
					}
					add_popularity = {
						ideology = nationalist
						popularity = 0.35
					}
					add_to_variable = { party_pop_array^21 = 0.5 }
					recalculate_party = yes
					start_civil_war = {
						ideology = neutrality
						size = 1
					}
				}
			}
		}
	}

	SOO_caucas_south_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 180
		days_remove = 220
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_caucas_south_attack"
			create_wargoal = {
				type = annex_everything
				target = AZE
			}
			create_wargoal = {
				type = annex_everything
				target = GEO
			}
		}
	}
	SOO_central_azia_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 260
		days_remove = 220
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_central_azia_attack"
			TAJ = { country_event = Ossetia.7 }
			create_wargoal = {
				type = annex_everything
				target = KAZ
			}
			create_wargoal = {
				type = annex_everything
				target = TRK
			}
			create_wargoal = {
				type = annex_everything
				target = KYR
			}
			create_wargoal = {
				type = annex_everything
				target = UZB
			}
		}
	}
	SOO_CHINA_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 210
		days_remove = 140
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_CHINA_attack"
			create_wargoal = {
				type = annex_everything
				target = CHI
			}
		}
	}
	SOO_libya_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 210
		days_remove = 140
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_libya_attack"
			create_wargoal = {
				type = annex_everything
				target = LBA
			}
			create_wargoal = {
				type = annex_everything
				target = ITA
			}
		}
	}
	SOO_spain_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 210
		days_remove = 140
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_spain_attack"
			create_wargoal = {
				type = annex_everything
				target = SPR
			}
		}
	}
	SOO_India_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 250
		days_remove = 140
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_India_attack"
			RAJ = { country_event = Ossetia.7 }
		}
	}
	SOO_hungary_attack = {
		icon = GFX_decision_skufia_button
		allowed = {
			original_tag = SOO
		}
		cost = 250
		days_remove = 140
		fire_only_once = yes
		complete_effect = {
			log = "[GetDateText]: [Root.GetName]: Decision SOO_hungary_attack"
			HUN = { country_event = Ossetia.7 }
		}
	}
}
