SER_government_decisions = {
	icon = political_actions

	priority = 218

	allowed = {
		original_tag = SER
	}

	visible = {
		original_tag = SER
	}
}
SER_the_castle_crumbles = {
	icon = GFX_decisions_category_milochevic
	priority = 300
	allowed = {
		original_tag = SER
	}
	visible = {
		original_tag = SER
		NOT = { has_country_flag = SER_zover }
	}
}
SER_The_Montenegro_Confederacy = {
	icon = GFX_decisions_category_montenegro
	priority = 250
	allowed = {
		original_tag = SER
	}
	visible = {
		OR = {
			has_country_flag = SER_montenegro_independence
			has_country_flag = SER_montenegro_independence_violent
		}
		original_tag = SER
	}
}
SER_Export_category = {
	icon = GFX_decisions_category_export
	priority = 120
	allowed = {
		original_tag = SER
	}
	visible = { always = yes }
}
SER_exercises_category = {
	icon = GFX_decisions_category_exercises_usasov
	priority = 120
	allowed = {
		original_tag = SER
	}
	visible = {
		has_completed_focus = SER_uchenia
		original_tag = SER
	}
}
SER_doctrine_category = {
	icon = GFX_decisions_category
	priority = 120
	allowed = {
		original_tag = SER
	}
	visible = {
		has_completed_focus = SER_oborona
		original_tag = SER
	}
}
SER_ter_oborona_category = {
	icon = GFX_decisions_category_territorial_defence
	priority = 120
	allowed = {
		original_tag = SER
	}
	visible = {
		has_completed_focus = SER_ter_oborona
		original_tag = SER
		OR = {
			emerging_anarchist_communism_are_in_power = yes
			emerging_communist_state_are_in_power = yes
		} 
	}
}
SER_integrate_bosnia_category = {
	icon = GFX_decisions_category_bosn
	priority = 130
	allowed = {
		original_tag = SER
	}
	visible = {
		original_tag = SER
		has_completed_focus = SER_integrate_bosnia
	}
}
SER_integrate_yugoslavia_category = {
	icon = GFX_decisions_category_yugoslavia
	priority = 130
	allowed = {
		original_tag = SER
	}
	visible = {
		original_tag = SER
		OR = {
			emerging_anarchist_communism_are_in_power = yes
			emerging_communist_state_are_in_power = yes
		}
		has_completed_focus = SER_pheonix_nation
	}
}
SER_balkan_federation_category = {
	icon = GFX_decisions_category_balkan_federation
	priority = 130
	allowed = {
		original_tag = SER
	}
	visible = {
		original_tag = SER
		has_completed_focus = SER_confederation_realisation 
	}
}
SER_vojvodina_separatism_category = {
	icon = GFX_decisions_category_separ_voj
	priority = 130
	allowed = {
		OR = {
			original_tag = SER
			original_tag = VOJ
		}
	}
	visible = {
		original_tag = VOJ
		OR = {
			VOJ = {	
				has_autonomy_state = autonomy_autonomous_province1 
			}
			VOJ = {	
				has_autonomy_state = autonomy_autonomous_province
			}
		}
	}
}
KOS_albania_union = {
	icon = GFX_decisions_category_albans

	priority = 100

	allowed = {
		original_tag = KOS
	}

	visible = {
		has_country_flag = KOS_albania_unity
	}
}
KOS_int_recognition = {
	icon = GFX_decisions_category_recognition
	priority = 100
	allowed = {
		original_tag = KOS
	}
	visible = {
		OR = {
			has_idea = Lacks_International_Recognition
			has_idea = Non_State_Actor
		}
		has_completed_focus = KOS_struggle_for_recognition
	}
}