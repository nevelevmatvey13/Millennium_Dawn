################
##### Decisions for MD politics ######
################

generic_coalition_politics_desicions = {
	icon = GFX_decisions_category_political

	visible = {
		has_civil_war = no
		ERI_is_transitional_government_DIPLO = yes
	}

	priority = 30
}


coalition_decisions = {
	icon = GFX_decisions_category_political

	visible = {
		has_country_flag = coalition_forming
		check_variable = { government_coalition_strength_elect < majority_threshold } #not strong enough
	}
	priority = 60
}
