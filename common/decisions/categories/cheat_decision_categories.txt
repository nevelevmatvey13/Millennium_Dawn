cheat_decision_categories = {
	icon = generic_crisis
	# Visible tags
	visible = {
		is_ai = no
		OR = {
			has_global_flag = game_rule_allow_cheat_decisions
			has_global_flag = game_rule_allow_toggling_cheat_decisions
		}
	}
}