ETH_chi_investments_category = {
	icon = GFX_decision_generic_china

	priority = 100
	allowed = { original_tag = ETH }
	visible = { has_completed_focus = ETH_request_chinese_investments }
}

ETH_european_investments_category = {
	icon = GFX_decision_generic_european_union

	priority = 100
	allowed = { original_tag = ETH }
	visible = { has_completed_focus = ETH_request_european_investments }
}

ETH_eritrean_integration_category = {
	icon = GFX_decision_generic_eritrea

	priority = 100

	allowed = { original_tag = ETH }
	visible = { has_idea = ETH_dissolved_the_assembly }
}

ETH_eritrean_federalisation_category = {
	icon = GFX_decision_generic_eritrea

	priority = 100

	allowed = { original_tag = ETH }
	visible = { has_idea = ETH_eritrean_federalisation_underway }
}
