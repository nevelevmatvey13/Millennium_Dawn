MLR_manifest_decisions = {
	icon = GFX_decisions_category_moa
	priority = 110
	picture = GFX_decision_moa_decisions
	allowed = {
		original_tag = MLR
	}
	visible = {
		has_completed_focus = MLR_start
	}
}

