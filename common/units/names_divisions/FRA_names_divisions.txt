#Brigades names come from existing or former brigade of the French Army since the Cold War.
#To virtually help the list being slightly bigger, former divisions name were taken to as brigades. It's a common pratice, the 2nd Armored Brigade being the heir of 2nd Armored Division.
#Division names are unlikely to be used by the Player, except in USoE scenario, so I've simply taken the vanilla lists, removed some and add others.
#
FRA_INF_01 = {
	name = "Infantry Brigade"

	for_countries = { FRA }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat"  }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade d'infanterie"

	ordered = {
		1 = { "1er brigade d'infanterie 'Saint-Louis'" }
		2 = { "2e brigade d'infanterie 'Division Leclerc'" }
		3 = { "3e brigade d'infanterie 'Un seul but: la victoire'" }
		4 = { "4e brigade d'infanterie 'Supra Optimos'" }
		5 = { "5e brigade d'infanterie 'France d'abord'" }
		6 = { "6e brigade d'infanterie 'Vite, fort et loin'" }
		7 = { "7e brigade d'infanterie 'Force et Audace'" }
		8 = { "8e brigade d'infanterie 'En avant, toujours en avant'" }
		9 = { "9e brigade d'infanterie 'Semper et Ubique'" }
		10 = { "10e brigade d'infanterie 'Feux et foi'" }
		11 = { "11e brigade d'infanterie 'Droit devant'" }
		12 = { "12e brigade d'infanterie 'Brigade de fer'" }
		13 = { "13e brigade d'infanterie 'Le brave'" }
		14 = { "14e brigade d'infanterie 'Les aces'" }
		15 = { "15e brigade d'infanterie 'Utinam victrix'" }
		16 = { "16e brigade d'infanterie 'En flèche'" }
		17 = { "17e brigade d'infanterie 'La brigade Bretonne'" }
		18 = { "18e brigade d'infanterie 'Courage et Patrie'" }
		19 = { "19e brigade d'infanterie 'La France, toujours'" }
		20 = { "20e brigade d'infanterie 'Fer de lance'" }
		21 = { "21e brigade d'infanterie 'Auvergne'" }
		22 = { "22e brigade d'infanterie 'Fidèle'" }
		23 = { "23e brigade d'infanterie 'Sangliers des Ardennes'" }
		24 = { "24e brigade d'infanterie 'La 24ème'" }
		25 = { "25e brigade d'infanterie 'Pericula Ludus'" }
		26 = { "26e brigade d'infanterie 'Suivez-moi'" }
		27 = { "27e brigade d'infanterie 'Vivre libre ou mourir'" }
		28 = { "28e brigade d'infanterie 'mors venit, victoriam sonat'" }
		29 = { "29e brigade d'infanterie 'Pour la grandeur de la nation'" }
		30 = { "30e brigade d'infanterie 'Certum monstrat iter'" }
		31 = { "31e brigade d'infanterie 'Au-dessus de tous'" }
		32 = { "32e brigade d'infanterie 'Retroceder nescit'" }
		33 = { "33e brigade d'infanterie 'Tant qu'il en restera un'" }
		34 = { "34e brigade d'infanterie 'In gemino certamine'" }
		35 = { "35e brigade d'infanterie 'L'impétueuse'" }
		36 = { "36e brigade d'infanterie 'En avant Navarre sans peur'" }
		37 = { "37e brigade d'infanterie 'Potius mori quam foedari'" }
		38 = { "38e brigade d'infanterie 'La terrible que rien n'arrête'" }
		39 = { "39e brigade d'infanterie 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade d'infanterie 'Ex serviture libertas'" }
		41 = { "41e brigade d'infanterie 'Toujours prêt à bondir'" }
		42 = { "42e brigade d'infanterie 'Je suis de Verdun'" }
		43 = { "43e brigade d'infanterie 'Tout ou rien'" }
		44 = { "44e brigade d'infanterie 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade d'infanterie 'Noli irritare leonem'" }
		46 = { "46e brigade d'infanterie 'In periculo ludunt'" }
		47 = { "47e brigade d'infanterie 'Audace n'est pas déraison'" }
		48 = { "48e brigade d'infanterie 'Ma vie est dans l'action'" }
		49 = { "49e brigade d'infanterie 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade d'infanterie 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade d'infanterie 'Jusqu'au bout'" }
	}
}
FRA_MOT_01 = {
	name = "Motorized Brigade"

	for_countries = { FRA }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat"  }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de Brigade Motorisée"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade motorisée 'Saint-Louis'" }
		2 = { "2e brigade motorisée 'Division Leclerc'" }
		3 = { "3e brigade motorisée 'Un seul but: la victoire'" }
		4 = { "4e brigade motorisée 'Supra Optimos'" }
		5 = { "5e brigade motorisée 'France d'abord'" }
		6 = { "6e brigade motorisée 'Vite, fort et loin'" }
		7 = { "7e brigade motorisée 'Force et Audace'" }
		8 = { "8e brigade motorisée 'En avant, toujours en avant'" }
		9 = { "9e brigade motorisée 'Semper et Ubique'" }
		10 = { "10e brigade motorisée 'Feux et foi'" }
		11 = { "11e brigade motorisée 'Droit devant'" }
		12 = { "12e brigade motorisée 'Brigade de fer'" }
		13 = { "13e brigade motorisée 'Le brave'" }
		14 = { "14e brigade motorisée 'Les aces'" }
		15 = { "15e brigade motorisée 'Utinam victrix'" }
		16 = { "16e brigade motorisée 'En flèche'" }
		17 = { "17e brigade motorisée 'La brigade Bretonne'" }
		18 = { "18e brigade motorisée 'Courage et Patrie'" }
		19 = { "19e brigade motorisée 'La France, toujours'" }
		20 = { "20e brigade motorisée 'Fer de lance'" }
		21 = { "21e brigade motorisée 'Auvergne'" }
		22 = { "22e brigade motorisée 'Fidèle'" }
		23 = { "23e brigade motorisée 'Sangliers des Ardennes'" }
		24 = { "24e brigade motorisée 'La 24ème'" }
		25 = { "25e brigade motorisée 'Pericula Ludus'" }
		26 = { "26e brigade motorisée 'Suivez-moi'" }
		27 = { "27e brigade motorisée 'Vivre libre ou mourir'" }
		28 = { "28e brigade motorisée 'mors venit, victoriam sonat'" }
		29 = { "29e brigade motorisée 'Pour la grandeur de la nation'" }
		30 = { "30e brigade motorisée 'Certum monstrat iter'" }
		31 = { "31e brigade motorisée 'Au-dessus de tous'" }
		32 = { "32e brigade motorisée 'Retroceder nescit'" }
		33 = { "33e brigade motorisée 'Tant qu'il en restera un'" }
		34 = { "34e brigade motorisée 'In gemino certamine'" }
		35 = { "35e brigade motorisée 'L'impétueuse'" }
		36 = { "36e brigade motorisée 'En avant Navarre sans peur'" }
		37 = { "37e brigade motorisée 'Potius mori quam foedari'" }
		38 = { "38e brigade motorisée 'La terrible que rien n'arrête'" }
		39 = { "39e brigade motorisée 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade motorisée 'Ex serviture libertas'" }
		41 = { "41e brigade motorisée 'Toujours prêt à bondir'" }
		42 = { "42e brigade motorisée 'Je suis de Verdun'" }
		43 = { "43e brigade motorisée 'Tout ou rien'" }
		44 = { "44e brigade motorisée 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade motorisée 'Noli irritare leonem'" }
		46 = { "46e brigade motorisée 'In periculo ludunt'" }
		47 = { "47e brigade motorisée 'Audace n'est pas déraison'" }
		48 = { "48e brigade motorisée 'Ma vie est dans l'action'" }
		49 = { "49e brigade motorisée 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade motorisée 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade motorisée 'Jusqu'au bout'" }
	}
}
FRA_MECH_01 = {
	name = "Mechanized Brigade"

	for_countries = { FRA }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" "Mech_Inf_Bat" "Arm_Inf_Bat"  }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade mécanisée"

	ordered = {
		1 = { "1er brigade mécanisée 'Saint-Louis'" }
		2 = { "2e brigade mécanisée 'Brigade Leclerc'" }
		3 = { "3e brigade mécanisée 'Un seul but: la victoire'" }
		4 = { "4e brigade mécanisée 'Supra Optimos'" }
		5 = { "5e brigade mécanisée 'France d'abord'" }
		6 = { "6e brigade mécanisée 'Vite, fort et loin'" }
		7 = { "7e brigade mécanisée 'Force et Audace'" }
		8 = { "8e brigade mécanisée 'En avant, toujours en avant'" }
		9 = { "9e brigade mécanisée 'Semper et Ubique'" }
		10 = { "10e brigade mécanisée 'Feux et foi'" }
		11 = { "11e brigade mécanisée 'Droit devant'" }
		12 = { "12e brigade mécanisée 'Brigade de fer'" }
		13 = { "13e brigade mécanisée 'Le brave'" }
		14 = { "14e brigade mécanisée 'Les aces'" }
		15 = { "15e brigade mécanisée 'Utinam victrix'" }
		16 = { "16e brigade mécanisée 'En flèche'" }
		17 = { "17e brigade mécanisée 'La brigade Bretonne'" }
		18 = { "18e brigade mécanisée 'Courage et Patrie'" }
		19 = { "19e brigade mécanisée 'La France, toujours'" }
		20 = { "20e brigade mécanisée 'Fer de lance'" }
		21 = { "21e brigade mécanisée 'Auvergne'" }
		22 = { "22e brigade mécanisée 'Fidèle'" }
		23 = { "23e brigade mécanisée 'Sangliers des Ardennes'" }
		24 = { "24e brigade mécanisée 'La 24ème'" }
		25 = { "25e brigade mécanisée 'Pericula Ludus'" }
		26 = { "26e brigade mécanisée 'Suivez-moi'" }
		27 = { "27e brigade mécanisée 'Vivre libre ou mourir'" }
		28 = { "28e brigade mécanisée 'mors venit, victoriam sonat'" }
		29 = { "29e brigade mécanisée 'Pour la grandeur de la nation'" }
		30 = { "30e brigade mécanisée 'Certum monstrat iter'" }
		31 = { "31e brigade mécanisée 'Au-dessus de tous'" }
		32 = { "32e brigade mécanisée 'Retroceder nescit'" }
		33 = { "33e brigade mécanisée 'Tant qu'il en restera un'" }
		34 = { "34e brigade mécanisée 'In gemino certamine'" }
		35 = { "35e brigade mécanisée 'L'impétueuse'" }
		36 = { "36e brigade mécanisée 'En avant Navarre sans peur'" }
		37 = { "37e brigade mécanisée 'Potius mori quam foedari'" }
		38 = { "38e brigade mécanisée 'La terrible que rien n'arrête'" }
		39 = { "39e brigade mécanisée 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade mécanisée 'Ex serviture libertas'" }
		41 = { "41e brigade mécanisée 'Toujours prêt à bondir'" }
		42 = { "42e brigade mécanisée 'Je suis de Verdun'" }
		43 = { "43e brigade mécanisée 'Tout ou rien'" }
		44 = { "44e brigade mécanisée 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade mécanisée 'Noli irritare leonem'" }
		46 = { "46e brigade mécanisée 'In periculo ludunt'" }
		47 = { "47e brigade mécanisée 'Audace n'est pas déraison'" }
		48 = { "48e brigade mécanisée 'Ma vie est dans l'action'" }
		49 = { "49e brigade mécanisée 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade mécanisée 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade mécanisée 'Jusqu'au bout'" }
	}
}

FRA_ARM_01 = {
	name = "Armored Brigade"

	for_countries = { FRA }

	division_types = { "armor_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%dème brigade blindée"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade blindée 'Saint-Louis'" }
		2 = { "2e brigade blindée 'Division Leclerc'" }
		3 = { "3e brigade blindée 'Un seul but: la victoire'" }
		4 = { "4e brigade blindée 'Supra Optimos'" }
		5 = { "5e brigade blindée 'France d'abord'" }
		6 = { "6e brigade blindée 'Vite, fort et loin'" }
		7 = { "7e brigade blindée 'Force et Audace'" }
		8 = { "8e brigade blindée 'En avant, toujours en avant'" }
		9 = { "9e brigade blindée 'Semper et Ubique'" }
		10 = { "10e brigade blindée 'Feux et foi'" }
		11 = { "11e brigade blindée 'Droit devant'" }
		12 = { "12e brigade blindée 'Brigade de fer'" }
		13 = { "13e brigade blindée 'Le brave'" }
		14 = { "14e brigade blindée 'Les aces'" }
		15 = { "15e brigade blindée 'Utinam victrix'" }
		16 = { "16e brigade blindée 'En flèche'" }
		17 = { "17e brigade blindée 'La brigade Bretonne'" }
		18 = { "18e brigade blindée 'Courage et Patrie'" }
		19 = { "19e brigade blindée 'La France, toujours'" }
		20 = { "20e brigade blindée 'Fer de lance'" }
		21 = { "21e brigade blindée 'Auvergne'" }
		22 = { "22e brigade blindée 'Fidèle'" }
		23 = { "23e brigade blindée 'Sangliers des Ardennes'" }
		24 = { "24e brigade blindée 'La 24ème'" }
		25 = { "25e brigade blindée 'Pericula Ludus'" }
		26 = { "26e brigade blindée 'Suivez-moi'" }
		27 = { "27e brigade blindée 'Vivre libre ou mourir'" }
		28 = { "28e brigade blindée 'mors venit, victoriam sonat'" }
		29 = { "29e brigade blindée 'Pour la grandeur de la nation'" }
		30 = { "30e brigade blindée 'Certum monstrat iter'" }
		31 = { "31e brigade blindée 'Au-dessus de tous'" }
		32 = { "32e brigade blindée 'Retroceder nescit'" }
		33 = { "33e brigade blindée 'Tant qu'il en restera un'" }
		34 = { "34e brigade blindée 'In gemino certamine'" }
		35 = { "35e brigade blindée 'L'impétueuse'" }
		36 = { "36e brigade blindée 'En avant Navarre sans peur'" }
		37 = { "37e brigade blindée 'Potius mori quam foedari'" }
		38 = { "38e brigade blindée 'La terrible que rien n'arrête'" }
		39 = { "39e brigade blindée 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade blindée 'Ex serviture libertas'" }
		41 = { "41e brigade blindée 'Toujours prêt à bondir'" }
		42 = { "42e brigade blindée 'Je suis de Verdun'" }
		43 = { "43e brigade blindée 'Tout ou rien'" }
		44 = { "44e brigade blindée 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade blindée 'Noli irritare leonem'" }
		46 = { "46e brigade blindée 'In periculo ludunt'" }
		47 = { "47e brigade blindée 'Audace n'est pas déraison'" }
		48 = { "48e brigade blindée 'Ma vie est dans l'action'" }
		49 = { "49e brigade blindée 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade blindée 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade blindée 'Jusqu'au bout'" }
	}
}

FRA_ARM_02 = {
	name = "Light Armored Brigade"

	for_countries = { FRA }

	division_types = { "Mech_Inf_Bat" "armor_Bat" "Arm_Inf_Bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade légère blindée"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade légère blindée 'Saint-Louis'" }
		2 = { "2e brigade légère blindée 'Division Leclerc'" }
		3 = { "3e brigade légère blindée 'Un seul but: la victoire'" }
		4 = { "4e brigade légère blindée 'Supra Optimos'" }
		5 = { "5e brigade légère blindée 'France d'abord'" }
		6 = { "6e brigade légère blindée 'Vite, fort et loin'" }
		7 = { "7e brigade légère blindée 'Force et Audace'" }
		8 = { "8e brigade légère blindée 'En avant, toujours en avant'" }
		9 = { "9e brigade légère blindée 'Semper et Ubique'" }
		10 = { "10e brigade légère blindée 'Feux et foi'" }
		11 = { "11e brigade légère blindée 'Droit devant'" }
		12 = { "12e brigade légère blindée 'Brigade de fer'" }
		13 = { "13e brigade légère blindée 'Le brave'" }
		14 = { "14e brigade légère blindée 'Les aces'" }
		15 = { "15e brigade légère blindée 'Utinam victrix'" }
		16 = { "16e brigade légère blindée 'En flèche'" }
		17 = { "17e brigade légère blindée 'La brigade Bretonne'" }
		18 = { "18e brigade légère blindée 'Courage et Patrie'" }
		19 = { "19e brigade légère blindée 'La France, toujours'" }
		20 = { "20e brigade légère blindée 'Fer de lance'" }
		21 = { "21e brigade légère blindée 'Auvergne'" }
		22 = { "22e brigade légère blindée 'Fidèle'" }
		23 = { "23e brigade légère blindée 'Sangliers des Ardennes'" }
		24 = { "24e brigade légère blindée 'La 24ème'" }
		25 = { "25e brigade légère blindée 'Pericula Ludus'" }
		26 = { "26e brigade légère blindée 'Suivez-moi'" }
		27 = { "27e brigade légère blindée 'Vivre libre ou mourir'" }
		28 = { "28e brigade légère blindée 'mors venit, victoriam sonat'" }
		29 = { "29e brigade légère blindée 'Pour la grandeur de la nation'" }
		30 = { "30e brigade légère blindée 'Certum monstrat iter'" }
		31 = { "31e brigade légère blindée 'Au-dessus de tous'" }
		32 = { "32e brigade légère blindée 'Retroceder nescit'" }
		33 = { "33e brigade légère blindée 'Tant qu'il en restera un'" }
		34 = { "34e brigade légère blindée 'In gemino certamine'" }
		35 = { "35e brigade légère blindée 'L'impétueuse'" }
		36 = { "36e brigade légère blindée 'En avant Navarre sans peur'" }
		37 = { "37e brigade légère blindée 'Potius mori quam foedari'" }
		38 = { "38e brigade légère blindée 'La terrible que rien n'arrête'" }
		39 = { "39e brigade légère blindée 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade légère blindée 'Ex serviture libertas'" }
		41 = { "41e brigade légère blindée 'Toujours prêt à bondir'" }
		42 = { "42e brigade légère blindée 'Je suis de Verdun'" }
		43 = { "43e brigade légère blindée 'Tout ou rien'" }
		44 = { "44e brigade légère blindée 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade légère blindée 'Noli irritare leonem'" }
		46 = { "46e brigade légère blindée 'In periculo ludunt'" }
		47 = { "47e brigade légère blindée 'Audace n'est pas déraison'" }
		48 = { "48e brigade légère blindée 'Ma vie est dans l'action'" }
		49 = { "49e brigade légère blindée 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade légère blindée 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade légère blindée 'Jusqu'au bout'" }
	}
}

FRA_PAR_01 = {
	name = "Airborne Brigade"

	for_countries = { FRA }

	division_types = { "L_Air_Inf_Bat" "Mot_Air_Inf_Bat" "Mech_Air_Inf_Bat" "Arm_Air_Inf_Bat" "L_Air_assault_Bat" "Arm_Air_assault_Bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade parachutiste"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade parachutiste 'Saint-Louis'" }
		2 = { "2e brigade parachutiste 'Division Leclerc'" }
		3 = { "3e brigade parachutiste 'Un seul but: la victoire'" }
		4 = { "4e brigade parachutiste 'Supra Optimos'" }
		5 = { "5e brigade parachutiste 'France d'abord'" }
		6 = { "6e brigade parachutiste 'Vite, fort et loin'" }
		7 = { "7e brigade parachutiste 'Force et Audace'" }
		8 = { "8e brigade parachutiste 'En avant, toujours en avant'" }
		9 = { "9e brigade parachutiste 'Semper et Ubique'" }
		10 = { "10e brigade parachutiste 'Feux et foi'" }
		11 = { "11e brigade parachutiste 'Droit devant'" }
		12 = { "12e brigade parachutiste 'Brigade de fer'" }
		13 = { "13e brigade parachutiste 'Le brave'" }
		14 = { "14e brigade parachutiste 'Les aces'" }
		15 = { "15e brigade parachutiste 'Utinam victrix'" }
		16 = { "16e brigade parachutiste 'En flèche'" }
		17 = { "17e brigade parachutiste 'La brigade Bretonne'" }
		18 = { "18e brigade parachutiste 'Courage et Patrie'" }
		19 = { "19e brigade parachutiste 'La France, toujours'" }
		20 = { "20e brigade parachutiste 'Fer de lance'" }
		21 = { "21e brigade parachutiste 'Auvergne'" }
		22 = { "22e brigade parachutiste 'Fidèle'" }
		23 = { "23e brigade parachutiste 'Sangliers des Ardennes'" }
		24 = { "24e brigade parachutiste 'La 24ème'" }
		25 = { "25e brigade parachutiste 'Pericula Ludus'" }
		26 = { "26e brigade parachutiste 'Suivez-moi'" }
		27 = { "27e brigade parachutiste 'Vivre libre ou mourir'" }
		28 = { "28e brigade parachutiste 'mors venit, victoriam sonat'" }
		29 = { "29e brigade parachutiste 'Pour la grandeur de la nation'" }
		30 = { "30e brigade parachutiste 'Certum monstrat iter'" }
		31 = { "31e brigade parachutiste 'Au-dessus de tous'" }
		32 = { "32e brigade parachutiste 'Retroceder nescit'" }
		33 = { "33e brigade parachutiste 'Tant qu'il en restera un'" }
		34 = { "34e brigade parachutiste 'In gemino certamine'" }
		35 = { "35e brigade parachutiste 'L'impétueuse'" }
		36 = { "36e brigade parachutiste 'En avant Navarre sans peur'" }
		37 = { "37e brigade parachutiste 'Potius mori quam foedari'" }
		38 = { "38e brigade parachutiste 'La terrible que rien n'arrête'" }
		39 = { "39e brigade parachutiste 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade parachutiste 'Ex serviture libertas'" }
		41 = { "41e brigade parachutiste 'Toujours prêt à bondir'" }
		42 = { "42e brigade parachutiste 'Je suis de Verdun'" }
		43 = { "43e brigade parachutiste 'Tout ou rien'" }
		44 = { "44e brigade parachutiste 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade parachutiste 'Noli irritare leonem'" }
		46 = { "46e brigade parachutiste 'In periculo ludunt'" }
		47 = { "47e brigade parachutiste 'Audace n'est pas déraison'" }
		48 = { "48e brigade parachutiste 'Ma vie est dans l'action'" }
		49 = { "49e brigade parachutiste 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade parachutiste 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade parachutiste 'Jusqu'au bout'" }
	}
}
FRA_MAR_01 = {
	name = "Marine Brigade"

	for_countries = { FRA }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade d'infanterie de marine"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade d'infanterie de marine 'Saint-Louis'" }
		2 = { "2e brigade d'infanterie de marine 'Division Leclerc'" }
		3 = { "3e brigade d'infanterie de marine 'Un seul but: la victoire'" }
		4 = { "4e brigade d'infanterie de marine 'Supra Optimos'" }
		5 = { "5e brigade d'infanterie de marine 'France d'abord'" }
		6 = { "6e brigade d'infanterie de marine 'Vite, fort et loin'" }
		7 = { "7e brigade d'infanterie de marine 'Force et Audace'" }
		8 = { "8e brigade d'infanterie de marine 'En avant, toujours en avant'" }
		9 = { "9e brigade d'infanterie de marine 'Semper et Ubique'" }
		10 = { "10e brigade d'infanterie de marine 'Feux et foi'" }
		11 = { "11e brigade d'infanterie de marine 'Droit devant" }
		12 = { "12e brigade d'infanterie de marine 'Brigade de fer'" }
		13 = { "13e brigade d'infanterie de marine 'Le brave'" }
		14 = { "14e brigade d'infanterie de marine 'Les aces'" }
		15 = { "15e brigade d'infanterie de marine 'Utinam victrix'" }
		16 = { "16e brigade d'infanterie de marine 'En flèche'" }
		17 = { "17e brigade d'infanterie de marine 'La brigade Bretonne'" }
		18 = { "18e brigade d'infanterie de marine 'Courage et Patrie'" }
		19 = { "19e brigade d'infanterie de marine 'La France, toujours'" }
		20 = { "20e brigade d'infanterie de marine 'Fer de lance'" }
		21 = { "21e brigade d'infanterie de marine 'Auvergne'" }
		22 = { "22e brigade d'infanterie de marine 'Fidèle'" }
		23 = { "23e brigade d'infanterie de marine 'Sangliers des Ardennes'" }
		24 = { "24e brigade d'infanterie de marine 'La 24ème'" }
		25 = { "25e brigade d'infanterie de marine 'Pericula Ludus'" }
		26 = { "26e brigade d'infanterie de marine 'Suivez-moi'" }
		27 = { "27e brigade d'infanterie de marine 'Vivre libre ou mourir'" }
		28 = { "28e brigade d'infanterie de marine 'mors venit, victoriam sonat'" }
		29 = { "29e brigade d'infanterie de marine 'Pour la grandeur de la nation'" }
		30 = { "30e brigade d'infanterie de marine 'Certum monstrat iter'" }
		31 = { "31e brigade d'infanterie de marine 'Au-dessus de tous'" }
		32 = { "32e brigade d'infanterie de marine 'Retroceder nescit'" }
		33 = { "33e brigade d'infanterie de marine 'Tant qu'il en restera un'" }
		34 = { "34e brigade d'infanterie de marine 'In gemino certamine'" }
		35 = { "35e brigade d'infanterie de marine 'L'impétueuse'" }
		36 = { "36e brigade d'infanterie de marine 'En avant Navarre sans peur'" }
		37 = { "37e brigade d'infanterie de marine 'Potius mori quam foedari'" }
		38 = { "38e brigade d'infanterie de marine 'La terrible que rien n'arrête'" }
		39 = { "39e brigade d'infanterie de marine 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade d'infanterie de marine 'Ex serviture libertas'" }
		41 = { "41e brigade d'infanterie de marine 'Toujours prêt à bondir'" }
		42 = { "42e brigade d'infanterie de marine 'Je suis de Verdun'" }
		43 = { "43e brigade d'infanterie de marine 'Tout ou rien'" }
		44 = { "44e brigade d'infanterie de marine 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade d'infanterie de marine 'Noli irritare leonem'" }
		46 = { "46e brigade d'infanterie de marine 'In periculo ludunt'" }
		47 = { "47e brigade d'infanterie de marine 'Audace n'est pas déraison'" }
		48 = { "48e brigade d'infanterie de marine 'Ma vie est dans l'action'" }
		49 = { "49e brigade d'infanterie de marine 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade d'infanterie de marine 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade d'infanterie de marine 'Jusqu'au bout'" }
	}
}
FRA_MAR_02 = {
	name = "Marine Armored Brigade"

	for_countries = { FRA }

	division_types = { "L_Marine_Bat" "Mot_Marine_Bat" "Mech_Marine_Bat" "Arm_Marine_Bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%dème Brigade d'Infanterie de Marine"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade légère blindée de marine 'Saint-Louis'" }
		2 = { "2e brigade légère blindée de marine 'Division Leclerc'" }
		3 = { "3e brigade légère blindée de marine 'Un seul but: la victoire'" }
		4 = { "4e brigade légère blindée de marine 'Supra Optimos'" }
		5 = { "5e brigade légère blindée de marine 'France d'abord'" }
		6 = { "6e brigade légère blindée de marine 'Vite, fort et loin'" }
		7 = { "7e brigade légère blindée de marine 'Force et Audace'" }
		8 = { "8e brigade légère blindée de marine 'En avant, toujours en avant'" }
		9 = { "9e brigade légère blindée de marine 'Semper et Ubique'" }
		10 = { "10e brigade légère blindée de marine 'Feux et foi'" }
		11 = { "11e brigade légère blindée de marine 'Droit devant" }
		12 = { "12e brigade légère blindée de marine 'Brigade de fer'" }
		13 = { "13e brigade légère blindée de marine 'Le brave'" }
		14 = { "14e brigade légère blindée de marine 'Les aces'" }
		15 = { "15e brigade légère blindée de marine 'Utinam victrix'" }
		16 = { "16e brigade légère blindée de marine 'En flèche'" }
		17 = { "17e brigade légère blindée de marine 'La brigade Bretonne'" }
		18 = { "18e brigade légère blindée de marine 'Courage et Patrie'" }
		19 = { "19e brigade légère blindée de marine 'La France, toujours'" }
		20 = { "20e brigade légère blindée de marine 'Fer de lance'" }
		21 = { "21e brigade légère blindée de marine 'Auvergne'" }
		22 = { "22e brigade légère blindée de marine 'Fidèle'" }
		23 = { "23e brigade légère blindée de marine 'Sangliers des Ardennes'" }
		24 = { "24e brigade légère blindée de marine 'La 24ème'" }
		25 = { "25e brigade légère blindée de marine 'Pericula Ludus'" }
		26 = { "26e brigade légère blindée de marine 'Suivez-moi'" }
		27 = { "27e brigade légère blindée de marine 'Vivre libre ou mourir'" }
		28 = { "28e brigade légère blindée de marine 'mors venit, victoriam sonat'" }
		29 = { "29e brigade légère blindée de marine 'Pour la grandeur de la nation'" }
		30 = { "30e brigade légère blindée de marine 'Certum monstrat iter'" }
		31 = { "31e brigade légère blindée de marine 'Au-dessus de tous'" }
		32 = { "32e brigade légère blindée de marine 'Retroceder nescit'" }
		33 = { "33e brigade légère blindée de marine 'Tant qu'il en restera un'" }
		34 = { "34e brigade légère blindée de marine 'In gemino certamine'" }
		35 = { "35e brigade légère blindée de marine 'L'impétueuse'" }
		36 = { "36e brigade légère blindée de marine 'En avant Navarre sans peur'" }
		37 = { "37e brigade légère blindée de marine 'Potius mori quam foedari'" }
		38 = { "38e brigade légère blindée de marine 'La terrible que rien n'arrête'" }
		39 = { "39e brigade légère blindée de marine 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade légère blindée de marine 'Ex serviture libertas'" }
		41 = { "41e brigade légère blindée de marine 'Toujours prêt à bondir'" }
		42 = { "42e brigade légère blindée de marine 'Je suis de Verdun'" }
		43 = { "43e brigade légère blindée de marine 'Tout ou rien'" }
		44 = { "44e brigade légère blindée de marine 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade légère blindée de marine 'Noli irritare leonem'" }
		46 = { "46e brigade légère blindée de marine 'In periculo ludunt'" }
		47 = { "47e brigade légère blindée de marine 'Audace n'est pas déraison'" }
		48 = { "48e brigade légère blindée de marine 'Ma vie est dans l'action'" }
		49 = { "49e brigade légère blindée de marine 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade légère blindée de marine 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade légère blindée de marine 'Jusqu'au bout'" }
	}
}

FRA_MNT_01 = {
	name = "Mountain Brigade"

	for_countries = { FRA }

	division_types = { "L_Inf_Bat" "Mot_Inf_Bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade d'infanterie de montagne"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade d'infanterie de montagne 'Saint-Louis'" }
		2 = { "2e brigade d'infanterie de montagne 'Division Leclerc'" }
		3 = { "3e brigade d'infanterie de montagne 'Un seul but: la victoire'" }
		4 = { "4e brigade d'infanterie de montagne 'Supra Optimos'" }
		5 = { "5e brigade d'infanterie de montagne 'France d'abord'" }
		6 = { "6e brigade d'infanterie de montagne 'Vite, fort et loin'" }
		7 = { "7e brigade d'infanterie de montagne 'Force et Audace'" }
		8 = { "8e brigade d'infanterie de montagne 'En avant, toujours en avant'" }
		9 = { "9e brigade d'infanterie de montagne 'Semper et Ubique'" }
		10 = { "10e brigade d'infanterie de montagne 'Feux et foi'" }
		11 = { "11e brigade d'infanterie de montagne 'Droit devant'" }
		12 = { "12e brigade d'infanterie de montagne 'Brigade de fer'" }
		13 = { "13e brigade d'infanterie de montagne 'Le brave'" }
		14 = { "14e brigade d'infanterie de montagne 'Les aces'" }
		15 = { "15e brigade d'infanterie de montagne 'Utinam victrix'" }
		16 = { "16e brigade d'infanterie de montagne 'En flèche'" }
		17 = { "17e brigade d'infanterie de montagne 'La brigade Bretonne'" }
		18 = { "18e brigade d'infanterie de montagne 'Courage et Patrie'" }
		19 = { "19e brigade d'infanterie de montagne 'La France, toujours'" }
		20 = { "20e brigade d'infanterie de montagne 'Fer de lance'" }
		21 = { "21e brigade d'infanterie de montagne 'Auvergne'" }
		22 = { "22e brigade d'infanterie de montagne 'Fidèle'" }
		23 = { "23e brigade d'infanterie de montagne 'Sangliers des Ardennes'" }
		24 = { "24e brigade d'infanterie de montagne 'La 24ème'" }
		25 = { "25e brigade d'infanterie de montagne 'Pericula Ludus'" }
		26 = { "26e brigade d'infanterie de montagne 'Suivez-moi'" }
		27 = { "27e brigade d'infanterie de montagne 'Vivre libre ou mourir'" }
		28 = { "28e brigade d'infanterie de montagne 'mors venit, victoriam sonat'" }
		29 = { "29e brigade d'infanterie de montagne 'Pour la grandeur de la nation'" }
		30 = { "30e brigade d'infanterie de montagne 'Certum monstrat iter'" }
		31 = { "31e brigade d'infanterie de montagne 'Au-dessus de tous'" }
		32 = { "32e brigade d'infanterie de montagne 'Retroceder nescit'" }
		33 = { "33e brigade d'infanterie de montagne 'Tant qu'il en restera un'" }
		34 = { "34e brigade d'infanterie de montagne 'In gemino certamine'" }
		35 = { "35e brigade d'infanterie de montagne 'L'impétueuse'" }
		36 = { "36e brigade d'infanterie de montagne 'En avant Navarre sans peur'" }
		37 = { "37e brigade d'infanterie de montagne 'Potius mori quam foedari'" }
		38 = { "38e brigade d'infanterie de montagne 'La terrible que rien n'arrête'" }
		39 = { "39e brigade d'infanterie de montagne 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade d'infanterie de montagne 'Ex serviture libertas'" }
		41 = { "41e brigade d'infanterie de montagne 'Toujours prêt à bondir'" }
		42 = { "42e brigade d'infanterie de montagne 'Je suis de Verdun'" }
		43 = { "43e brigade d'infanterie de montagne 'Tout ou rien'" }
		44 = { "44e brigade d'infanterie de montagne 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade d'infanterie de montagne 'Noli irritare leonem'" }
		46 = { "46e brigade d'infanterie de montagne 'In periculo ludunt'" }
		47 = { "47e brigade d'infanterie de montagne 'Audace n'est pas déraison'" }
		48 = { "48e brigade d'infanterie de montagne 'Ma vie est dans l'action'" }
		49 = { "49e brigade d'infanterie de montagne 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade d'infanterie de montagne 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade d'infanterie de montagne 'Jusqu'au bout'" }
	}
}

FRA_AIR_01 = {
	name = "Airmobile Brigade"

	for_countries = { FRA }

	division_types = { "L_Air_assault_Bat" "Arm_Air_assault_Bat" "attack_helo_bat" }

	link_numbering_with = { FRA_INF_01 FRA_MOT_01 FRA_MECH_01 FRA_ARM_01 FRA_ARM_02 FRA_PAR_01 FRA_MAR_01 FRA_MAR_02 FRA_MNT_01 FRA_AIR_01 }

	fallback_name = "%de brigade aéromobile"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er brigade aéromobile 'Saint-Louis'" }
		2 = { "2e brigade aéromobile 'Division Leclerc'" }
		3 = { "3e brigade aéromobile 'Un seul but: la victoire'" }
		4 = { "4e brigade aéromobile 'Supra Optimos'" }
		5 = { "5e brigade aéromobile 'France d'abord'" }
		6 = { "6e brigade aéromobile 'Vite, fort et loin'" }
		7 = { "7e brigade aéromobile 'Force et Audace'" }
		8 = { "8e brigade aéromobile 'En avant, toujours en avant'" }
		9 = { "9e brigade aéromobile 'Semper et Ubique'" }
		10 = { "10e brigade aéromobile 'Feux et foi'" }
		11 = { "11e brigade aéromobile 'Droit devant'" }
		12 = { "12e brigade aéromobile 'Brigade de fer'" }
		13 = { "13e brigade aéromobile 'Le brave'" }
		14 = { "14e brigade aéromobile 'Les aces'" }
		15 = { "15e brigade aéromobile 'Utinam victrix'" }
		16 = { "16e brigade aéromobile 'En flèche'" }
		17 = { "17e brigade aéromobile 'La brigade Bretonne'" }
		18 = { "18e brigade aéromobile 'Courage et Patrie'" }
		19 = { "19e brigade aéromobile 'La France, toujours'" }
		20 = { "20e brigade aéromobile 'Fer de lance'" }
		21 = { "21e brigade aéromobile 'Auvergne'" }
		22 = { "22e brigade aéromobile 'Fidèle'" }
		23 = { "23e brigade aéromobile 'Sangliers des Ardennes'" }
		24 = { "24e brigade aéromobile 'La 24ème'" }
		25 = { "25e brigade aéromobile 'Pericula Ludus'" }
		26 = { "26e brigade aéromobile 'Suivez-moi'" }
		27 = { "27e brigade aéromobile 'Vivre libre ou mourir'" }
		28 = { "28e brigade aéromobile 'mors venit, victoriam sonat'" }
		29 = { "29e brigade aéromobile 'Pour la grandeur de la nation'" }
		30 = { "30e brigade aéromobile 'Certum monstrat iter'" }
		31 = { "31e brigade aéromobile 'Au-dessus de tous'" }
		32 = { "32e brigade aéromobile 'Retroceder nescit'" }
		33 = { "33e brigade aéromobile 'Tant qu'il en restera un'" }
		34 = { "34e brigade aéromobile 'In gemino certamine'" }
		35 = { "35e brigade aéromobile 'L'impétueuse'" }
		36 = { "36e brigade aéromobile 'En avant Navarre sans peur'" }
		37 = { "37e brigade aéromobile 'Potius mori quam foedari'" }
		38 = { "38e brigade aéromobile 'La terrible que rien n'arrête'" }
		39 = { "39e brigade aéromobile 'Tué oui, vaincu Jamais'" }
		40 = { "40e brigade aéromobile 'Ex serviture libertas'" }
		41 = { "41e brigade aéromobile 'Toujours prêt à bondir'" }
		42 = { "42e brigade aéromobile 'Je suis de Verdun'" }
		43 = { "43e brigade aéromobile 'Tout ou rien'" }
		44 = { "44e brigade aéromobile 'Incorrupta fides et avita vera'" }
		45 = { "45e brigade aéromobile 'Noli irritare leonem'" }
		46 = { "46e brigade aéromobile 'In periculo ludunt'" }
		47 = { "47e brigade aéromobile 'Audace n'est pas déraison'" }
		48 = { "48e brigade aéromobile 'Ma vie est dans l'action'" }
		49 = { "49e brigade aéromobile 'Ardeur, efficacité, audace'" }
		50 = { "50e brigade aéromobile 'En dépit de tout, au-delà de tout'" }
		51 = { "51e brigade aéromobile 'Jusqu'au bout'" }
	}
}
FRA_SOF_01 = {
	name = "SOF Regiment"

	for_countries = { FRA }

	division_types = { "Special_Forces" }

	fallback_name = "%de régiment de dragons parachutistes"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er RPIMa 'Qui ose gagne'" }
		2 = { "2e RPIMa" }
		3 = { "3e RPIMa 'Être et durer'" }
		4 = { "4e RPIMa" }
		5 = { "5e RPIMa" }
		6 = { "6e RPIMa" }
		7 = { "7e RPIMa" }
		8 = { "8e RPIMa 'Volontaire'" }
		9 = { "9e RPIMa" }
		10 = { "CPA 10/30 'Au-dessus de la mêlée'" }
		11 = { "11e régiment de dragons parachutistes" }
		12 = { "12e régiment de dragons parachutistes" }
		13 = { "13e régiment de dragons parachutistes 'Au-delà du possible'" }
		14 = { "14e régiment de dragons parachutistes" }
		15 = { "15e régiment de dragons parachutistes" }
		100 = { "Service Action" }

	}
}

FRA_FRENCH_LEGION_NAME = {
	name = "Foreign Legion"

	for_countries = { FRA }

	division_types = { "Special_Forces" }

	#link_numbering_with = { FRA_INF_01 }

	fallback_name = "%de régiment étranger"

	# Names with numbers (only one number per entry).
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "1er régiment étranger 'Nous sommes tous des volontaires'" }
		2 = { "2e régiment étranger" }
		3 = { "3e régiment étranger" }
		4 = { "4e régiment étranger 'Creuset de la Légion'" }
		5 = { "5e régiment étranger" }
		6 = { "6e régiment étranger" }
		7 = { "7e régiment étranger" }
		8 = { "8e régiment étranger" }
		9 = { "9e régiment étranger" }
		10 = { "10e régiment étranger" }
		11 = { "11e régiment étranger" }
		12 = { "12e régiment étranger" }
		13 = { "13e régiment étranger" }
		14 = { "14e régiment étranger" }
		15 = { "15e régiment étranger" }
		16 = { "16e régiment étranger" }
		17 = { "17e régiment étranger" }
		18 = { "18e régiment étranger" }
		19 = { "19e régiment étranger" }
		20 = { "20e régiment étranger" }
		21 = { "21e régiment étranger" }
		22 = { "22e régiment étranger" }
		23 = { "23e régiment étranger" }
		24 = { "24e régiment étranger"' }
		25 = { "25e régiment étranger"' }
		26 = { "26e régiment étranger"' }
		27 = { "27e régiment étranger" }
		28 = { "28e régiment étranger" }
		29 = { "29e régiment étranger" }
		30 = { "30e régiment étranger" }
		31 = { "31e régiment étranger" }
		32 = { "32e régiment étranger" }
		33 = { "33e régiment étranger" }
		34 = { "34e régiment étranger" }
		35 = { "35e régiment étranger" }
		36 = { "36e régiment étranger" }
		37 = { "37e régiment étranger" }
		38 = { "38e régiment étranger" }
		39 = { "39e régiment étranger" }
		40 = { "40e régiment étranger" }
	}
}
