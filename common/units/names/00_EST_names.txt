EST = {
	air_wing_names_template = AIR_WING_NAME_EST_FALLBACK

	#Air wings can only be named through archetype
	small_plane_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC
	}
	small_plane_strike_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	small_plane_cas_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	small_plane_naval_bomber_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	small_plane_suicide_airframe = {
		prefix = ""
		generic = { "Mehitamata Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	medium_plane_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC
		unique = {
			"3 Hävituslennuk" "7 Hävituslennuk" "8 Hävituslennuk"
		}
	}
	medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	medium_plane_suicide_airframe = {
		prefix = ""
		generic = { "Mehitamata Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	cv_medium_plane_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC
	}
	cv_medium_plane_fighter_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	cv_medium_plane_cas_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	cv_medium_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	cv_medium_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transpordilennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	large_plane_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	large_plane_cas_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	large_plane_maritime_patrol_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	large_plane_awacs_airframe = {
		prefix = ""
		generic = { "Hävituslennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	large_plane_air_transport_airframe = {
		prefix = ""
		generic = { "Transpordilennuk" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}
	attack_helicopter_equipment = {
		prefix = ""
		generic = { "Rünnakuhelikopter" }
		generic_pattern = AIR_WING_NAME_EST_GENERIC

	}

}