﻿## SUBJECTS OF RUSSIA FOCUS TREE
## Author: LordBogdanoff
focus_tree = {

	id = russian_subjects_focus_tree

	country = {
		factor = 0
		modifier = {
			add = 20
			OR = {
				original_tag = YAK
				original_tag = BRY
				original_tag = KHS
				original_tag = ADY
				original_tag = CKK
				original_tag = NEE
				original_tag = YAM
				original_tag = ALT
				original_tag = KAE
				original_tag = KOM
				original_tag = TUV
				original_tag = KLM
				original_tag = KBK
				original_tag = DAG
				original_tag = ING
				original_tag = URA
				original_tag = GOR
				original_tag = FAR
				original_tag = SIB
				original_tag = KUB
				original_tag = KCC
				original_tag = CHU
				original_tag = MEL
				original_tag = MOV
				original_tag = UDM
			}
		}
	}

	continuous_focus_position = { x = 2000 y = 2640 }

	focus = {
		id = SUB_start
		icon = constitution
		x = 23
		y = 0

		cost = 0.1

		search_filters = { FOCUS_FILTER_POLITICAL }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_start"
			add_political_power = 100
			hidden_effect = {
			set_country_flag = SOV_subject_agree	
			if = {
				limit = { ROOT = { is_ai = yes } }
				country_event = { id = dpr.6 days = 1 }
			}
			if = { limit = { SOV = { has_country_flag = SOV_united_russia } } 
				set_country_flag = SUB_united_russia
				update_party_name = yes
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_start_industry
		icon = economic_civil_industry
		x = 0
		y = 2
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_start_industry"
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_fight_corruption
		icon = BLR_Anti_Corruption
		x = -8
		y = 2
		relative_position_id = SUB_start
		cost = 10
		

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_fight_corruption"
			decrease_corruption = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_national_battalions
		icon = sov_dobro_bat
		x = -6
		y = 2
		available = {
			ROOT = { is_subject_of = SOV }
			OR = {
				has_war = yes
				SOV = {	has_country_flag = subject_war_prepare }
			}
			has_manpower > 1999
		}
		relative_position_id = SUB_start
		cost = 10
		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_national_battalions"
			division_template = {
				name = "National Battalion"
				regiments = {
					L_Inf_Bat = { x = 0 y = 0 }
					L_Inf_Bat = { x = 0 y = 1 }
				}
				support = {
					L_Recce_Comp = { x = 0 y = 0 }
				}
			}
			russia_battalions = yes
		}
		ai_will_do = { factor = 10 }
	}
	focus = {
		id = SUB_fight_bad_cops
		icon = sov_police
		x = -9
		y = 3
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_fight_corruption }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_fight_bad_cops"
			decrease_corruption = yes
			remove_ideas = SUB_corrupt_police_idea
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.02 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_fight_bandits
		icon = focus_banditus_dominatus
		x = -7
		y = 3
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_fight_corruption }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_fight_bandits"
			remove_ideas = SUB_banditism_idea
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.08 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_infrastructure
		icon = construction_buldozer
		x = -2
		y = 3
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start_industry }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_infrastructure"
			one_random_infrastructure = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_cooperate_rjd
		icon = rjd
		x = -1
		y = 4
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_infrastructure }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_infrastructure"
			one_random_infrastructure = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			add_relative_party_popularity = yes
			set_temp_variable = { percent_change = 3.5 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = ROOT }
			change_influence_percentage = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_airport
		icon = build_new_airports
		x = 0
		y = 3
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start_industry }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_infrastructure"
			one_air_base = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.04 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_tourism
		icon = economic_tourism
		x = -2
		y = 5
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_infrastructure }
		prerequisite = { focus = SUB_selhoz_chastniki }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_tourism"
			add_ideas = SUB_develop_turism
			set_temp_variable = { treasury_change = -0.5 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_free_zone
		icon = grozni_city
		x = -2
		y = 6
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_tourism }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_free_zone"
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.01 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_energy
		icon = BLR_Energy_conflict
		x = 2
		y = 3
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start_industry }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_energy"
			one_random_fossil_fuel_powerplant = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_rosneft
		icon = Rosneft
		x = 2
		y = 4
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_energy }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_rosneft"
			set_temp_variable = { percent_change = 5.11 }
			set_temp_variable = { tag_index = SOV }
			set_temp_variable = { influence_target = ROOT }
			change_influence_percentage = yes
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			random_core_state = {
				add_resource = {
					type = oil
					amount = 2
				}
			}
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_social_invest
		icon = social_democracy_2
		x = 0
		y = 5
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_energy }
		prerequisite = { focus = SUB_infrastructure }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_social_invest"
			increase_social_spending = yes
			add_ideas = SUB_min_obr
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.04 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_obrazovania
		icon = university
		x = 0
		y = 6
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_social_invest }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_obrazovania"
			increase_education_budget = yes
			swap_ideas = {
				remove_idea = SUB_min_obr
				add_idea = SUB_min_obr1
			}
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_selhoz
		icon = mlw_buying_kubotas
		x = -4
		y = 2
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_selhoz"
			add_ideas = SUB_min_selhoz1
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_selhoz_subsidia
		icon = wheat_field
		x = -4
		y = 3
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_selhoz }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_selhoz_subsidia"
			set_temp_variable = { treasury_change = -3.5 }
			modify_treasury_effect = yes
			add_ideas = BRA_idea_pro_farmers
			set_temp_variable = { temp_opinion = 10 }
			change_farmers_opinion = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_selhoz_chastniki
		icon = farming_sector
		x = -4
		y = 4
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_selhoz_subsidia }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_selhoz_chastniki"
			swap_ideas = {
				remove_idea = SUB_min_selhoz1
				add_idea = SUB_min_selhoz2
			}
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.03 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_medicina
		icon = generic_hospital
		x = 4
		y = 3
		relative_position_id = SUB_start
		cost = 10
		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_start }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_medicina"
			increase_healthcare_budget = yes
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.05 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_fight_narcos
		icon = welfare
		x = 4
		y = 5
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_medicina }

		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_fight_narcos"
			remove_ideas = SUB_narcos_idea
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.02 }
			add_relative_party_popularity = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_sport
		icon = ethiopia_maraton_motors
		x = 4
		y = 6
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_fight_narcos }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_sport"
			add_ideas = ARM_sports_develop1
			add_stability = 0.015
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.02 }
			add_relative_party_popularity = yes
			set_temp_variable = { treasury_change = -1 }
			modify_treasury_effect = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_smi_free
		icon = break_free2
		x = 1
		y = 7
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_obrazovania }
		prerequisite = { focus = SUB_sport }
		mutually_exclusive = { focus = SUB_smi_not_free }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_smi_free"
			add_political_power = -50
			add_stability = 0.09
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_more_free_zone
		icon = blr_market_economy
		x = -2
		y = 9
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_free_zone }
		prerequisite = { focus = SUB_smi_free }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_more_free_zone"
			add_popularity = {
				ideology = democratic
				popularity = 0.05
			}
			recalculate_party = yes
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_lgbt
		icon = pro_lgbt
		x = 0
		y = 9
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_smi_free }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_lgbt"
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.03 }
			add_relative_party_popularity = yes
			add_ideas = SPR_pro_lgbtqa_stance_idea
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_smi_not_free
		icon = anti_pacifism
		x = 3
		y = 7
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_obrazovania }
		prerequisite = { focus = SUB_sport }
		mutually_exclusive = { focus = SUB_smi_free }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SSUB_smi_not_free"
			add_political_power = 50
			add_stability = -0.09
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_no_lgbt
		icon = anti_lgbt
		x = 3
		y = 9
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_smi_not_free }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_no_lgbt"
			set_party_index_to_ruling_party = yes
			set_temp_variable = { party_popularity_increase = 0.09 }
			add_relative_party_popularity = yes
			add_ideas = SPR_anti_lgbtqa_stance_idea
		}
		ai_will_do = { factor = 100 }
	}
	focus = {
		id = SUB_zavods
		icon = factory_facto
		x = 5
		y = 9
		relative_position_id = SUB_start
		cost = 10

		search_filters = { FOCUS_FILTER_POLITICAL }
		prerequisite = { focus = SUB_smi_not_free }
		
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus SUB_no_lgbt"
			two_random_industrial_complex = yes
		}
		ai_will_do = { factor = 100 }
	}
}



