#Made by Lord Bogdanoff
focus_tree = {
	id = vojvodina_focus
	shared_focus = USoE001
	shared_focus = POTEF001
	country = {
		factor = 0
		modifier = {
			add = 20
			original_tag = VOJ
		}
	}
	continuous_focus_position = { x = 1000 y = 1200 }
	#Madyar Start
	focus = {
		id = VOJ_madyar
		icon = voj_hung_maj
		x = 17
		y = 0
		ai_will_do = { factor = 20 }
		cost = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_madyar"
			add_political_power = 150
			set_temp_variable = { modify_voj_separ = 2 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_new_nation
		icon = voj_automony
		x = 0
		y = 1
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_madyar }
		cost = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_new_nation"
			add_timed_idea = {
				idea = VOJ_newnation_born
				days = 210
			}
			set_temp_variable = { modify_voj_separ = 5 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_we_hungary
		icon = voj_we_hungary
		x = -1
		y = 2
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_new_nation }
		cost = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_we_hungary"
			set_temp_variable = { modify_voj_separ = 5 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = -5 }
			modify_voj_integr_support = yes
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = HUN }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
			remove_ideas = VOJ_no_nation
		}
	}
	focus = {
		id = VOJ_we_not_serb
		icon = voj_anti_serb
		x = 1
		y = 2
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_new_nation }
		cost = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_we_not_serb"
			set_temp_variable = { modify_voj_separ = 5 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = -5 }
			modify_voj_integr_support = yes
			set_temp_variable = { percent_change = -4 }
			set_temp_variable = { tag_index = SER }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
			reverse_add_opinion_modifier = {
				target = SER
				modifier = recent_actions_positive
			}
			add_opinion_modifier = {
				target = SER
				modifier = recent_actions_positive
			}
			add_ideas = VOJ_antiserb
		}
	}
	focus = {
		id = VOJ_propagandase
		icon = voj_patriotism_hun
		x = 0
		y = 3
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_we_not_serb }
		prerequisite = { focus = VOJ_we_hungary }
		cost = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_propagandase"
			add_ideas = VOJ_vojvodina_nation
			set_temp_variable = { modify_voj_separ = 10 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = -5 }
			modify_voj_integr_support = yes
		}
	}
	focus = {
		id = VOJ_defences
		icon = Generic_Soldiers_Defense
		x = -2
		y = 4
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_propagandase }
		cost = 5
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_defences"
			set_temp_variable = { treasury_change = -2 }
			modify_treasury_effect = yes
			130 = {
				add_building_construction = {
					type = bunker
					province = 11580
					level = 2
					instant_build = yes
				}
			}
			998 = {
				add_building_construction = {
					type = bunker
					province = 614
					level = 2
					instant_build = yes
				}
			}
		}
	}
	focus = {
		id = VOJ_mobilisation
		icon = army_mobilisation1
		x = 0
		y = 4
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_propagandase }
		cost = 5
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_mobilisation"
			add_manpower = 2000
		}
	}
	focus = {
		id = VOJ_guard
		icon = voj_own_army
		x = 2
		y = 4
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_propagandase }
		cost = 7
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_guard"
			set_temp_variable = { modify_voj_separ = 5 }
			modify_voj_separ_support = yes
			division_template = {
				name = "National Guard"
				regiments = {
					L_Inf_Bat = { x = 0 y = 0 }
					L_Inf_Bat = { x = 0 y = 1 }
				}
			}
			add_manpower = -500
			set_temp_variable = { treasury_change = -3 }
			modify_treasury_effect = yes
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 130 }
				create_unit = {
					division = "name = \"National Guard Battalion\" division_template = \"National Battalion\" start_experience_factor = 0.1"
					owner = VOJ
				}
			}
		}
	}
	focus = {
		id = VOJ_path
		icon = voj_federalism
		x = 0
		y = 5
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_guard }
		prerequisite = { focus = VOJ_mobilisation }
		prerequisite = { focus = VOJ_defences }
		cost = 5
		available = {
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_guard"
			add_political_power = 100
		}
	}
	focus = {
		id = VOJ_part_hungary
		icon = voj_part_hungary
		x = -2
		y = 6
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_path }
		cost = 5
		available = {
			country_exists = HUN
			is_subject = no
			nationalist_fascist_are_in_power = yes
		}
		mutually_exclusive = { focus = VOJ_nopart_hungary }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_part_hungary"
			HUN = { country_event = ser_voj.1 }
		}
	}
	focus = {
		id = VOJ_nopart_hungary
		icon = voj_anti_unite_hun
		x = 2
		y = 6
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_path }
		cost = 7
		available = {
			is_subject = no
			nationalist_fascist_are_in_power = yes
		}
		mutually_exclusive = { focus = VOJ_part_hungary }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_nopart_hungary"
			swap_ideas = {
				remove_idea = VOJ_vojvodina_nation
				add_idea = VOJ_vojvodina_nation1
			}
			set_cosmetic_tag = VOJ_hungary
			clr_country_flag = dynamic_flag
		}
	}
	focus = {
		id = VOJ_volunteer
		icon = voj_hun_volunteer
		x = 1
		y = 7
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_nopart_hungary }
		cost = 5
		available = {
			is_subject = no
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_volunteer"
			add_manpower = 4000
		}
	}
	focus = {
		id = VOJ_claims
		icon = voj_hun_claims
		x = 3
		y = 7
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_nopart_hungary }
		cost = 7
		available = {
			is_subject = no
			nationalist_fascist_are_in_power = yes
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_claims"
			add_state_claim = 122
			add_state_claim = 121
			add_state_claim = 123
			add_state_claim = 999
		}
	}
	focus = {
		id = VOJ_war
		icon = voj_war_hungary
		x = 2
		y = 8
		relative_position_id = VOJ_madyar
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_claims }
		prerequisite = { focus = VOJ_volunteer }
		cost = 7
		available = {
			is_subject = no
			country_exists = HUN
			nationalist_fascist_are_in_power = yes
		}
		will_lead_to_war_with = HUN
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_war"
			create_wargoal = {
				type = puppet_wargoal_focus
				target = HUN
			}
		}
	}
	#Pro-Serbia
	focus = {
		id = VOJ_serbs
		icon = serb_majority
		x = 7
		y = 0
		ai_will_do = { factor = 20 }
		cost = 7
		available = {
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_serbs"
			add_political_power = 150
			set_temp_variable = { modify_voj_separ = -2 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_anti_separ
		icon = voj_anti_unite_hun
		x = -1
		y = 1
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_serbs }
		cost = 5
		available = {
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_anti_separ"
			set_temp_variable = { modify_voj_separ = -5 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_propagandais
		icon = SER_populism
		x = 1
		y = 1
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_serbs }
		cost = 5
		available = {
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_propagandais"
			set_temp_variable = { modify_voj_integr = 5 }
			modify_voj_integr_support = yes
		}
	}
	focus = {
		id = VOJ_serb_infra
		icon = voj_war_hungary
		x = 0
		y = 2
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_propagandais }
		prerequisite = { focus = VOJ_anti_separ }
		cost = 7
		available = {
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_serb_infra"
			one_random_infrastructure = yes
			set_temp_variable = { modify_voj_separ = -2 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = 2 }
			modify_voj_integr_support = yes
		}
	}
	focus = {
		id = VOJ_serb_people
		icon = flag_serbia
		x = 0
		y = 3
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_serb_infra }
		cost = 7
		available = {
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_serb_people"
			add_political_power = 100
			one_random_infrastructure = yes
			set_temp_variable = { modify_voj_separ = -5 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = 5 }
			modify_voj_integr_support = yes
			remove_ideas = VOJ_no_nation
		}
	}
	focus = {
		id = VOJ_serb_invest
		icon = gen_coins_n
		x = -1
		y = 4
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_serb_people }
		cost = 7
		available = {
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_serb_invest"
			set_temp_variable = { modify_voj_separ = -5 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = 5 }
			modify_voj_integr_support = yes
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
			set_temp_variable = { percent_change = 3 }
			set_temp_variable = { tag_index = SER }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
		}
	}
	focus = {
		id = VOJ_serb_police
		icon = ser_police
		x = 1
		y = 4
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_serb_people }
		cost = 7
		available = {
			SER = { has_completed_focus = SER_police }
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_serb_police"
			set_temp_variable = { modify_voj_separ = -2 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = 2 }
			modify_voj_integr_support = yes
			add_stability = 0.10
			add_ideas = VOJ_ser_police
			set_temp_variable = { percent_change = 1 }
			set_temp_variable = { tag_index = SER }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
		}
	}
	focus = {
		id = VOJ_serb_military
		icon = zastava_sa
		x = 0
		y = 5
		relative_position_id = VOJ_serbs
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_serb_invest }
		prerequisite = { focus = VOJ_serb_police }
		cost = 7
		available = {
			SER = { has_completed_focus = SER_avto }
			is_subject_of = SER
			OR = {
				nationalist_right_wing_populists_are_in_power = yes
				neutrality_neutral_oligarch_are_in_power = yes
			}
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_serb_military"
			add_ideas = VOJ_ser_avto
			one_random_arms_factory = yes
			set_temp_variable = { percent_change = 1 }
			set_temp_variable = { tag_index = SER }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
		}
	}
	#Separ Start
	focus = {
		id = VOJ_separ
		icon = voj_socdem
		x = 12
		y = 0
		ai_will_do = { factor = 20 }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_separ"
			add_political_power = 150
			set_temp_variable = { modify_voj_separ = 2 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_vojvodina_spirit
		icon = voj_automony
		x = 0
		y = 1
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_separ }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_vojvodina_spirit"
			add_ideas = VOJ_patriotism
			set_temp_variable = { modify_voj_separ = 7 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = -5 }
			modify_voj_integr_support = yes
			remove_ideas = VOJ_no_nation
		}
	}
	focus = {
		id = VOJ_europe_friend
		icon = align_to_europe
		x = -2
		y = 2
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_vojvodina_spirit }
		cost = 5
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_europe_friend"
			every_country = {
				limit = {
					is_in_europe = yes
					has_government = democratic
				}
				add_opinion_modifier = {
					target = VOJ
					modifier = recent_actions_positive
				}
			}
			set_temp_variable = { treasury_change = 3.50 }
			modify_treasury_effect = yes
		}
	}
	focus = {
		id = VOJ_brothers_nation
		icon = voj_hun_cro
		x = 0
		y = 2
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_vojvodina_spirit }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_brothers_nation"
			reverse_add_opinion_modifier = {
				target = CRO
				modifier = recent_actions_positive
			}
			add_opinion_modifier = {
				target = CRO
				modifier = recent_actions_positive
			}
			reverse_add_opinion_modifier = {
				target = HUN
				modifier = recent_actions_positive
			}
			add_opinion_modifier = {
				target = HUN
				modifier = recent_actions_positive
			}
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = HUN }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
			set_temp_variable = { percent_change = 5 }
			set_temp_variable = { tag_index = CRO }
			set_temp_variable = { influence_target = VOJ }
			change_influence_percentage = yes
			one_random_industrial_complex = yes
			one_random_fossil_fuel_powerplant = yes
		}
	}
	focus = {
		id = VOJ_kosovo
		icon = align_to_kosovo
		x = 2
		y = 2
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_vojvodina_spirit }
		cost = 7
		available = {
			country_exists = KOS
			has_government = democratic
		}
		bypass = { NOT = { country_exists = KOS } }
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_kosovo"
			reverse_add_opinion_modifier = {
				target = KOS
				modifier = recent_actions_positive
			}
			add_opinion_modifier = {
				target = KOS
				modifier = recent_actions_positive
			}
			KOS = { country_event = ser_voj.2 }
		}
	}
	focus = {
		id = VOJ_propaganda
		icon = propaganda
		x = 0
		y = 3
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_kosovo }
		prerequisite = { focus = VOJ_brothers_nation }
		prerequisite = { focus = VOJ_europe_friend }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_propaganda"
			swap_ideas = {
				remove_idea = VOJ_patriotism
				add_idea = VOJ_patriotism1
			}
			set_temp_variable = { modify_voj_separ = 5 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = -5 }
			modify_voj_integr_support = yes
			add_political_power = 50
		}
	}
	focus = {
		id = VOJ_self_defence
		icon = voj_own_army
		x = -1
		y = 4
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_propaganda }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_self_defence"
			swap_ideas = {
				remove_idea = VOJ_patriotism1
				add_idea = VOJ_patriotism2
			}
			set_temp_variable = { modify_voj_separ = 3 }
			modify_voj_separ_support = yes
			division_template = {
				name = "People's Militia Battalion"
					regiments = {
						Militia_Bat = { x = 0 y = 0 }
					}
			}
			random_owned_controlled_state = {
				limit = { ROOT = { has_full_control_of_state = PREV } }
				prioritize = { 130 }
				create_unit = {
					division = "name = \"People's Militia Battalion\" division_template = \"People's Militia Battalion\" start_experience_factor = 0.1"
					owner = VOJ
				}
			}
		}
	}
	focus = {
		id = VOJ_buy_weapon
		icon = army_more_ak47
		x = 1
		y = 4
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_propaganda }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_buy_weapon"
			set_temp_variable = { treasury_change = -4 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = 500
				producer = CRO
			}
			add_equipment_to_stockpile = {
				type = infantry_weapons
				amount = 500
				producer = HUN
			}
			reverse_add_opinion_modifier = {
				target = CRO
				modifier = improve_trade
			}
			add_opinion_modifier = {
				target = CRO
				modifier = improve_trade
			}
			reverse_add_opinion_modifier = {
				target = HUN
				modifier = improve_trade
			}
			add_opinion_modifier = {
				target = HUN
				modifier = improve_trade
			}
		}
	}
	focus = {
		id = VOJ_anti_serb
		icon = voj_anti_serb
		x = 0
		y = 5
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_buy_weapon }
		prerequisite = { focus = VOJ_self_defence }
		cost = 7
		available = {
			has_government = democratic
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_anti_serb"
			set_temp_variable = { modify_voj_separ = 10 }
			modify_voj_separ_support = yes
			set_temp_variable = { modify_voj_integr = -5 }
			modify_voj_integr_support = yes
			reverse_add_opinion_modifier = {
				target = SER
				modifier = recent_actions_positive
			}
			add_opinion_modifier = {
				target = SER
				modifier = recent_actions_positive
			}
		}
	}
	focus = {
		id = VOJ_war_freedom
		icon = war_against_serbia
		x = 0
		y = 6
		relative_position_id = VOJ_separ
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_anti_serb }
		cost = 7
		available = {
			is_subject = no
			country_exists = SER
			has_government = democratic
		}
		will_lead_to_war_with = SER
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_war_freedom"
			create_wargoal = {
				type = puppet_wargoal_focus
				target = SER
			}
		}
	}
	#Army of Vojvodina
	focus = {
		id = VOJ_start_army
		icon = voj_own_army
		x = 21
		y = 0
		ai_will_do = { factor = 20 }
		cost = 7
		available = {
			NOT = { has_idea = SER_subject_idea }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_start_army"
			add_political_power = 50
			add_command_power = 20
		}
	}
	focus = {
		id = VOJ_reforms_army
		icon = army_mobilisation
		x = -1
		y = 1
		relative_position_id = VOJ_start_army
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_start_army }
		cost = 7
		available = {
			NOT = { has_idea = SER_subject_idea }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_reforms_army"
			swap_ideas = {
				remove_idea = VOJ_no_army
				add_idea = VOJ_no_army1
			}
		}
	}
	focus = {
		id = VOJ_weapon_zakup
		icon = army_more_ak47
		x = 1
		y = 1
		relative_position_id = VOJ_start_army
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_start_army }
		cost = 7
		available = {
			NOT = { has_idea = SER_subject_idea }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_weapon_zakup"
			set_temp_variable = { treasury_change = -12.5 }
			modify_treasury_effect = yes
			add_equipment_to_stockpile = {
				type = infantry_weapons2
				amount = -1000
				producer = GER
			}
			add_equipment_to_stockpile = {
				type = util_vehicle_equipment
				amount = -50
				producer = USA
			}
			add_equipment_to_stockpile = {
				type = AA_Equipment
				amount = -9
				producer = SOV
			}
			add_equipment_to_stockpile = {
				type = L_AT_Equipment
				amount = -15
				producer = SPR
			}
			add_equipment_to_stockpile = {
				type = command_control_equipment2
				amount = -250
				producer = FRA
			}
		}
	}
	focus = {
		id = VOJ_new_officer
		icon = army_general_stuff3
		x = 0
		y = 2
		relative_position_id = VOJ_start_army
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_weapon_zakup }
		prerequisite = { focus = VOJ_reforms_army }
		cost = 7
		available = {
			NOT = { has_idea = SER_subject_idea }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_new_officer"
			swap_ideas = {
				remove_idea = VOJ_no_army1
				add_idea = VOJ_no_army2
			}
		}
	}
	focus = {
		id = VOJ_inter_specialist
		icon = army_planning
		x = 0
		y = 3
		relative_position_id = VOJ_start_army
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_new_officer }
		cost = 7
		available = {
			NOT = { has_idea = SER_subject_idea }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_inter_specialist"
			swap_ideas = {
				remove_idea = VOJ_no_army2
				add_idea = VOJ_no_army3
			}
		}
	}
	focus = {
		id = VOJ_finish_reform
		icon = army_upgrade
		x = 0
		y = 4
		relative_position_id = VOJ_start_army
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_inter_specialist }
		cost = 7
		available = {
			NOT = { has_idea = SER_subject_idea }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_finish_reform"
			remove_ideas = VOJ_no_army3
		}
	}
	#Federalisation Start
	focus = {
		id = VOJ_regionalism
		icon = KHM_democratic_coalition
		x = 25
		y = 0
		ai_will_do = { factor = 20 }
		cost = 1
		available = {
			VOJ = {	has_autonomy_state = autonomy_autonomous_province }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_regionalism"
			set_temp_variable = { percent_change = 5 }
			change_domestic_influence_percentage = yes
			set_temp_variable = { modify_voj_separ = 1 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_federalisation
		icon = voj_federalism
		x = 0
		y = 1
		relative_position_id = VOJ_regionalism
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_regionalism }
		cost = 7
		available = {
			VOJ = {	has_autonomy_state = autonomy_autonomous_province }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_federalisation"
			add_political_power = 150
			set_temp_variable = { modify_voj_separ = 5 }
			modify_voj_separ_support = yes
		}
	}
	focus = {
		id = VOJ_vs
		icon = voj_own_army
		x = -1
		y = 2
		relative_position_id = VOJ_regionalism
		ai_will_do = { factor = 20 }
		available = {
			VOJ = {	has_autonomy_state = autonomy_autonomous_province1 }
		}
		prerequisite = { focus = VOJ_federalisation }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_vs"
			SER = {
				country_event = {
					id = SerbiaFocus.104
					days = 1
				}
			}
		}
	}
	focus = {
		id = VOJ_autonomy
		icon = voj_automony
		x = 1
		y = 2
		relative_position_id = VOJ_regionalism
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_federalisation }
		cost = 7
		available = {
			VOJ = {	has_autonomy_state = autonomy_autonomous_province }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_autonomy"
			SER = {
				country_event = {
					id = SerbiaFocus.109
					days = 1
				}
			}
		}
	}
	focus = {
		id = VOJ_true_fed
		icon = voj_federation
		x = 0
		y = 3
		relative_position_id = VOJ_regionalism
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_autonomy }
		prerequisite = { focus = VOJ_vs }
		cost = 7
		available = {
			VOJ = {	has_autonomy_state = autonomy_autonomous_province1 }
		}
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_true_fed"
			SER = {
				country_event = {
					id = SerbiaFocus.101
					days = 1
				}
			}
		}
	}
	#Economic Start
	focus = {
		id = VOJ_economic
		icon = balance_buget
		x = 24
		y = 5
		ai_will_do = { factor = 20 }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_economic"
			increase_economic_growth = yes
		}
	}
	focus = {
		id = VOJ_selhoz
		icon = agriculture
		x = -2
		y = 1
		relative_position_id = VOJ_economic
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_economic }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_selhoz"
			set_temp_variable = { temp_opinion = 10 }
			change_landowners_opinion = yes
			add_ideas = VOJ_agro_selhoz
		}
	}
	focus = {
		id = VOJ_construct
		icon = economic_civil_industry
		x = 0
		y = 1
		relative_position_id = VOJ_economic
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_economic }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_construct"
			set_temp_variable = { treasury_change = -7.5 }
			modify_treasury_effect = yes
			capital_scope = {
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
				add_building_construction = {
					type = fossil_powerplant
					level = 1
				}
			}
			if = {
				limit = { num_of_factories > 25 }
				one_random_industrial_complex = yes
			}
		}
	}
	focus = {
		id = VOJ_oil
		icon = oil_trade
		x = 2
		y = 1
		relative_position_id = VOJ_economic
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_economic }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_oil"
			add_ideas = VOJ_prom_idea
		}
	}
	focus = {
		id = VOJ_chim
		icon = scientific_exchange
		x = -1
		y = 2
		relative_position_id = VOJ_economic
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_construct }
		prerequisite = { focus = VOJ_selhoz }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_chim"
			swap_ideas = {
				remove_idea = VOJ_agro_selhoz
				add_idea = VOJ_agro_selhoz1
			}
		}
	}
	focus = {
		id = VOJ_metal
		icon = modern_refinery_industry
		x = 1
		y = 2
		relative_position_id = VOJ_economic
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_construct }
		prerequisite = { focus = VOJ_oil }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_metal"
			set_temp_variable = { treasury_change = -0.95 }
			modify_treasury_effect = yes
			add_resource = {
				type = steel
				amount = 2
				state = 130
			}
			swap_ideas = {
				remove_idea = VOJ_prom_idea
				add_idea = VOJ_prom_idea1
			}
		}
	}
	focus = {
		id = VOJ_it
		icon = economic_it
		x = 0
		y = 3
		relative_position_id = VOJ_economic
		ai_will_do = { factor = 20 }
		prerequisite = { focus = VOJ_chim }
		prerequisite = { focus = VOJ_metal }
		cost = 7
		completion_reward = {
			log = "[GetDateText]: [Root.GetName]: Focus VOJ_it"
			add_tech_bonus = {
				name = BLR_Digitalization
				bonus = 0.55
				uses = 2
				category = CAT_internet_tech
			}
			set_temp_variable = { treasury_change = -4.60 }
			modify_treasury_effect = yes
			add_popularity = {
				ideology = democratic
				popularity = 0.1
			}
			recalculate_party = yes
		}
	}
}