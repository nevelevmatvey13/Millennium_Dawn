#	Example:
#
#	example_dynamic_modifier = {
#		icon = "GFX_idea_unknown" # optional, will show up in guis if icon is specified
#		enable = { always = yes } #optional, the modifier won't apply if not enabled
#		remove_trigger = { always = no } #optional, will remove the modifier if true
#
#		# list of modifiers
#		fuel_cost = 321
#		max_fuel = var_max_fuel # will be taken from a variable
#	}
#
#
#	In a script file:
#
#	effect = {
#		add_dynamic_modifier = {
#			modifier = example_dynamic_modifier
#			scope = GER # optional, if you specify this your dynamic modifier scoped to this scope (root is the effect scope)
#			days = 14 # optional, will be removed after this many days passes
#		}
#	}
#
#	can be added to countries, states or unit leaders
#	will only updated daily, unless forced by force_update_dynamic_modifier effect

sabotaged_resources = {
	remove_trigger = {
		has_resistance = no
	}

	icon = GFX_modifiers_sabotaged_resource

	temporary_state_resource_oil = sabotaged_oil
	temporary_state_resource_aluminium = sabotaged_aluminium
	temporary_state_resource_tungsten = sabotaged_tungsten
	temporary_state_resource_steel = sabotaged_steel
}

autonomous_state = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource

	recruitable_population_factor = -0.5
	local_building_slots_factor = -0.25
	state_resources_factor = -0.25
	state_production_speed_buildings_factor = -0.25
}

semi_autonomous_state = {
	enable = { always = yes }

	icon = GFX_modifiers_sabotaged_resource

	local_building_slots_factor = -0.25
	state_resources_factor = -0.25
	state_production_speed_buildings_factor = -0.25
}

generic_five_year_plan_dynamic_modifier = {
	enable = { always = yes }

	remove_trigger = {
		emerging_communist_state_are_in_power = no
		neutrality_neutral_communism_are_in_power = no
	}

	icon = GFX_idea_central_management

	political_power_factor = five_year_plan_political_power_factor
	production_speed_infrastructure_factor = five_year_plan_infra_factor
	production_speed_air_base_factor = five_year_plan_air_base_factor
	production_speed_anti_air_building_factor = five_year_plan_anti_air_factor
	production_speed_fuel_silo_factor = five_year_plan_fuel_reserve_factor
	production_speed_radar_station_factor = five_year_plan_intelligence_station_factor
	production_speed_internet_station_factor = five_year_plan_mobile_radio_mast_factor
	production_speed_arms_factory_factor = five_year_plan_MIC_factor
	production_speed_industrial_complex_factor = five_year_plan_CIC_factor
	production_speed_offices_factor = five_year_plan_office_factor
	production_speed_dockyard_factor = five_year_plan_NIC_factor
	production_speed_synthetic_refinery_factor = five_year_plan_refinery_factor
	production_speed_naval_base_factor = five_year_plan_naval_base_factor
	production_speed_bunker_factor = five_year_plan_land_bunker_factor
	production_speed_coastal_bunker_factor = five_year_plan_coastal_bunker_factor
	production_speed_agriculture_district_factor = five_year_plan_ag_districts_factor
	local_resources_factor = five_year_plan_resources_factor
}

government_popularity_modifier = {
	enable = { always = yes }

	stability_factor = gov_pop_stability_factor
	political_power_factor = gov_pop_political_power_factor
	drift_defence_factor = gov_pop_drift_defence_factor
}

influence_drift_modifier = {
	enable = { always = yes }

	nationalist_drift = nationalist_drift_influence_var
	fascism_drift = fascism_drift_influence_var
	neutrality_drift = neutrality_drift_influence_var
	communism_drift = communism_drift_influence_var
	democratic_drift = democratic_drift_influence_var
}

cartel_penalties = {
	icon = "GFX_idea_cartels"
	enable = {
		is_cartel_nation = yes
		NOT = { has_country_flag = CARTEL_defeated_the_cartels }
	}

	political_power_factor = CAR_political_power_factor # -0.25
	corruption_cost_factor = CAR_corruption_cost_factor # -1.00
	industry_free_repair_factor = CAR_industry_free_repair_factor # = -0.5
	stability_factor = CAR_stability_factor # -0.20
	MONTHLY_POPULATION = CAR_monthly_population # -0.20
	army_morale_factor = CAR_army_morale_factor # -0.10
	army_org_factor = CAR_army_org_factor # -0.10
	tax_gain_multiplier_modifier = CAR_tax_gain_multiplier_modifier # -0.50
	neutrality_drift = CAR_neutrality_drift
}

narco_state_bonuses = {
	icon = "GFX_idea_cartels"
	enable = {
		is_cartel_nation = yes
	}

	political_power_factor = CAR_political_power_factor # 30%
	corruption_cost_factor = CAR_corruption_cost_factor # 150%
	stability_factor = CAR_stability_factor # 10%
	army_morale_factor = CAR_army_morale_factor # 10%
	army_org_factor = CAR_army_org_factor # 10%
	tax_gain_multiplier_modifier = CAR_tax_gain_multiplier_modifier # -50%
	neutrality_drift = CAR_neutrality_drift # 0.20
	foreign_influence_modifier = CAR_foreign_influence_modifier # 10%
	custom_modifier_tooltip = CAR_additional_income_drug_trade_tt
}

neighbor_effect_modifiers = {
	enable = { always = yes }

	# Stability from Unstable Neighbors
	stability_factor = stability_factor_neighbor_var

	# Drifts from Neighbors
	nationalist_drift = nationalist_drift_neighbor_var
	fascism_drift = fascism_drift_neighbor_var
	neutrality_drift = neutrality_drift_neighbor_var
	communism_drift = communism_drift_neighbor_var
	democratic_drift = democratic_drift_neighbor_var
}

public_war_weariness_effect = {
	enable = { always = yes }

	war_support_factor = war_support_factor_var
}

auto_influencer_cost_effect = {
	enable = { always = yes }

	political_power_cost = auto_influence_cost
}

migration_rate_impact_effect = {
	enable = { always = yes }

	monthly_population = net_immigration_rate
}
