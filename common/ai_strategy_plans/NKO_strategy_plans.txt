NKO_UN_RULES = {
	allowed = { original_tag = NKO }
	name = "Orthodox Successor"
	desc = "Behaviour for Orthodox Successor"

	enable = {
		original_tag = NKO
		has_global_flag = NKO_UN_RULES_FOCUS_PATH
	}

	abort = { is_subject = yes }

	ai_national_focuses = {
		NKO_Struggle_for_Power
		NKO_Orthodox_Successor
		NKO_Promote_Self_Reliance
		NKO_Ostracize_Kim_Jong_nam
		NKO_Bribe_KPA_seniors
		NKO_Oppose_to_Chinese_Reform_Road
		NKO_Discredit_Jang_Seong_taek
		NKO_Kim_Jong_un
		NKO_Consolidate_power
		NKO_Light_Industry_Develop
		NKO_Improvement_of_People_live
		NKO_People_support
		NKO_Criticize_Ineptitude
		NKO_Centralize_Power
		NKO_Byungjin
		NKO_Scale_Back_Spending
		NKO_State_owned_Enterprises_Competition
		NKO_Encourage_Consumption
		NKO_Resorts_and_Tourism
		NKO_Samjiyon
		NKO_Loosen_Grip_on_SOEs
		NKO_Formalize_Power_Structures
		NKO_Root_out_Corruption
		NKO_Assassinate_Kim_Jong-Nam
		NKO_Civilian_Control_of_the_Economy
		NKO_Appoint_Loyalists
		NKO_Party_Unchecked
		NKO_Incentive-Based_Economy
		NKO_Weaken_the_Donju
		NKO_Eternal_Juche_sasang
		NKO_Encourage_Foreign_Investment
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

NKO_NAM_RULES = {
	allowed = { original_tag = NKO }
	name = "The Great Change"
	desc = "Behaviour for The Great Change"

	enable = {
		original_tag = NKO
		has_global_flag = NKO_NAM_RULES_FOCUS_PATH
	}

	abort = { is_subject = yes }

	ai_national_focuses = {
		NKO_Struggle_for_Power
		NKO_Call_for_Change
		NKO_Pull_in_young_Military_Officers
		NKO_Stir_up_Father-son_Relations
		NKO_Frame_Opposition
		NKO_Place_Loyalists_in_KPA
		NKO_Finding_Reformist_of_Party
		NKO_Kim_Jong_Nam
		NKO_Embrace_the_people
		NKO_Loosen_Information_monopoly
		NKO_Byungjin
		NKO_State_owned_Enterprises_Competition
		NKO_Scale_Back_Spending
		NKO_Face_world
		NKO_Encourage_Consumption
		NKO_Conciliatory_Rhetoric
		NKO_Diplomatic_Overtures
		NKO_Tone_Down_Propaganda
		NKO_step_up_joint_Ventures
		NKO_Resorts_and_Tourism
		NKO_End_forced_Labor
		NKO_Samjiyon
		NKO_Loosen_Grip_on_SOEs
		NKO_Preserve_the_Party
		NKO_Expand_SEZs
		NKO_Incentive-Based_Economy
		NKO_Empower_the_Donju
		NKO_Appeal_to_the_Youth
		NKO_Encourage_Foreign_Investment
		NKO_End_Juche
		NKO_Foreign_Knowledge
		NKO_The_Choson_Dream
		NKO_Privatize_Means_of_Production
		NKO_Join_the_WTO
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

NKO_JUNTA_RULES = {
	allowed = { original_tag = NKO }
	name = "Junta Coup"
	desc = "Behaviour for Junta Coup"

	enable = {
		original_tag = NKO
		has_global_flag = NKO_JUNTA_RULES_FOCUS_PATH
	}

	abort = { is_subject = yes }

	ai_national_focuses = {
		NKO_Junta
		NKO_Preserve_Songun
		NKO_Smash_Black_Market
		NKO_Determining_the_loyalty_of_army
		NKO_Entrench_Military_Authority
		NKO_Purge_Dissenters
		NKO_Crush_the_Kims
		NKO_Constrain_the_Donju
		NKO_High_pressure_politics
		NKO_Encourage_private_enterprise
		NKO_Empower_the_State_Security_Department
		NKO_CHI_military_support
		NKO_secure_chinese_assistance_in_crushing_dissent
		NKO_request_chinese_protection
		NKO_Market-based_economy
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}
}

#NKO_YOJONG_RULES = {
#	allowed = { original_tag = NKO }
#	name = "Kim Yo-jong"
#	desc = "Behaviour for Kim Yo-jong"
#
#	enable = {
#		original_tag = NKO
#		has_global_flag = NKO_YOJONG_RULES_FOCUS_PATH
#	}
#
#	abort = {
#		is_subject = yes
#	}
#
#	ai_national_focuses = {
#		NKO_kim_yo_jong
#		NKO_juche_for_new_era
#		NKO_agricultural_reform
#		NKO_self_employed_agriculture
#		NKO_decrease_agricultural_tax
#		NKO_for_well_being_of_people
#		NKO_quality_first
#		NKO_chosun_dream
#		NKO_for_peace_of_people
#		NKO_inter_korean_summit
#		NKO_affirmative_politics
#		NKO_women_in_grassroots_cadres
#		NKO_women_in_workers
#		NKO_promote_younger_cadres
#		NKO_close_DMZ
#		NKO_cease_propaganda_broadcasts_yo_jong
#		NKO_conciliatory_rhetoric
#		NKO_abolish_life_time_system_for_Cadres_in_leadership
#		NKO_grassroots_self_governance_system
#		NKO_market_economy_system_pilot
#		NKO_privatize_civil_industry
#		NKO_superiority_of_DPRK_model
#		NKO_offer_negotiations_yo_jong
#		NKO_collective_leadership_system
#		NKO_strong_prosperous_nation
#		NKO_chinese_style_democracy
#		NKO_complementary_to_state_owned_economy
#		NKO_affluent_society
#		NKO_propose_north_led_unification_yo_jong
#		NKO_national_ownership_reform
#		NKO_allow_donju_to_join_WPK
#		NKO_jucheised_market_economy
#		NKO_intra_party_democracy
#		NKO_pave_the_way_for_reunification_yo_jong
#		NKO_happiest_people_in_world
#	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
#	weight = {
#		factor = 1.0
#		modifier = {
#			factor = 1.0
#		}
#	}
#}