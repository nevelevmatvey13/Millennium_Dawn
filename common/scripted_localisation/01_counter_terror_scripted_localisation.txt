defined_text = {
	name = threat_level_flag
	text = { trigger = { has_country_flag = threat_level_negligible }
		localization_key = threat_level_negligible_loc
	}
	text = { trigger = { has_country_flag = threat_level_low }
		localization_key = threat_level_low_loc
	}
	text = { trigger = { has_country_flag = threat_level_moderate }
		localization_key = threat_level_moderate_loc
	}
	text = { trigger = { has_country_flag = threat_level_substantial }
		localization_key = threat_level_substantial_loc
	}
	text = { trigger = { has_country_flag = threat_level_severe }
		localization_key = threat_level_severe_loc
	}
	text = { trigger = { has_country_flag = threat_level_critical }
		localization_key = threat_level_critical_loc
	}
}

defined_text = {
	name = radicalization_loc
	text = { trigger = { check_variable = { radicalization < 10 } }
		localization_key = radicalization_negligible
	}
	text = {
		trigger = {
			check_variable = { radicalization > 9 }
			check_variable = { radicalization < 30 }
		}
		localization_key = radicalization_low
	}
	text = {
		trigger = {
			check_variable = { radicalization > 29 }
			check_variable = { radicalization < 50 }
		}
		localization_key = radicalization_significant
	}
	text = {
		trigger = {
			check_variable = { radicalization > 49 }
		}
		localization_key = radicalization_high
	}
}

defined_text = {
	name = LBA_threat_level_flag

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { has_country_flag = threat_level_negligible }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { has_country_flag = threat_level_negligible }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { has_country_flag = threat_level_negligible }
			}
		}
		localization_key = threat_level_negligible
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { has_country_flag = threat_level_low }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { has_country_flag = threat_level_low }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { has_country_flag = threat_level_low }
			}
		}
		localization_key = threat_level_low
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { has_country_flag = threat_level_moderate }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { has_country_flag = threat_level_moderate }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { has_country_flag = threat_level_moderate }
			}
		}
		localization_key = threat_level_moderate
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { has_country_flag = threat_level_substantial }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { has_country_flag = threat_level_substantial }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { has_country_flag = threat_level_substantial }
			}
		}
		localization_key = threat_level_substantial
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { has_country_flag = threat_level_severe }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { has_country_flag = threat_level_severe }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { has_country_flag = threat_level_severe }
			}
		}
		localization_key = threat_level_severe
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { has_country_flag = threat_level_critical }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { has_country_flag = threat_level_critical }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { has_country_flag = threat_level_critical }
			}
		}
		localization_key = threat_level_critical
	}
}

defined_text = {
	name = LBA_radicalization

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = { check_variable = { radicalization < 10 } }
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = { check_variable = { radicalization < 10 } }
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = { check_variable = { radicalization < 10 } }
			}
		}
		localization_key = radicalization_negligible
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = {
					check_variable = { radicalization > 9 }
					check_variable = { radicalization < 30 }
				}
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = {
					check_variable = { radicalization > 9 }
					check_variable = { radicalization < 30 }
				}
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = {
					check_variable = { radicalization > 9 }
					check_variable = { radicalization < 30 }
				}
			}
		}
		localization_key = radicalization_low
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = {
					check_variable = { radicalization > 29 }
					check_variable = { radicalization < 50 }
				}
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = {
					check_variable = { radicalization > 29 }
					check_variable = { radicalization < 50 }
				}
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = {
					check_variable = { radicalization > 29 }
					check_variable = { radicalization < 50 }
				}
			}
		}
		localization_key = radicalization_significant
	}

	text = {
		trigger = {
			if = {
				limit = { country_exists = LBA }
				LBA = {
					check_variable = { radicalization > 49 }
				}
			}
			else_if = {
				limit = {
					NOT = { country_exists = LBA }
					country_exists = GNA
				}
				GNA = {
					check_variable = { radicalization > 49 }
				}
			}
			else_if = {
				limit = {
					NOT = { country_exists = GNA }
					country_exists = HOR
				}
				HOR = {
					check_variable = { radicalization > 49 }
				}
			}
		}
		localization_key = radicalization_high
	}
}

define_text = {
	name = CT_radicalization_clamp_display
	text = {
		trigger = {
			NOT = { has_idea = ibadi has_idea = sufi_islam }
			has_stability < 0.4
			OR = {
				has_idea = paralyzing_corruption
				has_idea = crippling_corruption
				has_idea = rampant_corruption
				has_idea = unrestrained_corruption
			}
			check_variable = { gdp_per_capita < 10.000 }
		}
		localization_key = CT_radicalization_clamp_one
	}
	text = {
		trigger = {
			NOT = { has_idea = ibadi has_idea = sufi_islam }
			OR = {
				AND = {
					has_stability < 0.4
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
					}
				}
				AND = {
					has_stability < 0.4
					check_variable = { gdp_per_capita < 10.000 }
				}
				AND = {
					check_variable = { gdp_per_capita < 10.000 }
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
					}
				}
			}
		}
		localization_key = CT_radicalization_clamp_two
	}
	text = {
		trigger = {
			OR = {
				has_stability < 0.4
				has_idea = paralyzing_corruption
				has_idea = crippling_corruption
				has_idea = rampant_corruption
				has_idea = unrestrained_corruption
				check_variable = { gdp_per_capita < 10.000 }
			}
			NOT = { has_idea = ibadi has_idea = sufi_islam }
		}
		localization_key = CT_radicalization_clamp_three
	}
	text = {
		trigger = {
			OR = {
				NOT = {
					has_stability < 0.4
					has_idea = paralyzing_corruption
					has_idea = crippling_corruption
					has_idea = rampant_corruption
					has_idea = unrestrained_corruption
					check_variable = { gdp_per_capita < 10.000 }
				}
				has_idea = ibadi
				has_idea = sufi_islam
			}
		}
		localization_key = CT_radicalization_clamp_four
	}
}

define_text = {
	name = CT_threat_level_clamp_display
	text = {
		trigger = {
			OR = {
				jihadist_government = yes
				has_completed_focus = GCC_a_radical_reorientation
			}
		}
		localization_key = CT_threat_level_clamp_one
	}
	text = {
		trigger = {
			has_war = yes
			no_jihadist_government = yes
			NOT = { has_completed_focus = GCC_a_radical_reorientation }
		}
		localization_key = CT_threat_level_clamp_two
	}
	text = {
		trigger = {
			has_war = no
			no_jihadist_government = yes
			NOT = { has_completed_focus = GCC_a_radical_reorientation }
			has_stability < 0.4
			OR = {
				has_idea = paralyzing_corruption
				has_idea = crippling_corruption
				has_idea = rampant_corruption
				has_idea = unrestrained_corruption
			}
			check_variable = { gdp_per_capita < 10.000 }
		}
		localization_key = CT_threat_level_clamp_three
	}
	text = {
		trigger = {
			has_war = no
			no_jihadist_government = yes
			NOT = { has_completed_focus = GCC_a_radical_reorientation }
			OR = {
				AND = {
					has_stability < 0.4
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
					}
				}
				AND = {
					has_stability < 0.4
					check_variable = { gdp_per_capita < 10.000 }
				}
				AND = {
					check_variable = { gdp_per_capita < 10.000 }
					OR = {
						has_idea = paralyzing_corruption
						has_idea = crippling_corruption
						has_idea = rampant_corruption
						has_idea = unrestrained_corruption
					}
				}
			}
		}
		localization_key = CT_threat_level_clamp_four
	}
	text = {
		trigger = {
			has_war = no
			no_jihadist_government = yes
			NOT = { has_completed_focus = GCC_a_radical_reorientation }
			OR = {
				has_stability < 0.4
				has_idea = paralyzing_corruption
				has_idea = crippling_corruption
				has_idea = rampant_corruption
				has_idea = unrestrained_corruption
				check_variable = { gdp_per_capita < 10.000 }
			}
		}
		localization_key = CT_threat_level_clamp_five
	}
	text = {
		trigger = {
			has_war = no
			no_jihadist_government = yes
			NOT = { has_completed_focus = GCC_a_radical_reorientation }
			NOT = {
				has_stability < 0.4
				has_idea = paralyzing_corruption
				has_idea = crippling_corruption
				has_idea = rampant_corruption
				has_idea = unrestrained_corruption
				check_variable = { gdp_per_capita < 10.000 }
			}
		}
		localization_key = CT_threat_level_clamp_six
	}
}