opinion_modifiers = {
	VEN_oil_deal = {
		trade = yes
		value = 50
	}
	VEN_rejected_deal = {
		value = -15
	}
	VEN_friendship = {
		value = 50
	}
	VEN_friendship_cuba = {
		value = 50
	}
	VEN_friendship_brazil = {
		value = 50
	}
	VEN_friendship_argentina = {
		value = 50
	}
	VEN_reached_to_usa = {
		value = 25
		decay = 1
	}

	VEN_us_import_policy_aggressive = {
		value = -5
		decay = 1
	}
	VEN_us_import_policy_fair = {
		value = 5
		decay = 1
	}
	VEN_reached_out_to_cuba_modifier = {
		value = 25
	}
}
