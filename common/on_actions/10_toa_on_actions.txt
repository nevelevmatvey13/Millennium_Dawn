
on_actions = {
	on_ruling_party_change = {
	}
	on_startup = {
	}
	on_peaceconference_ended = {
	}

	on_puppet = { #ROOT = nation being puppeted, FROM = overlord
	}

	on_release_as_puppet = { # ROOT is the nation being released, FROM is the overlord.
	}

	#ROOT is subject FROM is previous overlord
	on_subject_free = {
	}

	# called when a country send volunteers to another
	# ROOT is sender, FROM is receiver
	on_send_volunteers = { #sets flag for whether Argentina has sent volunteers to Nationalist Spain
	}

	#FROM is war target
	#ROOT is country declaring war
	on_declare_war = {
	}

	#ROOT = attacking side
	#FROM = defending side
	on_declare_war = {
	}

	#ROOT is the nation being released, FROM is the overlord
	on_release_as_free = {
	}

	on_liberate = {
	}

	# ROOT is capitulated country, FROM is winner
	on_capitulation = {
	}
}