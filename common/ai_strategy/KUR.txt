KUR_declare_war_on_iraq = {
	allowed = { original_tag = KUR }
	enable = {
		country_exists = IRQ
		has_country_flag = KUR_joined_the_invasion_of_iraq
		IRQ = { is_subject = no }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = declare_war id = "IRQ" value = 200 }
}
