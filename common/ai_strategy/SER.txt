#I hate Kosovo
SER_hate_KOS = {
	allowed = { original_tag = SER }
	enable = {
		country_exists = KOS
	}
	abort = {
		KOS = { is_subject_of = SER }
	}

	ai_strategy = { type = antagonize id = "KOS" value = 150 }
	ai_strategy = { type = conquer id = "KOS" value = 150 }
}
#Russian Friend
SER_befriend_SOV = {
	allowed = { original_tag = SER }
	enable = {
		SOV = { NOT = { has_government = democratic } }
		NOT = { has_government = democratic }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = befriend id = "SOV" value = 150 }
}
#Help Srpska
SER_bosnian_civil_war_srpska = {
	allowed = { original_tag = SER }
	enable = {
		BOS = { has_country_flag = defeat_all_serbs }
		RSK = { has_war_with = BOS }
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = send_volunteers_desire id = "RSK" value = 200 }
	ai_strategy = { type = befriend id = "RSK" value = 200 }
	ai_strategy = { type = support id = "RSK" value = 200 }
}
