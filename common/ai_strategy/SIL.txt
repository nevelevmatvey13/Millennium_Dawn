WTO_help_silesia = {
	allowed = {
		OR = {
			original_tag = HUN
			original_tag = ROM
			original_tag = CZE
			original_tag = BUL
			original_tag = ALB
		}
				
	}
	enable = {
		POL = { 
			has_country_flag = SIL_start_bund 
		}
		has_idea = SOV_warsaw_pact_idea
		SIL = { 
		country_exists = SIL
		is_subject = no
		}
	}
	abort_when_not_enabled = yes

	ai_strategy = { type = alliance id = "SIL" value = 500 }
	ai_strategy = { type = befriend id = "SIL" value = 150 }
	ai_strategy = { type = support id = "SIL" value = 1000 }
	ai_strategy = { type = protect id = "SIL" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "SIL" value = 1000 }
}
Subject_help_silesia = {
	allowed = {
		OR = {
			original_tag = CHE
			original_tag = TAT
			original_tag = BSH
			original_tag = CRM
			original_tag = YAK
			original_tag = BRY
			original_tag = KHS
			original_tag = ADY
			original_tag = CKK
			original_tag = NEE
			original_tag = YAM
			original_tag = ALT
			original_tag = KAE
			original_tag = KOM
			original_tag = TUV
			original_tag = KLM
			original_tag = KBK
			original_tag = DAG
			original_tag = KHM
			original_tag = ING
			original_tag = URA
			original_tag = GOR
			original_tag = FAR
			original_tag = SIB
			original_tag = KUB
			original_tag = KCC
		}
	}
	enable = {
		POL = { 
			has_country_flag = SIL_start_bund 
		}
		is_subject = yes
		has_autonomy_state = autonomy_republic_rf
		SIL = { 
		country_exists = SIL
		is_subject = no
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = befriend id = "SIL" value = 150 }
	ai_strategy = { type = support id = "SIL" value = 1000 }
	ai_strategy = { type = send_volunteers_desire id = "SIL" value = 1000 }
}
Russia_help_silesia = {
	allowed = {
		original_tag = SOV
		
	}
	enable = {
		POL = { 
			has_country_flag = SIL_start_bund 
		}
		SIL = { 
		country_exists = SIL
		is_subject = no
		}
	}
	abort_when_not_enabled = yes
	ai_strategy = { type = alliance id = "SIL" value = 500 }
	ai_strategy = { type = befriend id = "SIL" value = 150 }
	ai_strategy = { type = support id = "SIL" value = 1000 }
	ai_strategy = { type = protect id = "SIL" value = 150 }
	ai_strategy = { type = send_volunteers_desire id = "SIL" value = 1000 }
}