# Written by Mosaink
increase_sanctions = {
	if = {
		limit = { has_idea = Massive_International_Sanctions }
		add_political_power = 50
	}
	if = {
		limit = { has_idea = international_sanctions }
		swap_ideas = {
			remove_idea = international_sanctions
			add_idea = Massive_International_Sanctions
		}
	}
	if = {
		limit = { has_idea = Western_Sanctions }
		swap_ideas = {
			remove_idea = Western_Sanctions
			add_idea = international_sanctions
		}
	}
	if = {
		limit = { has_idea = Reduced_Western_Sanctions }
		swap_ideas = {
			remove_idea = Reduced_Western_Sanctions
			add_idea = Western_Sanctions
		}
	}
}

decrease_sanctions = {
	if = {
		limit = { has_idea = Massive_International_Sanctions }
		swap_ideas = {
			remove_idea = Massive_International_Sanctions
			add_idea = international_sanctions
		}
	}
	if = {
		limit = { has_idea = international_sanctions }
		swap_ideas = {
			remove_idea = international_sanctions
			add_idea = Western_Sanctions
		}
	}
	if = {
		limit = { has_idea = Western_Sanctions }
		swap_ideas = {
			remove_idea = Western_Sanctions
			add_idea = Reduced_Western_Sanctions
		}
	}
	if = {
		limit = { has_idea = Reduced_Western_Sanctions }
		remove_ideas = Reduced_Western_Sanctions
	}
}