india_religious_events = {
	#recurring mafia events
	random_list = {
		60 = {
			add_to_variable = { event_counter_1_religious = 1 }
		}
		40 = {
		}
	}
	if = {
		limit = { check_variable = { event_counter_1_religious > 5 } }
		set_variable = { event_counter_1_religious = 0 }
		random_list = {
			25 = {
				country_event = RAJ_religion.1
			}
			25 = {
				country_event = RAJ_religion.2
			}
			25 = {
				country_event = RAJ_religion.3
			}
			25 = {
				country_event = RAJ_religion.4
			}
		}
	}
}