set_leader_WAG = {

	if = { limit = { has_country_flag = set_conservatism NOT = { has_country_flag = WAG_Prigoba_dead } }
		if = { limit = { check_variable = { conservatism_leader = 0 } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = conservatism
				traits = {
					western_conservatism leader_of_wagner
				} 
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { conservatism_leader = 1 } NOT = { has_country_flag = WAG_Prigoba_dead } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = conservatism
				traits = {
					western_conservatism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead } has_country_flag = set_liberalism }
		if = { limit = { check_variable = { liberalism_leader = 0 } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = liberalism
				traits = {
					western_liberalism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead } check_variable = { liberalism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = liberalism
				traits = {
					western_liberalism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { check_variable = { liberalism_leader = 2 } NOT = { has_country_flag = WAG_Prigoba_dead } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { liberalism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = liberalism
				traits = {
					western_liberalism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { liberalism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Communist-State }
		if = { limit = { check_variable = { Communist-State_leader = 0 } }
			add_to_variable = { Communist-State_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Communist-State
				traits = {
					emerging_Communist-State leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Communist-State_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_oligarchism }
		if = { limit = { check_variable = { oligarchism_leader = 0 } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { oligarchism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { oligarchism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = oligarchism
				traits = {
					neutrality_oligarchism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { oligarchism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_neutral_Social }
		if = { limit = { check_variable = { neutral_Social_leader = 0 } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { neutral_Social_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { neutral_Social_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { neutral_Social_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = neutral_Social
				traits = {
					neutrality_neutral_Social leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { neutral_Social_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Neutral_Libertarian }
		if = { limit = { check_variable = { Neutral_Libertarian_leader = 0 } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { Neutral_Libertarian_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Libertarian_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Neutral_Libertarian
				traits = {
					neutrality_Neutral_Libertarian leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Libertarian_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Neutral_green }
		if = { limit = { check_variable = { Neutral_green_leader = 0 } }
			add_to_variable = { Neutral_green_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Neutral_green
				traits = {
					neutrality_Neutral_green leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_green_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Nat_Autocracy }
				if = { limit = { has_country_flag = SOV_wagner_parties check_variable = { Nat_Autocracy_leader = 0 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 0 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy
					leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { Nat_Autocracy_leader = 2 } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
		if = { limit = { check_variable = { Nat_Autocracy_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Nat_Autocracy_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Nat_Autocracy
				traits = {
					nationalist_Nat_Autocracy leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Autocracy_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Nat_Populism }
		if = { limit = { check_variable = { Nat_Populism_leader = 0 } }
			add_to_variable = { Nat_Populism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Nat_Populism
				traits = {
					nationalist_Nat_Populism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Populism_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Monarchist }
		if = { limit = { check_variable = { Monarchist_leader = 0 } }
			add_to_variable = { Monarchist_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Monarchist
				traits = {
					nationalist_Monarchist leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Monarchist_leader = 1 } }
			if = { limit = { date < 2016.1.2 } set_temp_variable = { b = 1 } } #skip if 2017
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_socialism }
		if = { limit = { check_variable = { socialism_leader = 0 } }
			add_to_variable = { socialism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = socialism
				traits = {
					western_socialism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { socialism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_anarchist_communism }
		if = { limit = { check_variable = { anarchist_communism_leader = 0 } }
			add_to_variable = { anarchist_communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = anarchist_communism
				traits = {
					emerging_anarchist_communism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { anarchist_communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Neutral_conservatism }
		if = { limit = { check_variable = { Neutral_conservatism_leader = 0 } }
			add_to_variable = { Neutral_conservatism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Neutral_conservatism
				traits = {
					neutrality_Neutral_conservatism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_conservatism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Neutral_Communism }
		if = { limit = { check_variable = { Neutral_Communism_leader = 0 } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { Neutral_Communism_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Neutral_Communism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Neutral_Communism
				traits = {
					neutrality_Neutral_Communism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Neutral_Communism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Nat_Fascism }
		if = { limit = { check_variable = { Nat_Fascism_leader = 0 } }
			add_to_variable = { Nat_Fascism_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Nat_Fascism
				traits = {
					nationalist_Nat_Fascism leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Nat_Fascism_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Conservative }
		if = { limit = { check_variable = { Conservative_leader = 0 } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { Conservative_leader = 1 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { Conservative_leader = 2 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative leader_of_wagner
				} 
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
		if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  check_variable = { Conservative_leader = 3 } NOT = { check_variable = { b = 1 } } }
			add_to_variable = { Conservative_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Conservative
				traits = {
					emerging_Conservative leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Conservative_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
	else_if = { limit = { NOT = { has_country_flag = WAG_Prigoba_dead }  has_country_flag = set_Caliphate }
		if = { limit = { check_variable = { Caliphate_leader = 0 } }
			add_to_variable = { Caliphate_leader = 1 }
			hidden_effect = { kill_country_leader = yes }

			create_country_leader = {
				name = "Yevgeny Prigozhin"
				picture = "yevgeny_prigozhin.dds"
				ideology = Caliphate
				traits = {
					salafist_Caliphate leader_of_wagner
				}
			}

			if = { limit = { has_country_flag = do_not_retire } subtract_from_variable = { Caliphate_leader = 1 } }
			set_temp_variable = { b = 1 }
		}
	}
}