SWI_mowag_tank_manufacturer = {
	allowed = { original_tag = SWI }
	icon = GFX_idea_mowag_logo
	name = SWI_mowag_tank_manufacturer
	include = generic_tank_equipment_organization
}

SWI_ruag_materiel_manufacturer = {
	allowed = { original_tag = SWI }
	icon = GFX_idea_RUAG
	name = SWI_ruag_materiel_manufacturer
	include = generic_infantry_equipment_organization
}
SWI_SIG_Sauer_materiel_manufacturer = {
	allowed = { original_tag = SWI }
	icon = GFX_idea_SIG_Sauer
	name = GER_SIG_Sauer_materiel_manufacturer_name
	include = GER_generic_small_arms_manufacturer
}
SWI_ruag_aircraft_manufacturer = {
	allowed = { original_tag = SWI }
	icon = GFX_idea_RUAG
	name = SWI_ruag_aircraft_manufacturer
	# TODO: Replace with heavy air wing MIO 
	include = generic_air_equipment_organization
}

SWI_pilatus_aircraft_aircraft_manufacturer = {
	allowed = { original_tag = SWI }
	icon = GFX_idea_Pilatus_Aircraft_logo
	name = SWI_pilatus_aircraft_aircraft_manufacturer
	# TODO: Replace with fighter MIO
	include = generic_air_equipment_organization
}