jihadist_government = {
	custom_trigger_tooltip = {
		tooltip = GCC_jihadist_government_tt
		hidden_trigger = {
			OR = {
				is_in_array = { ruling_party = 11 }
				is_in_array = { gov_coalition_array = 11 }
			}
		}
	}
}

no_jihadist_government = {
	custom_trigger_tooltip = {
		tooltip = GCC_no_jihadist_government_tt
		hidden_trigger = {
			NOT = {
				is_in_array = { ruling_party = 11 }
				is_in_array = { gov_coalition_array = 11 }
			}
		}
	}
}

gcc_dominant_member = {
	custom_trigger_tooltip = {
		tooltip = gcc_dominant_member_tt
		has_country_flag = gcc_dominant_member
	}
}

gcc_no_veto = {
	custom_trigger_tooltip = {
		tooltip = gcc_no_veto_tt
		NOT = {
			any_of_scopes = {
				array = global.gcc_member_state
				has_idea = GCC_veto
			}
		}
	}
}

gcc_unity_greater_0 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_0_tt
		check_variable = { global.gcc_unity > 0 }
	}
}

gcc_unity_greater_20 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_20_tt
		check_variable = { global.gcc_unity > 20 }
	}
}

gcc_unity_greater_40 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_40_tt
		check_variable = { global.gcc_unity > 40 }
	}
}

gcc_unity_greater_50 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_50_tt
		check_variable = { global.gcc_unity > 50 }
	}
}

gcc_unity_greater_60 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_60_tt
		check_variable = { global.gcc_unity > 60 }
	}
}

gcc_unity_greater_75 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_75_tt
		check_variable = { global.gcc_unity > 75 }
	}
}

gcc_unity_greater_80 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_80_tt
		check_variable = { global.gcc_unity > 80 }
	}
}

gcc_unity_greater_90 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_90_tt
		check_variable = { global.gcc_unity > 90 }
	}
}

gcc_unity_greater_95 = {
	custom_trigger_tooltip = {
		tooltip = gcc_unity_greater_95_tt
		check_variable = { global.gcc_unity > 95 }
	}
}