# Scripted triggers					Scope				Explanation
# ------
# is_western_nation					COUNTRY			TRUE if SCOPE COUNTRY is western
# is_african_nation					COUNTRY 		TRUE if SCOPE COUNTRY is African
# is_west_african_nation			COUNTRY			TRUE if SCOPE COUNTRY is located in West Africa
# is_arabic_nation					COUNTRY			TRUE if SCOPE COUNTRY is Arabic
# is_european_nation				COUNTRY			TRUE if SCOPE COUNTRY is European. Includes all European countries (Russia etc.) but not the Caucasus
# is_asian_nation					COUNTRY			TRUE if SCOPE COUNTRY is Asian
# is_EFTA							COUNTRY			TRUE if SCOPE COUNTRY is part of EFTA common market (Norway, Iceland, Switzerland and EU)
# is_Schengen_Area					COUNTRY			TRUE if SCOPE COUNTRY is part of EU's common labor market and VISA-free travel
# is_Central_Asia					COUNTRY			TRUE if SCOPE COUNTRY is located in Central Asia (includes Afghanistan)
# is_Caucasus						COUNTRY			TRUE if SCOPE COUNTRY is located in the Caucasus
# is_South_Asia						COUNTRY			TRUE if SCOPE COUNTRY is located in South Asia (India, Pakistan, etc)
# is_SE_Asia						COUNTRY			TRUE if SCOPE COUNTRY is located in South East Asia
# is_Middle_East					COUNTRY			TRUE if SCOPE COUNTRY is located in the Middle East and North Africa
# is_North_America					COUNTRY			TRUE if SCOPE COUNTRY is located in North America
# is_Central_America				COUNTRY			TRUE if SCOPE COUNTRY is located in Central America
# is_caribbean_nation				COUNTRY 		TRUE if SCOPE COUNTRY is located in the Caribbean
# is_South_America					COUNTRY 		TRUE if SCOPE COUNTRY is located in South America
# is_oceania_nation					COUNTRY 		TRUE if SCOPE COUNTRY is located in Oceania
# is_arid_nation					COUNTRY 		TRUE if SCOPE COUNTRY has an Arid Climate
# is_arctic_nation					COUNTRY 		TRUE if SCOPE COUNTRY is part of the Arctic Council
# is_earthquake_prone_nation		COUNTRY 		TRUE if SCOPE COUNTRY is prone to getting devastating earthquakes
# is_micro_nation					COUNTRY 		TRUE if SCOPE COUNTRY is a country with about 1 million people or less
# is_tropical_nation				COUNTRY 		TRUE if SCOPE COUNTRY has a Tropical Climate
# is_banana_nation					COUNTRY 		TRUE if SCOPE COUNTRY is a major exporter or dependent on growing bananas
# French_Speaking					COUNTRY 		TRUE if SCOPE COUNTRY has French as its main language
# Spanish_Speaking					COUNTRY 		TRUE if SCOPE COUNTRY has Spanish as its main language
# Portugese_Speaking				COUNTRY 		TRUE if SCOPE COUNTRY has Portuguese as its main language
# Chinese_Speaking					COUNTRY 		TRUE if SCOPE COUNTRY has Mandarin or Cantonese as its main language
# Spanish_Speaking					COUNTRY 		TRUE if SCOPE COUNTRY has Spanish as its main language
# Swahili_Speaking 					COUNTRY 		TRUE if SCOPE COUNTRY has Swahili as one of its main languages
# Russian_Proficient				COUNTRY 		TRUE if SCOPE COUNTRY speaks Russian as a commonly known language
# German_Speaking					COUNTRY 		TRUE if SCOPE COUNTRY has German as its largest language
# English_Speaking					COUNTRY 		TRUE if SCOPE COUNTRY has English as its official language. FALSE if English is spoken as 2nd language
# Multi_Ethnic_State				COUNTRY 		TRUE if SCOPE COUNTRY has several large minorities native to the land
# HIV_Epidemic						COUNTRY 		TRUE if SCOPE COUNTRY has a higher infection rate than 3,6%.
# Is_Possible_Muslim_Brotherhood	COUNTRY 		TRUE if SCOPE COUNTRY can have Muslim Brotherhood as a party
# Is_Muslim_Brotherhood				COUNTRY 		TRUE if SCOPE COUNTRY has Muslim Brotherhood as ruling party OR friendly supporter like Qatar
# Is_2017_Riyadh_Summit_Member		COUNTRY 		TRUE if SCOPE COUNTRY was part of the 2017 Riyadh summit
# Is_Permanent_UN_Member			COUNTRY 		TRUE if SCOPE COUNTRY is a Permanent UN Member
# Is_UN_Member						COUNTRY 		TRUE if SCOPE COUNTRY is a UN Member

## THESE CAN'T OVERLAP! USED FOR POWER RANKING ##
#is_european_nation
#is_sub_saharan_nation
#is_middle_eastern_nation
#is_asian_nation
#is_american_nation

ROOT_and_THIS_are_in_the_same_group = {
	OR = {
		AND = {
			THIS = { is_european_nation = yes }
			ROOT = { is_european_nation = yes }
		}
		AND = {
			THIS = { is_sub_saharan_nation = yes }
			ROOT = { is_sub_saharan_nation = yes }
		}
		AND = {
			THIS = { is_middle_eastern_nation = yes }
			ROOT = { is_middle_eastern_nation = yes }
		}
		AND = {
			THIS = { is_asian_nation = yes }
			ROOT = { is_asian_nation = yes }
		}
		AND = {
			THIS = { is_american_nation = yes }
			ROOT = { is_american_nation = yes }
		}
	}
}

### groups used for regional power setup:
is_european_nation = {
	OR = {
		original_tag = ADO
		original_tag = ALB
		original_tag = AUS
		original_tag = BEL
		original_tag = BLR
		original_tag = BOS
		original_tag = BUL
		original_tag = CAT
		original_tag = CRE
		original_tag = CRO
		original_tag = CYP
		original_tag = CZE
		original_tag = DEN
		original_tag = ENG
		original_tag = EST
		original_tag = EUU
		original_tag = FIN
		original_tag = FRA
		original_tag = FYR
		original_tag = GAL
		original_tag = GER
		original_tag = GRE
		original_tag = HLS
		original_tag = HOL
		original_tag = HUN
		original_tag = ICE
		original_tag = IOM
		original_tag = IRE
		original_tag = ITA
		original_tag = KOS
		original_tag = LAT
		original_tag = LIC
		original_tag = LIT
		original_tag = LUX
		original_tag = MLT
		original_tag = MLV
		#original_tag = MCN
		original_tag = MNT
		original_tag = NAV
		original_tag = NOR
		original_tag = NOV
		original_tag = PMR
		original_tag = POL
		original_tag = POR
		original_tag = ROM
		original_tag = SAR
		original_tag = SCL
		original_tag = SCO
		original_tag = SER
		original_tag = SLO
		original_tag = SLV
		original_tag = SMA
		original_tag = SOV
		original_tag = SPR
		original_tag = SWE
		original_tag = SWI
		original_tag = UKR
		original_tag = WAS
	}
}
is_sub_saharan_nation = {
	OR = {
		original_tag = AFR
		original_tag = AGF
		original_tag = AGL
		original_tag = BAL
		original_tag = BEN
		original_tag = BFA
		original_tag = BOT
		original_tag = BUR
		original_tag = CAM
		original_tag = CAR
		original_tag = CDI
		original_tag = CHA
		original_tag = CNG
		original_tag = COM
		original_tag = DAR
		original_tag = DJI
		original_tag = DRC
		original_tag = EGU
		original_tag = ERI
		original_tag = ETH
		original_tag = GAB
		original_tag = GAH
		original_tag = GAM
		original_tag = GUB
		original_tag = GUI
		original_tag = JUB
		original_tag = KEN
		original_tag = LES
		original_tag = LIB
		original_tag = LOG
		original_tag = LUR
		original_tag = MAD
		original_tag = MAL
		original_tag = MAU
		original_tag = MLC
		original_tag = MLW
		original_tag = MOZ
		original_tag = MRT
		original_tag = NAM
		original_tag = NGR
		original_tag = NIG
		original_tag = PUN
		original_tag = RCD
		original_tag = RWA
		original_tag = SAF
		original_tag = SAO
		original_tag = SEL
		original_tag = SEN
		original_tag = SEY
		original_tag = SHB
		original_tag = SIE
		original_tag = SML
		original_tag = SNA
		original_tag = SOM
		original_tag = SRF
		original_tag = SSU
		original_tag = STH
		original_tag = SUD
		original_tag = SWA
		original_tag = SWS
		original_tag = TNZ
		original_tag = TOG
		original_tag = TUA
		original_tag = UGA
		original_tag = UNI
		original_tag = VER
		original_tag = ZAM
		original_tag = ZIM
	}
}
is_middle_eastern_nation = {
	OR = {
		original_tag = ABK
		original_tag = ALA
		original_tag = ALG
		original_tag = AQY
		original_tag = ARA
		original_tag = ARM
		original_tag = AZE
		original_tag = BHR
		original_tag = CHE
		original_tag = CNR
		original_tag = DRU
		original_tag = EGY
		original_tag = FSA
		original_tag = GEO
		original_tag = GNA
		original_tag = GNC
		original_tag = HAM
		original_tag = HEJ
		original_tag = HEZ
		original_tag = HOR
		original_tag = HOU
		original_tag = IRQ
		original_tag = ISI
		original_tag = ISR
		original_tag = JOR
		original_tag = KUR
		original_tag = KUW
		original_tag = LBA
		original_tag = LEB
		original_tag = LOR
		original_tag = MOR
		original_tag = NCY
		original_tag = NEJ
		original_tag = NKR
		original_tag = NUS
		original_tag = OMA
		original_tag = PAL
		original_tag = PER
		original_tag = PKK
		original_tag = QAT
		original_tag = QTF
		original_tag = ROJ
		original_tag = SAU
		original_tag = SHA
		original_tag = SOO
		original_tag = SYR
		original_tag = TAB
		original_tag = TUN
		original_tag = TUR
		original_tag = UAE
		original_tag = YEM
	}
}
is_islamist_secularism_mechanic = {
	OR = {
		original_tag = ALA
		original_tag = ALG
		original_tag = ARA
		original_tag = BHR
		original_tag = CHE
		original_tag = CNR
		original_tag = DRU
		original_tag = EGY
		original_tag = GNA
		original_tag = GNC
		original_tag = HEJ
		original_tag = IRQ
		original_tag = JOR
		original_tag = KUW
		original_tag = LBA
		original_tag = LEB
		original_tag = LOR
		original_tag = MAY
		original_tag = MOR
		original_tag = NCY
		original_tag = NEJ
		original_tag = OMA
		original_tag = PKK
		original_tag = QAT
		original_tag = QTF
		original_tag = SAU
		original_tag = SYR
		original_tag = TAB
		original_tag = TUN
		original_tag = TUR
		original_tag = UAE
		original_tag = YEM
		original_tag = KAZ
		original_tag = UZB
		original_tag = KYR
	}
}
is_asian_nation = {
	OR = {
		original_tag = ACE
		original_tag = AFG
		original_tag = AST
		original_tag = BAN
		original_tag = BHU
		original_tag = BLC
		original_tag = BRM
		original_tag = BRU
		original_tag = CBD
		original_tag = CHI
		original_tag = ETK
		original_tag = FIJ
		original_tag = HKG
		original_tag = IND
		original_tag = JAP
		original_tag = KAC
		original_tag = KAR
		original_tag = KAZ
		original_tag = KOR
		original_tag = KYR
		original_tag = LAO
		original_tag = MAC
		original_tag = MIC
		original_tag = MLD
		original_tag = MON
		original_tag = NEP
		original_tag = NKO
		original_tag = NZL
		original_tag = PAK
		original_tag = PAP
		original_tag = PAU
		original_tag = PHI
		original_tag = SAM
		original_tag = RAJ
		original_tag = SHN
		original_tag = NHN
		original_tag = SIA
		original_tag = SIN
		original_tag = SOL
		original_tag = SRI
		original_tag = TAI
		original_tag = TAJ
		original_tag = TAL
		original_tag = TIB
		original_tag = TIM
		original_tag = TRK
		original_tag = TTP
		original_tag = UZB
		original_tag = VIE
		original_tag = WAA
		original_tag = MAR
		original_tag = KIR
		original_tag = TUL
		original_tag = VAN
		original_tag = TON
		original_tag = NAU
		original_tag = MAY
	}
}
is_american_nation = {
	OR = {
		original_tag = ARG
		original_tag = BAH
		original_tag = BAR
		original_tag = BLZ
		original_tag = BOL
		original_tag = BRA
		original_tag = CAL
		original_tag = CAN
		original_tag = CAS
		original_tag = CHL
		original_tag = COL
		original_tag = COS
		original_tag = CSA
		original_tag = CUB
		original_tag = DMI
		original_tag = DOM
		original_tag = ECU
		original_tag = ELS
		original_tag = FGU
		original_tag = GLC
		original_tag = GRA
		original_tag = GRL
		original_tag = GUA
		original_tag = GUY
		original_tag = HAI
		original_tag = HON
		original_tag = JAM
		original_tag = MEX
		original_tag = NEN
		original_tag = NIC
		original_tag = PAN
		original_tag = PAR
		original_tag = PAT
		original_tag = PRU
		original_tag = QUE
		original_tag = RGD
		original_tag = SLA
		original_tag = SPL
		original_tag = STK
		original_tag = STL
		original_tag = STV
		original_tag = SUL
		original_tag = SUR
		original_tag = TAM
		original_tag = TEX
		original_tag = TRC
		original_tag = TRI
		original_tag = URG
		original_tag = USA
		original_tag = VEN
		original_tag = YUC
		original_tag = ZAP
	}
}

#Checks if a country is classified as a Western nation
is_western_nation = {
	OR = {
		original_tag = USA
		original_tag = CAN

		original_tag = ENG
		original_tag = IRE
		original_tag = FRA
		original_tag = SPR
		original_tag = POR
		original_tag = AUS
		original_tag = ITA
		original_tag = BEL
		original_tag = HOL
		original_tag = LUX
		original_tag = GER
		original_tag = DEN
		original_tag = NOR
		original_tag = SWE
		original_tag = FIN
		original_tag = POL
		original_tag = EST
		original_tag = LAT
		original_tag = LIT
		original_tag = CZE
		original_tag = SLO
		original_tag = HUN
		original_tag = ROM
		original_tag = BUL
		original_tag = CRO
		original_tag = TUR

		original_tag = SWI
		original_tag = ICE

		original_tag = AST
		original_tag = NZL
	}
}



#Checks if a country is classified as an African nation
is_african_nation = {
	OR = {
		original_tag = AGL
		original_tag = ALG
		original_tag = BEN
		original_tag = BFA
		original_tag = BOT
		original_tag = BUR
		original_tag = CAM
		original_tag = CAR
		original_tag = CDI
		original_tag = CHA
		original_tag = CNG
		original_tag = CNR
		original_tag = COM
		original_tag = DJI
		original_tag = DRC
		original_tag = EGU
		original_tag = EGY
		original_tag = ERI
		original_tag = ETH
		original_tag = GAB
		original_tag = GAH
		original_tag = GAM
		original_tag = GUB
		original_tag = GUI
		original_tag = KEN
		original_tag = LBA
		original_tag = LES
		original_tag = MAD
		original_tag = MAL
		original_tag = MAU
		original_tag = MLW
		original_tag = MOR
		original_tag = MOZ
		original_tag = MRT
		original_tag = NAM
		original_tag = NGR
		original_tag = NIG
		original_tag = RWA
		original_tag = SAF
		original_tag = SAO
		original_tag = SEN
		original_tag = SEY
		original_tag = SHA
		original_tag = SIE
		original_tag = SML
		original_tag = SOM
		original_tag = SSU
		original_tag = STH
		original_tag = SUD
		original_tag = SWA
		original_tag = TNZ
		original_tag = TOG
		original_tag = TUN
		original_tag = UGA
		original_tag = VER
		original_tag = ZAM
		original_tag = ZIM
	}
}





is_west_african_nation = {
	OR = {
		original_tag = AFR
		original_tag = BEN
		original_tag = BFA
		original_tag = CAM
		original_tag = CHA
		original_tag = CDI
		original_tag = EGU
		original_tag = GAH
		original_tag = GAM
		original_tag = GUB
		original_tag = GUI
		original_tag = LIB
		original_tag = LUR
		original_tag = MAL
		original_tag = NGR
		original_tag = NIG
		original_tag = SAO
		original_tag = SEN
		original_tag = SIE
		original_tag = TOG
	}
}

is_horn_of_africa_nation = {
	OR = {
		original_tag = ETH
		original_tag = ERI
		original_tag = DJI
		is_somali_nation = yes

	}
}

is_somali_nation = {
	OR = {
		original_tag = SOM
		original_tag = PUN
		original_tag = SNA
		original_tag = SWS
		original_tag = SHB
		original_tag = JUB
		original_tag = SML
	}
}

is_sudanese_nation = {
	OR = {
		original_tag = SRF
		original_tag = DAR
		original_tag = SUD
		original_tag = SSU
		original_tag = AGF
	}
}

#Checks if a country is classified as an Arabic nation (ethnically Arab, doesn't include Djibouti, Chad, Somalia or Comoros even though Arabic is an official language)
#Countries must also be added to global.arabic_countries array in history files
is_arabic_nation = {
	OR = {
		original_tag = MAU
		original_tag = MOR
		original_tag = SHA
		original_tag = ALG
		original_tag = TUN
		original_tag = LBA
		#TODO ADD NEW LIBYAN RELEASABLES HERE
		original_tag = EGY
		original_tag = SUD
		original_tag = PAL
		original_tag = HAM
		original_tag = JOR
		original_tag = LEB
		original_tag = HEZ
		original_tag = SYR
		original_tag = FSA
		original_tag = NUS
		original_tag = DRU
		original_tag = ALA
		original_tag = ISI
		original_tag = IRQ
		original_tag = KUW
		original_tag = SAU
		original_tag = HEJ
		original_tag = NEJ
		original_tag = QTF
		original_tag = BHR
		original_tag = QAT
		original_tag = UAE
		original_tag = OMA
		original_tag = YEM
		original_tag = HOU
		original_tag = AQY
	}
}

is_EFTA = {
	OR = {
		original_tag = ICE
		original_tag = NOR
		original_tag = SWI
	}
}

is_Schengen_Area = {
	OR = {
		original_tag = ICE
		original_tag = NOR
		original_tag = SWI
		original_tag = DEN
		original_tag = SWE
		original_tag = POL
		original_tag = HUN
		original_tag = SWE
		original_tag = FRA
		original_tag = SPR
		original_tag = POR
		original_tag = AUS
		original_tag = ITA
		original_tag = BEL
		original_tag = HOL
		original_tag = LUX
		original_tag = GER
		original_tag = FIN
		original_tag = EST
		original_tag = LAT
		original_tag = LIT
		original_tag = SLO
		original_tag = SLV
		original_tag = GRE
		original_tag = CZE
		original_tag = MLT
	}
}
is_North_America = {
	OR = {
		original_tag = CAN
		original_tag = USA
		original_tag = MEX
		original_tag = GUA
		original_tag = BEL
		original_tag = HON
		original_tag = ELS
		original_tag = NIC
		original_tag = COS
		original_tag = PAN
		original_tag = CUB
		original_tag = HAI
		original_tag = DOM
		original_tag = JAM
		original_tag = BAH
		original_tag = DMI
		original_tag = STV
		original_tag = GRA
		original_tag = BAR
		original_tag = TRI
	}

}

is_Central_America = {
	OR = {
		original_tag = MEX
		original_tag = GUA
		original_tag = BEL
		original_tag = HON
		original_tag = ELS
		original_tag = NIC
		original_tag = COS
		original_tag = PAN
	}
}
is_caribbean_nation = {
	OR = {
		original_tag = CUB
		original_tag = HAI
		original_tag = DOM
		original_tag = JAM
		original_tag = BAH
		original_tag = DMI
		original_tag = STV
		original_tag = GRA
		original_tag = BAR
		original_tag = TRI
	}
}
is_South_America = {
	OR = {
		original_tag = COL
		original_tag = VEN
		original_tag = ECU
		original_tag = PRU
		original_tag = SUR
		original_tag = GUY
		original_tag = BRA
		original_tag = BOL
		original_tag = PAR
		original_tag = URG
		original_tag = ARG
		original_tag = CHL
	}
}

is_Central_Asia = {
	OR = {
		original_tag = TRK
		original_tag = TAJ
		original_tag = UZB
		original_tag = KYR
		original_tag = KAZ
		original_tag = AFG
		original_tag = TAL
		original_tag = TTP
	}
}

is_Caucasus = {
	OR = {
		original_tag = AZE
		original_tag = GEO
		original_tag = ARM
		original_tag = ABK
		original_tag = SOO
		original_tag = NKR
	}
}

is_South_Asia = {
	OR = {
		original_tag = RAJ
		original_tag = PAK
		original_tag = BAN
		original_tag = BHU
		original_tag = SRI
		original_tag = NEP
		original_tag = MLD
	}
}
is_East_Asia = {
	OR = {
		original_tag = CHI
		original_tag = MON
		original_tag = NKO
		original_tag = KOR
		original_tag = JAP
		original_tag = TAI
	}
}
is_SE_Asia = {
	OR = {
		original_tag = BRU
		original_tag = CBD
		original_tag = IND
		original_tag = LAO
		original_tag = KAC
		original_tag = KAR
		original_tag = MAY
		original_tag = BRM
		original_tag = PHI
		original_tag = SHN
		original_tag = NHN
		original_tag = SIN
		original_tag = KOR
		original_tag = SIA
		original_tag = TIM
		original_tag = VIE
		original_tag = WAA
		}
}

is_oceania_nation = {
	OR = {
		original_tag = AST
		original_tag = PAP
		original_tag = NZL
		original_tag = SOL
		original_tag = FIJ
		original_tag = SAM
		original_tag = MIC
		original_tag = PAU
		original_tag = MAR
		original_tag = KIR
		original_tag = TUL
		original_tag = VAN
		original_tag = TON
		original_tag = NAU
		}
	}
is_arid_nation = {
	OR = {
		#Asia
		original_tag = AST
		original_tag = MON
		original_tag = UZB
		original_tag = TRK
		original_tag = KAZ
		original_tag = AFG
		original_tag = TAL

		#Africa
		original_tag = ETH
		original_tag = ERI
		original_tag = SOM
		original_tag = SML
		original_tag = KEN
		original_tag = LBA
		original_tag = ALG
		original_tag = BOT
		original_tag = CHA
		original_tag = DJI
		original_tag = EGY
		original_tag = MAL
		original_tag = MAU
		original_tag = MOR
		original_tag = NAM
		original_tag = NGR
		original_tag = SAF
		original_tag = SHA
		original_tag = SUD
		original_tag = LES
		original_tag = TUN
		original_tag = DAR
		original_tag = GNA
		original_tag = GNC
		original_tag = HOR
		original_tag = SHB
		original_tag = SRF
		original_tag = TUA

		#Middle East
		original_tag = PER
		original_tag = BHR
		original_tag = IRQ
		original_tag = ISR
		original_tag = JOR
		original_tag = KUW
		original_tag = OMA
		original_tag = QAT
		original_tag = SAU
		original_tag = SYR
		original_tag = UAE
		original_tag = YEM
		original_tag = ISI
		original_tag = NUS
		original_tag = FSA
		original_tag = ROJ
		original_tag = KUR
		original_tag = HAM
		original_tag = HAM
		original_tag = HAM
	}
}

is_arctic_nation = {
	OR = {
		original_tag = CAN
		original_tag = USA
		original_tag = ICE
		original_tag = DEN
		original_tag = SWE
		original_tag = NOR
		original_tag = FIN
		original_tag = SOV
	}
}

is_earthquake_prone_nation = {
	OR = {
		original_tag = JAP
		original_tag = NEP
		original_tag = RAJ
		original_tag = PAK
		original_tag = IND
		original_tag = ELS
		original_tag = MEX
		original_tag = ECU
		original_tag = CHL
		original_tag = PHI
		original_tag = TUR
		original_tag = PER
		original_tag = AFG
		original_tag = HAI
		original_tag = NIC
		original_tag = HON
		original_tag = COS
		original_tag = GUA
		original_tag = PAN
		original_tag = NZL
		original_tag = BHU
		original_tag = ITA
		original_tag = YEM
		original_tag = PRU
		original_tag = BOL
	}
}

is_micro_nation = {
	OR = {
		original_tag = LUX
		original_tag = ICE
		original_tag = MNT
		original_tag = KOS
		original_tag = NCY
		original_tag = CYP
		original_tag = MLT
		original_tag = PMR
		original_tag = SOO
		original_tag = ABK

		original_tag = BLZ
		original_tag = BAH
		original_tag = BAR
		original_tag = DMI
		original_tag = GRA
		original_tag = STK
		original_tag = STL
		original_tag = STV
		original_tag = SUR
		original_tag = GUY

		original_tag = COM
		original_tag = DJI
		original_tag = EGU
		original_tag = GUB
		original_tag = SAO
		original_tag = SHA
		original_tag = SEY
		original_tag = GAM

		original_tag = BRU
		original_tag = BHU
		original_tag = MLD
		original_tag = TIM
		original_tag = FIJ
		original_tag = SAM
		original_tag = SOL
		original_tag = MIC
		original_tag = VER
		original_tag = PAU
		original_tag = MAR
		original_tag = KIR
		original_tag = TUL
		original_tag = VAN
		original_tag = TON
		original_tag = NAU
	}
}

is_tropical_nation = {
	OR = {

		#Africa
		original_tag = AGL
		original_tag = BEN
		original_tag = BFA
		original_tag = BUR
		original_tag = CAM
		original_tag = CAR
		original_tag = CDI
		original_tag = CNG
		original_tag = DRC
		original_tag = EGU
		original_tag = COM
		original_tag = GAB
		original_tag = GAH
		original_tag = GAM
		original_tag = GUB
		original_tag = GUI
		original_tag = LIB
		original_tag = MAD
		original_tag = MLW
		original_tag = MOZ
		original_tag = MRT
		original_tag = NIG
		original_tag = RWA
		original_tag = SAO
		original_tag = SEN
		original_tag = SEY
		original_tag = SIE
		original_tag = SSU
		original_tag = TOG
		original_tag = UGA
		original_tag = ZAM
		original_tag = ZIM
		original_tag = LOG
		original_tag = AGF
		original_tag = BAL
		original_tag = SEL

		#Latin America
		original_tag = BRA
		original_tag = COL
		original_tag = COS
		original_tag = ECU
		original_tag = ELS
		original_tag = GUA
		original_tag = GUY
		original_tag = HON
		original_tag = NIC
		original_tag = PAN
		original_tag = PAR
		original_tag = SUR
		original_tag = VEN

		#Carribean
		original_tag = BAH
		original_tag = BAR
		original_tag = BLZ
		original_tag = CUB
		original_tag = DMI
		original_tag = DOM
		original_tag = GRA
		original_tag = HAI
		original_tag = JAM
		original_tag = STK
		original_tag = STL
		original_tag = STV
		original_tag = TRI

		#Pacific
		original_tag = FIJ
		original_tag = MIC
		original_tag = PAP
		original_tag = SAM
		original_tag = SOL
		original_tag = PAU
		original_tag = MAR
		original_tag = KIR
		original_tag = TUL
		original_tag = VAN
		original_tag = TON
		original_tag = NAU

		#Asia
		original_tag = BAN
		original_tag = RAJ
		original_tag = BRM
		original_tag = BRU
		original_tag = CBD
		original_tag = IND
		original_tag = LAO
		original_tag = MAY
		original_tag = MLD
		original_tag = PHI
		original_tag = SIA
		original_tag = SIN
		original_tag = SRI
		original_tag = TIM
		original_tag = VIE
	}
}

is_banana_nation = {
	OR = {
		original_tag = ECU
		original_tag = GUA
		original_tag = COS
		original_tag = DOM
		original_tag = CDI
		original_tag = CAM
		original_tag = HON
		original_tag = PHI
		original_tag = COL
	}
}

French_Speaking = {
	OR = {
		original_tag = FRA
		original_tag = DRC
		original_tag = MAD
		original_tag = CAM
		original_tag = NGR
		original_tag = BFA
		original_tag = MAL
		original_tag = SEN
		original_tag = CHA
		original_tag = GUI
		original_tag = RWA
		original_tag = BUR
		original_tag = BEN
		original_tag = HAI
		original_tag = TOG
		original_tag = CAR
		original_tag = GAB
		original_tag = DJI
		original_tag = COM
		original_tag = SEY
		original_tag = BAL
		original_tag = LOG
		original_tag = SEL
		original_tag = BEL
	}
}

Spanish_Speaking = {
	OR = {
		original_tag = GAL
		original_tag = SPA
		original_tag = CAT
		original_tag = NAV
		original_tag = SPR
		original_tag = EGU
		original_tag = MEX
		original_tag = COL
		original_tag = ARG
		original_tag = PRU
		original_tag = VEN
		original_tag = CHL
		original_tag = ECU
		original_tag = GUA
		original_tag = BOL
		original_tag = DOM
		original_tag = HON
		original_tag = PAR
		original_tag = ELS
		original_tag = NIC
		original_tag = COS
		original_tag = CUB
		original_tag = PAN
		original_tag = URG
	}
}

Portugese_Speaking = {
	OR = {
		original_tag = POR
		original_tag = BRA
		original_tag = AGL
		original_tag = MOZ
		original_tag = GUB
		original_tag = TIM
		original_tag = VER
		original_tag = SAO
	}
}

Chinese_Speaking = {
	OR = {
		original_tag = CHI
		original_tag = TAI
		original_tag = SIN
		original_tag = HKG
		original_tag = MAC
		original_tag = WAA
	}
}

Swahili_Speaking = {
	OR = {
		original_tag = TNZ
		original_tag = KEN
		original_tag = DRC
		original_tag = UGA
	}
}

German_Speaking = {
	OR = {
		original_tag = GER
		original_tag = AUS
		original_tag = SWI
		original_tag = LUX
	}
}

Russian_Proficient = {
	OR = {
		original_tag = SOV
		original_tag = BLR
		original_tag = KAZ
		original_tag = KYR
		original_tag = TRK
		original_tag = TAJ
		original_tag = UZB
		original_tag = UKR
		original_tag = NOV
		original_tag = PMR
		original_tag = SOO
		original_tag = ABK
	}
}

English_Speaking = {
	OR = {
		original_tag = ENG
		original_tag = CAN
		original_tag = AST
		original_tag = NZL
		original_tag = USA
		original_tag = IRE
		original_tag = BAH
		original_tag = BAR
		original_tag = BLZ
		original_tag = DMI
		original_tag = GUY
		original_tag = JAM
		original_tag = GRA
		original_tag = STK
		original_tag = STV
		original_tag = STL
		original_tag = TRI
		original_tag = NIG
		original_tag = GAH
	}
}

Multi_Ethnic_State = {
	OR = {
		#Asia
		original_tag = CHI
		original_tag = PER
		original_tag = LAO
		original_tag = VIE
		original_tag = RAJ
		original_tag = NEP
		original_tag = SRI
		original_tag = AFG
		original_tag = PAK
		original_tag = MAY
		original_tag = TIM
		original_tag = IND
		original_tag = KAZ
		original_tag = KYR
		original_tag = IRQ

		#Europe
		original_tag = BOS
		original_tag = MNT
		original_tag = SOV
		original_tag = UKR
		original_tag = BEL
		original_tag = SER
		original_tag = SPR
		original_tag = ENG
		original_tag = TUR
		original_tag = LAT
		original_tag = EST
		original_tag = SWI
		original_tag = GER
		has_country_flag = USoE

		#Africa
		original_tag = KEN
		original_tag = ETH
		original_tag = NIG
		original_tag = SAF
		original_tag = DRC
		#original_tag = RWA
		original_tag = MAL
		original_tag = CHA
		original_tag = SUD
		original_tag = NGR
		original_tag = MAU
		original_tag = CAM
		original_tag = CAR
		original_tag = CDI
		original_tag = BEN
		original_tag = UGA
		original_tag = CNG
		original_tag = LIB
		original_tag = SIE
		original_tag = GAB
		original_tag = AGL
		original_tag = ZAM
		original_tag = MAD
		original_tag = SSU
		original_tag = AGF

		#Americas
		original_tag = GUY

		#Oceania
		original_tag = FIJ
		original_tag = PAP
	}
}

HIV_Epidemic = {
	OR = {
		original_tag = SWA
		original_tag = BOT
		original_tag = LES
		original_tag = SAF
		original_tag = ZIM
		original_tag = NAM
		original_tag = MOZ
		original_tag = MLW
		original_tag = UGA
		original_tag = EGU
		original_tag = TNZ
		original_tag = KEN
		original_tag = CAM
		original_tag = CAR
		original_tag = GAB
		original_tag = GUB
	}
}

##### Muslim Brotherhood is possible ####
Is_Possible_Muslim_Brotherhood = {
	OR = {
		tag = JOR
		tag = TUR
		tag = SYR
		tag = FSA
		tag = NUS
		tag = AFG
		tag = TAL
		tag = HAM
		tag = SAU
		tag = BHR
		tag = UAE
		tag = YEM
		tag = EGY
		tag = SUD
		tag = LBA
		tag = GNC
		tag = HOR
		tag = GNA
		tag = ALG
		tag = MOR
		tag = QAT
	}
}

##### Is Muslim Brotherhood ####
Is_Muslim_Brotherhood = {
	OR = {
		AND = {
			tag = JOR
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = TUR
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = SYR
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = FSA
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = NUS
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = AFG
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = TAL
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = HAM
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = SAU
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = BHR
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = UAE
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = YEM
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = EGY
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = SUD
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = LBA
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = GNC
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = HOR
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = GNA
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = ALG
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = MOR
			is_in_array = { ruling_party = 12 }
		}
		AND = {
			tag = QAT
			has_idea = al_jazeera
			NOT = { has_government = democratic }
		}
	}
}

Is_2017_Riyadh_Summit_Member = {
	OR = {
		original_tag = AFG original_tag = ALB original_tag = ALG original_tag = AZE original_tag = BAN
		original_tag = BEN original_tag = BRU original_tag = BFA original_tag = CAM original_tag = CHA
		original_tag = COM original_tag = DJI original_tag = EGY original_tag = GAB original_tag = GAM
		original_tag = GUI original_tag = GUB original_tag = GUY original_tag = IND original_tag = IRQ
		original_tag = CDI original_tag = JOR original_tag = KAZ original_tag = KUW original_tag = KYR
		original_tag = LEB original_tag = LBA original_tag = MAY original_tag = MLD original_tag = MAL
		original_tag = MAU original_tag = MOR original_tag = MOZ original_tag = NGR original_tag = NIG
		original_tag = PAK original_tag = PAL original_tag = SEN original_tag = SIE original_tag = SOM
		original_tag = SUR original_tag = TAJ original_tag = TOG original_tag = TUN original_tag = TUR
		original_tag = TRK original_tag = UGA original_tag = UZB original_tag = YEM original_tag = USA
		has_idea = idea_gcc_member_state
	}
}

Is_Permanent_UN_Member = {
	OR = {
		original_tag = CHI original_tag = FRA original_tag = SOV original_tag = ENG original_tag = USA
	}
}

Is_UN_Member = {
	OR = {
		original_tag = AFG original_tag = ALB original_tag = ALG original_tag = AGL original_tag = ALB
		original_tag = ARG original_tag = ARM original_tag = AST original_tag = AUS original_tag = AZE
		original_tag = BAH original_tag = BHR original_tag = BAN original_tag = BAR original_tag = BLR
		original_tag = BEL original_tag = BLZ original_tag = BEN original_tag = BHU original_tag = BOL
		original_tag = BOS original_tag = BOT original_tag = BRA original_tag = BRU original_tag = BUL
		original_tag = BFA original_tag = BUR original_tag = VER original_tag = CBD original_tag = CAM
		original_tag = CAN original_tag = CAR original_tag = CHA original_tag = CHL original_tag = CHI
		original_tag = COL original_tag = COM original_tag = CNG original_tag = COS original_tag = CDI
		original_tag = CRO original_tag = CUB original_tag = CYP original_tag = CZE original_tag = NKO
		original_tag = DRC original_tag = DEN original_tag = DJI original_tag = DOM original_tag = ECU
		original_tag = EGY original_tag = ELS original_tag = EGU original_tag = ERI original_tag = EST
		original_tag = ETH original_tag = FIJ original_tag = FIN original_tag = FRA original_tag = GAB
		original_tag = GAM original_tag = GEO original_tag = GER original_tag = GAH original_tag = GRE
		original_tag = GRA original_tag = GUA original_tag = GUI original_tag = GUB original_tag = GUY
		original_tag = HON original_tag = HUN original_tag = ICE original_tag = RAJ original_tag = IND
		original_tag = PER original_tag = IRQ original_tag = IRE original_tag = ISR original_tag = ITA
		original_tag = JAM original_tag = JAP original_tag = JOR original_tag = KAZ original_tag = KEN
		original_tag = KIR original_tag = KUW original_tag = KYR original_tag = LAO original_tag = LEB
		original_tag = LES original_tag = LIB original_tag = LBA original_tag = LIT original_tag = LUX
		original_tag = MAD original_tag = MAR original_tag = MLW original_tag = MAY original_tag = MLD
		original_tag = MAL original_tag = MLT original_tag = MAU original_tag = MRT original_tag = MEX
		original_tag = MIC original_tag = MON original_tag = MNT original_tag = MOR original_tag = MOZ
		original_tag = BRM original_tag = NAM original_tag = NAU original_tag = NEP original_tag = HOL
		original_tag = NZL original_tag = NIC original_tag = NGR original_tag = NIG original_tag = NOR
		original_tag = OMA original_tag = PAK original_tag = PAN original_tag = PAP original_tag = PAR
		original_tag = PAU original_tag = PER original_tag = PHI original_tag = POL original_tag = POR
		original_tag = QAT original_tag = KOR original_tag = MLV original_tag = ROM original_tag = SAM
		original_tag = SOV original_tag = RWA original_tag = STK original_tag = STL original_tag = SAO
		original_tag = SAU original_tag = SEN original_tag = SER original_tag = SEY original_tag = SIE
		original_tag = SIN original_tag = SLO original_tag = SLV original_tag = SOL original_tag = SOM
		original_tag = SAF original_tag = SSU original_tag = SPR original_tag = SRI original_tag = SUD
		original_tag = SUR original_tag = SWA original_tag = SWE original_tag = SWI original_tag = SYR
		original_tag = TAJ original_tag = SIA original_tag = FYR original_tag = TIM original_tag = TOG
		original_tag = TON original_tag = TRI original_tag = TUL original_tag = TUN original_tag = TUR
		original_tag = TRK original_tag = UGA original_tag = UKR original_tag = UAE original_tag = ENG
		original_tag = TNZ original_tag = USA original_tag = URG original_tag = UZB original_tag = VAN
		original_tag = VEN original_tag = VIE original_tag = YEM original_tag = ZAM original_tag = ZIM
	}
}

IRQ_is_selected_state_iraq = {
	OR = {
		has_country_flag = IRQ_anbar_selected
		has_country_flag = IRQ_karbala_selected
		has_country_flag = IRQ_najaf_selected
		has_country_flag = IRQ_muthanna_selected
		has_country_flag = IRQ_sinjar_selected
		has_country_flag = IRQ_nineveh_selected
	#	has_country_flag = IRQ_sulaymaniyah_selected
		has_country_flag = IRQ_wasit_selected
		has_country_flag = IRQ_diyala_selected
		has_country_flag = IRQ_babylon_selected
		has_country_flag = IRQ_basrah_selected
	#	has_country_flag = IRQ_dohuk_selected
	#	has_country_flag = IRQ_erbil_selected
		has_country_flag = IRQ_salahuddin_selected
		has_country_flag = IRQ_dhi_qar_selected
		has_country_flag = IRQ_maysan_selected
		has_country_flag = IRQ_kirkuk_selected
		has_country_flag = IRQ_qadisiyah_selected
	}
}

is_french_africa_nation = {
	OR = {
		tag = MRT # Mauritius
		tag = COM # Comoros
		tag = MAD # Madagascar
		tag = CNG # Republic of the Congo
		tag = DRC # Democratic People's Republic of the Congo
		tag = GAB # Gabon
		tag = CAM # Cameroon
		tag = CAR # Central African Republic
		tag = CHA # Chad
		tag = NGR # Niger
		tag = BEN # Benin
		tag = TOG # Togo
		tag = SUD # Sudan (Not technically a French nation but will be used later in the Sahel content)
		tag = ERI # ERitrea (Not technically a French africa nation but will be used later in the Sahel content)
		tag = BFA # Burkino Faso
		tag = CDI # Ivory Coast
		tag = GUI # Guinea
		tag = SEN # Senegal
		tag = MAL # Mali
		tag = MAU # Mauretania
		tag = MOR # Morocco
		tag = ALG # Algeria
		tag = TUN # Tunisia
		tag = DJI # Djibouti
		tag = SEY # Seychelles
		tag = SEN # Senegal
		tag = GUB # Guniea-Bissau
	}
}

is_sahel_nation = {
	OR = {
		tag = SEN
		tag = MAU
		tag = BFA
		tag = NGR
		tag = CHA
		tag = SUD
	}
}

is_central_african_nation = {
	OR = {
		tag = DRC
		tag = AGL
		tag = GAB
		tag = CAM
		tag = EGU
		tag = SAO
		tag = CHA
		tag = CAR
	}
}

mediterranean_tags = { #List of Mediterranenan tags for AI
	OR = {
		original_tag = ITA
		original_tag = SYR
		original_tag = TUR
		original_tag = SPR
		original_tag = ITA
		original_tag = FRA
		original_tag = EGY
		original_tag = MNC
		original_tag = NCY
		original_tag = CYP
		original_tag = GRE
		original_tag = SLV
		original_tag = CRO
		original_tag = MNT
		original_tag = ALB
		original_tag = ISR
		original_tag = HAM
		original_tag = SCL
		original_tag = MLT
		original_tag = SAR
		original_tag = CAT
		original_tag = BOS
		original_tag = LIB
		original_tag = TUN
		original_tag = MOR
		original_tag = ALG
	}
}
