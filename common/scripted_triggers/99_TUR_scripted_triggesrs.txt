TUR_is_kemalist = {
	custom_trigger_tooltip = {
		tooltip = TUR_IS_KEMALIST_TT
		OR = {
			is_in_array = { ruling_party = 1 }
			is_in_array = { ruling_party = 3 }
			is_in_array = { ruling_party = 4 }
			is_in_array = { ruling_party = 18 }
			is_in_array = { ruling_party = 22 }
		}
	}
}