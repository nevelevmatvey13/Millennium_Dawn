# Author(s): LordBogdanoff

BLR_targeted_country_has_more_than_70_percent_influence = {
	check_variable = { influence_array^0 = BLR }
	check_variable = { influence_array_val^0 > 79.999 }
}

SOV_targeted_country_has_more_than_75_percent_influence = {
	check_variable = { influence_array^0 = SOV }
	check_variable = { influence_array_val^0 > 74.999 }
}

CHI_targeted_country_has_more_than_70_percent_influence = {
	check_variable = { influence_array^0 = CHI }
	check_variable = { influence_array_val^0 > 69.999 }
}