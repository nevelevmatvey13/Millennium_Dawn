# Author(s): Aug
ideas = {
	country = {
		MNC_city_of_casino = {
			picture = new_deal
			modifier = {
				stability_factor = 0.1
				production_speed_buildings_factor = 0.05
				political_power_factor = -0.05
				corporate_tax_income_multiplier_modifier = 0.10
			}
		}
	}
}
