ideas = {
	tank_manufacturer = {
		designer = yes
		EGY_kader_factory_tank_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_kader_factory_tank_manufacturer" }
			picture = AOI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.093
			}

			traits = { CAT_afv_3 }
			ai_will_do = {
				factor = 1
			}
		}
		EGY_helwan_machine_tools_tank_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_helwan_machine_tools_tank_manufacturer" }
			picture = AOI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.062
			}

			traits = { CAT_armor_2 }
			ai_will_do = {
				factor = 1
			}
		}
		EGY_abu_zaabal_tank_factory_tank_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_abu_zaabal_tank_factory_tank_manufacturer" }
			picture = Abu_Zaabal_Factory
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_armor = 0.155
			}

			traits = { CAT_armor_5 }
			ai_will_do = {
				factor = 1
			}
		}
		EGY_abhco_tank_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_abhco_tank_manufacturer" }
			picture = AOI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_heli = 0.093
			}

			traits = { CAT_heli_3 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	materiel_manufacturer = {
		designer = yes
		EGY_kader_factory_materiel_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_kader_factory_materiel_manufacturer" }
			picture = AOI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf = 0.093
			}

			traits = { CAT_inf_3 }
			ai_will_do = {
				factor = 1
			}
		}

		EGY_maadi_arms_materiel_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_maadi_arms_materiel_manufacturer" }
			picture = AOI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_inf_wep = 0.124
			}

			traits = { CAT_inf_wep_4 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	aircraft_manufacturer = {
		designer = yes
		EGY_aoi_aircraft_aircraft_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_aoi_aircraft_aircraft_manufacturer" }

			picture = AOI
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_l_fighter = 0.093
			}

			traits = { CAT_l_fighter_3 }
			ai_will_do = {
				factor = 1
			}
		}
	}

	naval_manufacturer = {

		designer = yes

		EGY_alexandria_shipyard_naval_manufacturer = {
			allowed = { original_tag = EGY }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea EGY_alexandria_shipyard_naval_manufacturer" }

			picture = Alexandria_Shipyard
			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_naval_eqp = 0.062
			}

			traits = {
				CAT_naval_eqp_2

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}
