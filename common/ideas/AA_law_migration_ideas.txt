ideas = {
	migration_laws = {
		law = yes
		use_list_view = yes
		open_borders = {
			picture = migration_open_borders
			allowed_civil_war = { always = yes }

			on_add = {
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea open_borders"
			}

			removal_cost = -1
			level = 1
			cost = 100

			available = {
				NOT = {
					has_government = nationalist
					has_government = fascism
				}
				if = {
					limit = {
						OR = {
							has_idea = slightly_regulated_immigration
							has_idea = regulated_immigration
							has_idea = heavily_regulated_immigration
							has_idea = closed_borders
						}
						migration_law_decrease_blocked = yes
					}
					migration_law_decrease_blocked = no
				}
			}
			modifier = {
				base_migration_rate_value = 0.50
				drift_defence_factor = -0.20
				custom_modifier_tooltip = open_borders_tt
			}

			ai_will_do = {
				base = 0
			}
		}

		slightly_regulated_immigration = {
			picture = migration_regulation_light
			allowed_civil_war = { always = yes }

			on_add = {
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea slightly_regulated_immigration"
			}

			removal_cost = -1
			level = 2
			cost = 100

			available = {
				if = {
					limit = {
						OR = {
							has_idea = regulated_immigration
							has_idea = heavily_regulated_immigration
							has_idea = closed_borders
						}
						migration_law_decrease_blocked = yes
					}
					migration_law_decrease_blocked = no
				}
				if = {
					limit = {
						has_idea = open_borders
						migration_law_increase_blocked = yes
					}
					migration_law_increase_blocked = no
				}
			}
			modifier = {
				base_migration_rate_value = 0.40
				drift_defence_factor = -0.15
				nationalist_outlook_campaign_cost_modifier = -0.05
				custom_modifier_tooltip = slightly_regulated_immigration_tt
			}

			ai_will_do = {
				base = 0
			}
		}

		regulated_immigration = {
			picture = migration_regulation_medium
			allowed_civil_war = { always = yes }

			on_add = {
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea regulated_immigration"
			}

			default = yes
			removal_cost = -1
			level = 3
			cost = 100

			available = {
				if = {
					limit = {
						OR = {
							has_idea = heavily_regulated_immigration
							has_idea = closed_borders
						}
						migration_law_decrease_blocked = yes
					}
					migration_law_decrease_blocked = no
				}
				if = {
					limit = {
						OR = {
							has_idea = open_borders
							has_idea = slightly_regulated_immigration
						}
						migration_law_increase_blocked = yes
					}
					migration_law_increase_blocked = no
				}
			}
			modifier = {
				base_migration_rate_value = 0.20
				drift_defence_factor = -0.10
				nationalist_outlook_campaign_cost_modifier = -0.10
				custom_modifier_tooltip = regulated_immigration_tt
			}

			ai_will_do = {
				base = 0
			}
		}

		heavily_regulated_immigration = {
			picture = migration_regulation_hard
			allowed_civil_war = { always = yes }

			on_add = {
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea heavily_regulated_immigration"
			}
			removal_cost = -1
			level = 4
			cost = 100

			available = {
				if = {
					limit = {
						has_idea = closed_borders
						migration_law_decrease_blocked = yes
					}
					migration_law_decrease_blocked = no
				}
				if = {
					limit = {
						OR = {
							has_idea = regulated_immigration
							has_idea = open_borders
							has_idea = slightly_regulated_immigration
						}
						migration_law_increase_blocked = yes
					}
					migration_law_increase_blocked = no
				}
			}
			modifier = {
				base_migration_rate_value = 0.10
				drift_defence_factor = 0.10
				nationalist_outlook_campaign_cost_modifier = -0.15
				custom_modifier_tooltip = heavily_regulated_immigration_tt
			}

			ai_will_do = {
				base = 0
			}
		}

		closed_borders = {
			picture = migration_close_borders
			allowed_civil_war = { always = yes }

			on_add = {
				ingame_update_setup = yes
				log = "[GetDateText]: [Root.GetName]: add idea closed_borders"
			}
			removal_cost = -1
			level = 5
			cost = 100

			available = {
				if = {
					limit = {
						OR = {
							has_idea = open_borders
							has_idea = slightly_regulated_immigration
							has_idea = regulated_immigration
							has_idea = heavily_regulated_immigration
						}
						migration_law_increase_blocked = yes
					}
					migration_law_increase_blocked = no
				}
			}
			modifier = {
				base_migration_rate_value = 0.05
				drift_defence_factor = 0.20
				nationalist_outlook_campaign_cost_modifier = -0.20
				custom_modifier_tooltip = closed_borders_tt
			}

			ai_will_do = {
				base = 0
			}
		}
	}
}
