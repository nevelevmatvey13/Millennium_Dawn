ideas = {

	materiel_manufacturer = {

		designer = yes

		BRM_burma_defence_industries_materiel_manufacturer = {
			allowed = { original_tag = BRM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRM_burma_defence_industries_materiel_manufacturer" }

			picture = Arsenal_BUL

			cost = 150

			removal_cost = 10

			research_bonus = {
				CAT_inf = 0.062
			}

			traits = {
				CAT_inf_2

			}
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {

		designer = yes

		BRM_burma_defence_industries_tank_manufacturer = {
			allowed = { original_tag = BRM }
			on_add = { log = "[GetDateText]: [Root.GetName]: add idea BRM_burma_defence_industries_tank_manufacturer" }

			picture = Arsenal_BUL
			cost = 150
			removal_cost = 10
			research_bonus = {
				CAT_afv = 0.062
			}

			traits = {
				CAT_afv_2

			}
			ai_will_do = {
				factor = 1
			}
		}
	}
}