ideas = {
	country = {
		corruption_decreased_adm_spending = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_decreased_adm_spending"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				bureaucracy_cost_multiplier_modifier = -0.4
			}
		}
		corruption_decreased_police_spending = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_decreased_police_spending"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				police_cost_multiplier_modifier = -0.35
			}
		}
		corruption_decreased_mil_spending = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_decreased_mil_spending"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				army_personnel_cost_multiplier_modifier = -0.25
				navy_personnel_cost_multiplier_modifier = -0.25
				airforce_personnel_cost_multiplier_modifier = -0.25
				equipment_cost_multiplier_modifier = -0.25
				army_attack_factor = -0.1
				army_defence_factor = -0.1
			}
		}
		corruption_decreased_edu_spending = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_decreased_edu_spending"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				education_cost_multiplier_modifier = -0.5
			}
		}
		corruption_decreased_health_spending = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_decreased_health_spending"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				health_cost_multiplier_modifier = -0.25
			}
		}
		corruption_decreased_social_spending = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_decreased_social_spending"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				social_cost_multiplier_modifier = -0.25
			}
		}
		corruption_civ_construction = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_civ_construction"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				production_speed_industrial_complex_factor = 0.3
			}
		}
		corruption_mil_construction = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_mil_construction"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				production_speed_arms_factory_factor = 0.3
			}
		}
		corruption_construction_buff_idea = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_construction_buff_idea"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				production_speed_buildings_factor = 0.2
			}
		}
		corruption_military_prod_buff_idea = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_military_prod_buff_idea"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				industrial_capacity_factory = 0.20
			}
		}
		corruption_resource_buff_idea = {
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea corruption_resource_buff_idea"
				ingame_update_setup = yes
			}
			picture = puppet_master
			modifier = {
				resource_export_multiplier_modifier = 0.3
			}
		}
	}
}