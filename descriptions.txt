[img]https://i.imgur.com/hpEdk0J.png[/img]
[img]https://i.imgur.com/MYaznE7.png[/img]

With the dawn of a new millennium, a new chapter in human history is about to be written. Globalization continues to change the lives of countless billions while rising extremism is showing signs of igniting the fires of conflict. New powers begin to emerge, and not all believe their place in the global order, or that of some competitors, is warranted. A struggle will soon begin, and it will likely change everything. Will you lead your Nation to prosperity and dominance in the decades ahead, or will your Nation fall victim to the defining moments of history?

[b][i]Welcome to Millennium Dawn.[/i][/b]

Millennium Dawn is a multi-mod project set in 2000 and continues to the modern day and beyond. The mod boasts new and unique tech trees, focus trees, events, and decisions to immerse you in the intricacies of the modern era.

Further, it offers a new and unique economic system, a new political system, national taxation and debt, custom internal political factions, international influence mechanics, custom 3D models, a custom soundtrack, and much more!

[img]https://i.imgur.com/pfoT6n3.png[/img]

[list][*]New and Unique Map (new provinces, real borders, states, resources, industry, and population)
[*]New Countries (all with portraits, correct statistics, and military leaders)
[*]New Economic System (development, debt, corruption, and budget management)
[*]New Tech Tree (extending from 1965-2035)
[*]New Equipment and Unit types (all made from scratch to make combat a completely new experience)
[*]All countries have highly detailed and accurate armies, navies, aircraft, and equipment based on real-world statistics collected by the IISS's [i]The Military Balance 2016,[/i] SIPRI's [i]Trends in International Arms Transfers 2016,[/i] and FlightGlobal's [i]World Air Forces 2015.[/i]
[*]Detailed Civil Wars (if it exists, it's here)
[*]Modern 3D models
[*]New Political System
[*]Custom Internal Faction System
[*]Custom Sphere of Influence Mechanics
[*]Custom Missile System
[*]Custom European Union System
[*]All new modern sound effects[/list]

[h2]Focus Trees as of v1.10.1[/h2]
[table equalcells=1]
[tr][td]Abkhazia[/td][td]Afghanistan[/td][td]Armenia[/td][td]Artsakh[/td][td]Azerbaijan[/td][td]Bashkiriyia[/td][td]Belarus[/td][td]Bosnia[/td][/tr]
[tr][td]Botswana[/td][td]Brazil[/td][td]Bulgaria[/td][td]Canada[/td][td]Central Asia Shared Tree[/td][td]Chechyna[/td][td]China[/td][td]Comoros[/td][/tr]
[tr][td]Crimea[/td][td]Cuba[/td][td]Denmark[/td][td]Donetsk People's Republic[/td][td]Egypt[/td][td]Ethiopia[/td][td]EU POTEF Shared[/td][td]EU USoE Shared[/td][td]Fiji[/td][/tr]
[tr][td]Finland[/td][td]France[/td][td]Georgia[/td][td]Germany[/td][td]Greece[/td][td]Gulf Cooperation Council Shared[/td][td]Gulf Shared Tree[/td][td]Hezbollah[/td][/tr]
[tr][td]India[/td][td]Indonesia[/td][td]Iran[/td][td]Italy[/td][td]Iraq[/td][td]Japan[/td][td]Khanti-Mansi[/td][td]Kharkiv[/td][td]Kosovo[/td][/tr]
[tr][td]Liechtenstein[/td][td]Luhansk People's Republic[/td][td]Malorossiya[/td][td]Montenegro[/td][td]Myanmar[/td][td]Nigeria[/td][td]North Korea[/td][td]Norway[/td][/tr]
[tr][td]Odessa People's Republic[/td][td]Poland[/td][td]Russia[/td][td]San Marino[/td][td]Serbia[/td][td]Singapore[/td][td]South Korea[/td][td]South Ossetia[/td][/tr]
[tr][td]Spain[/td][td]Subjects of Russia Shared[/td][td]Sweden[/td][td]Switzerland[/td][td]Syria[/td][td]Tatarstan[/td][td]Transnistria[/td][td]Turkey[/td][/tr]
[tr][td]Ukraine[/td][td]United Kingdom[/td][td]United States[/td][td]Venezuela[/td][td]Vitebsk[/td][td]Vojovdina[/td][td]Wagner (Sahel)[/td][/tr]
[/table]

[h1][url=https://www.youtube.com/watch?v=9G6lYnP0knI&list=PL36TqZI0G592x3sYphwPHuMvobA6si543][b]Tutorials for v1.10.*[/b][/url][/h1]

[b]Current Version:[/b] 1.10.1
[b]Current HOI4 Version:[/b] 1.14.*
[b]Supported Languages:[/b] English, French, Russian, Simplified Chinese
[b]Expected Checksum:[/b] 9618

Most recent changelog can be found using the changelog feature on the workshop, Discord, GitLab Wiki, or in the source code for the mod.

[h3]Language Submods[/h3]
[url=https://steamcommunity.com/sharedfiles/filedetails/?id=1295588320]Korean[/url]
[url=https://steamcommunity.com/sharedfiles/filedetails/?id=3017970403]Polish[/url]
[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2778141436]Japanese[/url]
[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2936366779]Brazilian Portuguese[/url]

If you know of any other translation submods that are available on the workshop, please let one of the team members know so we can include in the list above.

[b]What can we expect in Future Releases?[/b]
Future updates will include things such as balance changes, new national focus trees, new mechanics, flavor content, improved graphics, performance improvements and all types of changes.
The roadmap can be found in our Discord with more detailed information about specific currently upcoming changes or reworks.

[h1][b]Disclaimer![/b][/h1]
Millennium Dawn has a much higher system requirement then vanilla Hearts of Iron IV. It is recommended you have at least the recommended specs to have the most enjoyable experience with Millennium Dawn.
We are constantly working on performance and as such we will continue to improve the playability for people with all types of PCs.

[h3]Common Issues & FAQ[/h3]
[b][list][*]Do not use Steam Cloud Saves. They cloud saves often break due to the mod file save game size.
[*]Do not use other mods or submods with Millennium Dawn unless it expressly states that it is compatible with current version of Millennium Dawn. MD does not play nicely with any other mods due to the size and scope of the changes.
[*]Any crash/bug report or balance issues reported to the Millennium Dawn team when your save/report does not contain the expected checksum we will ignore or disregard the reported issue.
[*]Please write to us about errors on our Discord in our Bug Report Forum. We track our bug reports there.
[*]Millennium Dawn does not require you have any specific combination of DLC, but we recommend having all to get the full experience of Millennium Dawn.
[/list][/b]

[img]https://i.imgur.com/2wL9t3y.png[/img]

[h1][b]Join our Discord Server and Follow Us on Social Media![/b][/h1]

The best place to reach us is Discord. We are most active on there, however, we do offer alternatives like Twitter or Reddit.
[url=https://discord.gg/millenniumdawn]Discord[/url]
[url=https://twitter.com/millenniumdawn?lang=en]Twitter[/url]
[url=https://www.reddit.com/r/MillenniumDawn/]Reddit[/url]
[url=https://vk.com/md_hoi4_mod]VKontakte (if you know the Russian language)[/url]

[b]Additional Credits for 3D Models:[/b]
Cyrus Jackson for sharing his models and [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1458561226]Cold War: Iron Curtain[/url].
Camo pack by Jeff Kleinzweig.
Some texture by 迷彩図鑑
Some texture by armeeoffizier.ch
BRA camo texture by Guilmann
French Centre Europe Camo by Thimbleweed
Hungary Camo by MrGnodski
Special thanks to [url=https://steamcommunity.com/sharedfiles/filedetails/?id=1521695605]KaiserRedux[/url] team for focus and ideas gfx sharing!

[b][i]Happy playing![/i][/b]
