﻿# Millennium Dawn CONTRIBUTOR LIST
# If you contributed, but are not listed here, contact: Bird
#
# This is in no way an exhaustive list of contributors and if you are missing and have proof of your work. Please let Bird know on the MD Discord.
#
# Feel free to add yourself to this when creating a pull
# request, preferably including gitLab name PDX plaza name and
# a email address. Additional information then name is optional.
# Format: "CommonName" (Discord: %) (GitLab: %) (PDX: %) <my@email.>
# Contributors
Ted52 (Discord: @Ted52) (Gitlab: @Ted52)
Angriest Bird (Discord: @Angriest Bird) (Gitlab: @AngriestBird)
Brostrom.A | Evul (Gitlab: @ColdEvul) (PDX: Cold Evil) <andreas.brostrom.ce@gmail.com>
Niko92 (Discord: @Kalkalash) (Gitlab: @Kalkalash) (PDX: Niko92) <nikoholkko@hotmail.com>
MSG (Discord: @MSG#9269) (Gitlab: @MSG2735) (PDX: MSG2735)
HansNery (Discord: @Hans#6833) (PDX: hansnery) <hansnery@gmail.com>
Hiddengearz (Discord: @hiddengearz#0792) (Gitlab: truemikesmith) (PDX: Hiddengearz) <truemikesmith@gmail.com>
roemer9 (Discord: roemer9#3321) (Gitlab: tantow23) (PDX: roemer9)
rafat (Discord: ArabianGeneral#6432) (Gitlab: @rafatstudios) <rafatstudios@gmail.com>
Dumaresq (Discord: Dumaresq#2890) (Gitlab: @Dumaresq) <florian.brun111@ gmail.com>
LSJHoward (Discord: LSJHoward#6181) <LSJHoward@me.com>
Roseru (Discord: Roseru#5160) (GitLab: Roseru1)
crocomoth (Discord: crocomoth#9256) (GitLab: crocomoth)
alexmarkel0v (Discord: alexmarkel0v#8167) (Gitlab: alexmarkel0v) <alexandermarkel0v53@gmail.com>
BoNDeX (Discord: BoNDeX#4624) (Gitlab: BoNDeXx) <jnothingtosay@gmail.com>
Curious Beats (Discord: Curious Beats#2276) (Gitlab/Github: CuriousBeats) (PDX: Curious Beats) <poonslyer6666669@gmail.com>
Patrador (Discord: Patrador#8249)
Fire_hair (Discord: Fire_hair) (Gitlab: Fire_hair)
Simone (Discord: Simone - Traiano#3209) (Gitlab: @TraianoGitHub)
_Levia (Discord: Water Witch#8669) (GitLab: AriaOfWater)
heastel (Discord: heastel#3381) (GitLab: heastel) <deminstepan2607@gmail.com>
Jakubzeml (Discord: Jakubzeml#2126)
Warner (Discord: Warner#8513) (GitLab: @WarnerDev) (PDX: Warner.py) <warner.py@gmail.com>
Кусь (rockon) (Discord: @rockon_ru)
Mopikel (Discord: Mopikel#1412) <mopikel.music@gmail.com>
DROID (Discord: TheBrokenDROID/MD's BrokenDROID)
Luigi IV (Discord: Luigi IV#8053)
Rowan (Discord: @rowx87) (GitLab: @rowx87)
pheyonix (Discord: @pheyonix)
PAKMAN (Discord: @whysmthalwayshappeningtopakman)
DarkCloud (Discord: @theinvisibleworldhegemon)
Mahhouse (Discord: @mahhouse)
Ddraig/Kanthier
MrPotter
Cybergev
FGR_UNN
Tanktema
ACTrigger
Dpurdy (Discord: @dpurdy)
DeviX8211
Ночной Кальмар
Kor
Killerrabbit
MathiasK
Walcanarus
Manchu Yu
Wigge
Omer B.
RanadianMan
Amtoj (Discord: @amtoj)
Scarecroww
Captain Gen
Ducky the Anti-Pope
Strategy Gamer
Icelordcryo
Pengu
Stalin Wasn't Stalin
Evanw1256
AP
CelsiuZ
Fischyk
Arvidus
CaptainDread
FyingKiwi
Lifey
HappyNTH
Yard1
Clucknorris
Hectormuozceballos
Griff
Mikey
Nick
Olorin
Prince of Babylon
Anni
Reptiljv9
Spontaneous Papaya
Thryn
Vadim
Vespinosa
Wilhelm
Zab
Buggy347
Tasos303 (Discord: @tasos303)
Bobguy/XilingoHotel
Rafael
Decerno
Divexz
Grim
Indyclone77
Londinium
Papinian7
Razmode
TESTUDO (Discord: @true_testudo)
Akuukis
ArabianGeneral
Toonu
Laven (Discord: @laven_2114)
Ba'athbomb
Blackice91
CenZ
Civi
fateweaver
Clientpozzedon
Dictatorial Karelian
Dinisdro
Edek
Emmisy Squire
Qwite
Raymondchoo
higuys
Hexcron
Grestin
AranhaBolt
Bluehunter
dc83
Levi
Magical Chicken
Mr. Tediore
RussianArmyToday
The_Ghost
SaintlyStorm16
GabbyH
Tasos303
Yekbun
Momokio
Aug (Discord: @aug3979)
Frog57
Salamin (Discord: @.salamin)
Reid (Discord: @[GR]Reid1651#4029)
Hammurabae (Discord: @hammurabae)
Scotch (Discord: @scotch152)
Maki (Discord: @maki0191)
Jame (Discord: @_j3j.)
PokemonUnited (Discord: @PokemonUnited#4149)
Darius (Discord: @dariushparsi)
TheGeneral (Discord: @thegeneral2061)
Malkonzo
Zorkan Azhret (Discord: Zorka Azhret)
Wolfpack
kaheti_carturi
Arsdor
BiometricNuke


Fellow Modders/Teams:
swf
libra
Tallcastle (Agency Submod creator) Source: https://steamcommunity.com/sharedfiles/filedetails/?id=2428791468
Orphanmaker(Indian icons) Source: https://steamcommunity.com/sharedfiles/filedetails/?id=1852163517, https://steamcommunity.com/sharedfiles/filedetails/?id=2814175841
Stjern (Total War mod for adaptation of Counter UI) Source: https://steamcommunity.com/sharedfiles/filedetails/?id=806209426&searchtext=total+war
KaiserRedux Team(For Sharing Icons) Source: https://steamcommunity.com/sharedfiles/filedetails/?id=2076426030
Sinuiju chan(MD PLA  general resetting) Source: https://steamcommunity.com/sharedfiles/filedetails/?id=3138331129

Millennium Dawn voice lines:
'Truce' Italian voicelines
'OddballHam' British voicelines
'McLain Hirschbuhl' American voicelines
'JohnBuLongDon' Chinese (mandarin) voicelines
'AzerMG' French voicelines
'Taruguete' Spanish voicelines
'Harsh Vasudatta Thakur' Hindi voicelines
'AlpArslan' Turkish voicelines

3D Models
Camo pack by Jeff Kleinzweig.
Some texture by 迷彩図鑑
Some texture by armeeoffizier.ch
BRA camo texture by Guilmann
French Centre Europe Camo by Thimbleweed
Hungary Camo by MrGnodski