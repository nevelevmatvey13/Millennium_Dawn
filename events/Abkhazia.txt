add_namespace = abkh

### Start

country_event = {
	id = abkh.1
	title = abkh.1.t
	desc = abkh.1.d
	
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = abkh.1.a
		log = "[GetDateText]: [This.GetName]: abkh.1.a executed"
		ai_chance = {
			factor = 100
		}
		add_ideas = CIS_two_member_state_idea
	}
	option = {
		log = "[GetDateText]: [This.GetName]: abkh.1.b executed"
		name = abkh.1.b
		ai_chance = {
			factor = 0
		}
	}
}


country_event = {
	id = abkh.2
	title = abkh.2.t
	desc = abkh.2.d
	
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = abkh.2.a
		log = "[GetDateText]: [This.GetName]: abkh.2.a executed"
		ai_chance = {
			factor = 100
		}
		ABK = { add_to_faction = SOO }
	}
	option = {
		name = abkh.2.b
		log = "[GetDateText]: [This.GetName]: abkh.2.b executed"
		ai_chance = {
			factor = 0
		}
	}
}

country_event = {
	id = abkh.3
	title = abkh.3.t
	desc = abkh.3.d
	
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = abkh.3.a
		log = "[GetDateText]: [This.GetName]: abkh.3.a executed"
		division_template = {
			name = "Pyatnashka Battalion"
			regiments = {
				Special_Forces = { x = 0 y = 0 }
				Special_Forces = { x = 0 y = 1 }
				Special_Forces = { x = 1 y = 0 }
			}
			priority = 2
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 693 }
			create_unit = {
				division = "name = \"Pyatnashka Battalion\" division_template = \"Volunteer Battalion\" start_experience_factor = 0.2"
				owner = NOV
			}
		}
		ai_chance = {
			factor = 70
		}

	}
	
}
country_event = {
	id = abkh.4
	title = abkh.4.t
	desc = abkh.4.d
	
	fire_only_once = yes
	is_triggered_only = yes

	option = {
		name = abkh.4.a
		log = "[GetDateText]: [This.GetName]: abkh.4.a executed"
		division_template = {
			name = "Pyatnashka Battalion"
			regiments = {
				Special_Forces = { x = 0 y = 0 }
				Special_Forces = { x = 0 y = 1 }
				Special_Forces = { x = 1 y = 0 }
			}
			priority = 2
		}
		random_owned_controlled_state = {
			limit = { ROOT = { has_full_control_of_state = PREV } }
			prioritize = { 693 }
			create_unit = {
				division = "name = \"Pyatnashka Battalion\" division_template = \"Volunteer Battalion\" start_experience_factor = 0.2"
				owner = DPR
			}
		}
		ai_chance = {
			factor = 70
		}

	}
	
}

