instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Aero L-39"
		amount = 25
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-21s Bis"
		amount = 80
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-23"
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Su-17"
		amount = 80
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-12"
		amount = 35
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 15
		producer = SOV
	}
}
