﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aermacchi MB-339"
		type = small_plane_strike_airframe_1
		amount = 5
		producer = ITA
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-23"
		type = small_plane_strike_airframe_1
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-21s Bis"
		type = small_plane_strike_airframe_1
		amount = 5
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-29 Fulcrum"
		type = small_plane_airframe_2
		amount = 4
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Harbin Y-12"
		type = large_plane_air_transport_airframe_1
		amount = 3
		producer = CHI
	}
}