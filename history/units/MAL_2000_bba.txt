﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Aero L-39"					#in place of L-29's, MiG-15's & Yaks
		type = small_plane_strike_airframe_1
		amount = 13
		producer = CZE
	}
	add_equipment_to_stockpile = {
		variant_name = "MiG-21s Bis"
		type = small_plane_strike_airframe_1
		amount = 11
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "Su-17"						#in place of MiG-17's
		type = small_plane_strike_airframe_1
		amount = 5
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 3
		producer = SOV
	}
}