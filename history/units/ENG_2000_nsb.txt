﻿division_template = {
	name = "Air Assault Brigade"

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
		L_Air_Inf_Bat = { x = 0 y = 2 }
		L_Air_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 0 y = 4 }
		Arty_Bat = { x = 1 y = 0 }
	}
}

division_template = {
	name = "Armoured Inf Brigade"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 3 }
		Mot_Inf_Bat = { x = 0 y = 4 }
		SP_Arty_Bat = { x = 1 y = 0 } #added in 1st, 2nd & 26th Royal Artillery
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
		armor_Recce_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Inf Brigade"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 0 y = 4 }
		Arty_Bat = { x = 1 y = 0 } #added in 3rd, 4th, 103rd & 105th Royal artillery
	}
}
division_template = {
	name = "Small Inf Brigade with Recon"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
	}
}

division_template = {
	name = "Inf Brigade with Recon"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
		Mot_Inf_Bat = { x = 0 y = 3 }
		Mot_Inf_Bat = { x = 0 y = 4 }
		Mot_Inf_Bat = { x = 1 y = 0 }
		Mot_Inf_Bat = { x = 1 y = 1 }
		Mot_Inf_Bat = { x = 1 y = 2 }
		L_Inf_Bat = { x = 2 y = 0 }
	}
}

division_template = {
	name = "Guards"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Special Forces"

	regiments = {
		Special_Forces = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Marine Regiment"

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
		L_Marine_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
		L_Inf_Bat = { x = 1 y = 0 }
		Arty_Bat = { x = 1 y = 1 }
	}
}

units = {
	division = {
		name = "22nd Special Air Service Regiment"
		location = 3241 #Herefordshire
		division_template = "Air Assault Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "Special Boat Service"
		location = 9458 #Poole
		division_template = "Special Forces"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st The Queen's Dragoon Guards"
		location = 377 #Cardiff
		division_template = "Small Inf Brigade with Recon"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "The Light Dragoons"
		location = 11270 #Catterick
		division_template = "Small Inf Brigade with Recon"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "16th Air Assault Brigade"
		location = 11374 #Colchester
		division_template = "Air Assault Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "7th Armoured Brigade"
		location = 6377 #Westfalen (Germany)
		division_template = "Armoured Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "20th Armoured Brigade"
		location = 6377 #Westfalen (Germany)
		division_template = "Armoured Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "1st Mechanised Brigade"
		location = 11471 #Bulford Camp
		division_template = "Armoured Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "4th Mechanised Brigade"
		location = 11471 #Bulford Camp
		division_template = "Armoured Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "12th Mechanised Brigade"
		location = 11471 #Bulford Camp
		division_template = "Armoured Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "8th Infantry Brigade"
		location = 385 #Derry
		division_template = "Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "42nd Infantry Brigade"
		location = 6335 #Preston
		division_template = "Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "51st Highland Brigade"
		location = 6385 #Stirling
		division_template = "Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "145th Brigade"
		location = 6351 #Aldershot
		division_template = "Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Commando Brigade"
		location = 6351 #Aldershot
		division_template = "Marine Regiment"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "39th Infantry Brigade"
		location = 3379 #Belfast
		division_template = "Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
	division = {
		name = "3rd Infantry Brigade"
		location = 3242 #Armagh
		division_template = "Inf Brigade"
		start_experience_factor = 0.75
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons2 #L85A1
		amount = 6332
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons3 #L85A2
		amount = 9450
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment3 #
		amount = 3158
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_2 #LAW
		amount = 900
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2 #Javelin
		amount = 120
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_2 #Starstreak
		amount = 595
	}

	#Vehicles
	add_equipment_to_stockpile = {
		type = util_vehicle_4 #Jackal
		amount = 400
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_3 #Jackal
		amount = 335
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 464 #scimitar + sabre
		producer = ENG
		variant_name = "FV107 Scimitar"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 11
		producer = GER
		variant_name = "TPz-Fuchs"
	}

	add_equipment_to_stockpile = {
		type = artillery_1 #L118 Light Gun
		amount = 166
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "M270"
		amount = 63
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spaa_hull_0
		amount = 57
		producer = ENG
		variant_name = "Rapier"
	}
	#####################
	#### Helicopters ####
	#####################
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_1
		amount = 67
		producer = USA
		variant_name = "AH-64 A/D Apache Longbow"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_1 #Lynx AH9A
		amount = 115
		producer = ENG
		variant_name = "Super Lynx"
	}
	add_equipment_to_stockpile = {
		type = attack_helicopter_hull_0 #Aérospatiale SA-342 Gazelle
		amount = 8
		producer = FRA
		variant_name = "SA-342 Gazelle"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 88
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Westland WG-13 Lynx
		amount = 65
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #AgustaWestland AW101
		amount = 38
		#version_name = "AgustaWestland AW101"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Boeing CH-47 Chinook
		amount = 38
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #AgustaWestland AW101
		amount = 22
		#version_name = "AgustaWestland AW101"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aerospatiale SA-330 Puma
		amount = 39
		#version_name = "Aerospatiale SA-330 Puma"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Sea King HU5
		amount = 32
		producer = USA
		#version_name = "Sea King HU5"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AS350B Ecureuil
		amount = 41
		producer = FRA
		#version_name = "AS350B Ecureuil"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 4
		producer = USA
	}

	if = {
		limit = { has_dlc = "Man the Guns" }
		add_equipment_production = {
			equipment = {
				type = frigate_hull_3
				creator = "ENG"
				version_name = "Type 23 Class"
			}
			requested_factories = 2
			progress = 0.8
			efficiency = 100
			amount = 1
		}
	}	
}