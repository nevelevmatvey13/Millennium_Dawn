﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1			#MiG-23 - numbers cut in half due to what is actually in operation
		amount = 15
		variant_name = "MiG-23"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1		#MiG-21 - numbers cut in half due to what is actually in operation
		amount = 40
		variant_name = "MiG-21s Bis"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = small_plane_airframe_2		#MiG-29 - numbers cut in half due to what is actually in operation
		amount = 3
		variant_name = "MiG-29 Fulcrum"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1		#Aero L-39
		amount = 25
		variant_name = "Aero L-39"
		producer = CZE
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#An-12 in place of An-2
		amount = 8
		variant_name = "An-12"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#An-26 in place of An-24
		amount = 16
		variant_name = "An-26"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#An-32 in place of An-30
		amount = 3
		variant_name = "An-32"
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1		#Il-76
		amount = 2
		variant_name = "Il-76"
		producer = SOV
	}


	#Helicopters

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Mi-8
		amount = 5
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1			#Mi-17
		#version_name = "Mil Mi-17"
		amount = 5
		producer = SOV
	}
}