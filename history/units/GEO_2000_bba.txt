﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "Su-25"
		type = medium_plane_cas_airframe_2
		amount = 7
		producer = SOV
	}
	add_equipment_to_stockpile = {
		variant_name = "An-26"
		type = large_plane_air_transport_airframe_1
		amount = 6
		producer = SOV
	}
}