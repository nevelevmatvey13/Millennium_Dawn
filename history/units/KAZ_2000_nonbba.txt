﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#L-39
		amount = 17
		producer = CZE
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter1		#MiG-23
		amount = 24
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2		#MiG-27
		#version_name = "MiG-27 Flogger"
		amount = 24
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2		#MiG-29
		amount = 40
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter2		#Su-24 Fencer
		amount = 37
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = AS_Fighter2		#MiG-31
		amount = 43
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = cas1		#SU-25
		amount = 25
		producer = SOV
	}
}