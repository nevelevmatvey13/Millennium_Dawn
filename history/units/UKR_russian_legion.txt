units = {
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 525
		division_template = "Foreign Volunteer Unit"
		start_experience_factor = 0.5
		start_equipment_factor = 0.9
	}
}