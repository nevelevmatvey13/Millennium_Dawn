﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = small_plane_airframe_1
		variant_name = "F-5E Tiger II"
		amount = 7
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = small_plane_cas_airframe_1
		variant_name = "A-5"					#in place of J-5
		amount = 5
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "J-6"
		amount = 7
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "F-7 Airguard"
		amount = 4
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "MiG-23"
		amount = 2
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "C-130 Hercules"
		amount = 4
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "DHC-4 Caribou"				#in place of DHC-5
		amount = 1
		producer = CAN
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Fokker F27 Friendship"
		amount = 1
		producer = HOL
	}

	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "Dassault Falcon 20"
		amount = 2
		producer = FRA
	}
}