﻿instant_effect = {
	add_equipment_to_stockpile = {
		variant_name = "A-37 Dragonfly"
		type = small_plane_strike_airframe_1
		amount = 13
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "F-5E Tiger II"
		type = small_plane_airframe_1
		amount = 11
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Embraer EMB-312 Tucano"
		type = small_plane_strike_airframe_1
		amount = 12
		producer = BRA
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"			#added additional in place of other variants we do not have
		type = large_plane_air_transport_airframe_1
		amount = 7
		producer = USA
	}
}