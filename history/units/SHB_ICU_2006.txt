division_template = {
	name = "Milishia ICU"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }

	}
}

division_template = {
	name = "Milishia"

	regiments = {
		Militia_Bat = { x = 0 y = 0 }
		Militia_Bat = { x = 0 y = 1 }

	}
}

units = {
	division = {
		name = "1. Milishia ICU"
		location = 13333
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "2. Milishia ICU"
		location = 558
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "3. Milishia ICU"
		location = 13876
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "4. Milishia ICU"
		location = 11014
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "5. Milishia ICU"
		location = 580
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "6. Milishia ICU"
		location = 13334
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "7. Milishia ICU"
		location = 580
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "8. Milishia ICU"
		location = 578
		division_template = "Milishia ICU"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "1. Milishia"
		location = 12941
		division_template = "Milishia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}

	division = {
		name = "2. Milishia"
		location = 12941
		division_template = "Milishia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.9
	}
}

instant_effect = {
	###A lot of encirclements, so equipment has to be given immediately to unis  ###
}
