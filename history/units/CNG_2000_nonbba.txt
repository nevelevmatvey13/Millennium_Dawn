﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = MR_Fighter1 			#MiG-21
		amount = 12
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_plane1 			#An-26
		amount = 1
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1 			#Mi-8
		amount = 2
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = transport_helicopter1 			#Mi-8
		#version_name = "Aérospatiale SA-316 Alouette III"
		amount = 2
		producer = FRA
	}
}