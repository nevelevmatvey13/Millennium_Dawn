﻿instant_effect = {
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AgustaWestland AW109
		amount = 20
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell UH-1 Iroquois
		amount = 27
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 206
		amount = 5
		producer = USA
		#version_name = "Bell 206"
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 214
		amount = 12
		producer = USA
		#version_name = "Bell 214"
	}
	add_equipment_to_stockpile = {
		type = transport_plane2 #C-212 Aviocar
		amount = 12
		producer = SPR
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter1 #F-5
		amount = 33
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = Strike_fighter1 #Dassault Mirage 5
		amount = 16
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2 #F-16
		amount = 21
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #Embraer EMB-312 Tucano
		amount = 32
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-316 Alouette III
		amount = 10
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter2 #Eurocopter AS532 Cougar
		amount = 4
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Hercules
		amount = 5
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #Aeritalia G.222
		amount = 8
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Aérospatiale SA-330 Puma
		amount = 8
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2 #T-38C Talon
		amount = 20
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = L_Strike_fighter1 #SIAI-Marchetti SF.260
		amount = 12
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AgustaWestland AW109
		amount = 4
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 214
		amount = 2
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #Bell 206
		amount = 20
		producer = ITA
	}
}