﻿division_template = {
	name = "Batalyon Dobrovolchev"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
		L_Inf_Bat = { x = 0 y = 3 }
	}

	priority = 0
}

division_template = {
	name = "Brigada Dobrovolchev"

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
	}

	priority = 0
}

division_template = {
	name = "Pekhotnaya Brigada"

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Motorizovannaya Brigada"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Batalyon Spetsnaza"

	regiments = {
		Mech_Inf_Bat = { x = 0 y = 0 }
		Mech_Inf_Bat = { x = 0 y = 1 }
		Mech_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		armor_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Mekhanizirovannaya Brigada"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
	}
	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
		L_Engi_Comp = { x = 0 y = 1 }
	}
}

division_template = {
	name = "Kazachya Brigada"

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		Arm_Inf_Bat = { x = 0 y = 1 }
		Arm_Inf_Bat = { x = 0 y = 2 }
		SP_Arty_Bat = { x = 0 y = 3 }
		armor_Bat = { x = 1 y = 0 }
	}
	support = {
		H_Engi_Comp = { x = 0 y = 0 }
	}

	priority = 2
}

division_template = {
	name = "Tankovyi Batalyon"

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
	}
	support = {
		armor_Recce_Comp = { x = 0 y = 0 }
	}
}

units = {
	#Donbass People's Militia
	division = {
		name = "Balayon 'Sever'"
		location = 3421
		division_template = "Batalyon Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 1.0
	}
	division = {
		name = "1-y Slavyanskaya Brigada"
		location = 6474
		division_template = "Motorizovannaya Brigada"
		force_equipment_variants = { apc_hull_0 = { owner = "SOV" version_name = "BTR-70" } }
		start_experience_factor = 0.5
		start_equipment_factor = 1.0
	}
	division = {
		name = "Brigada 'Vostok'"
		location = 3479
		division_template = "Pekhotnaya Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 1.0
	}
	division = {
		name = "Russkaya Pravoslavnaya Armiya"
		location = 6474
		division_template = "Brigada Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 1.0
	}
	division = {
		name = "Shakhtyorskaya Diviziya"
		location = 3479
		division_template = "Brigada Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 1.0
	}
	division = {
		name = "Brigada 'Kalmius'"
		location = 6505		#Debaltseve
		division_template = "Batalyon Dobrovolchev"
		start_experience_factor = 0.5
		start_equipment_factor = 1.0
	}
	division = {
		name = "Batalyon 'Voskhod'"
		location = 3421
		division_template = "Batalyon Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 1.0
	}
	division = {
		name = "Brigada 'Pyatnashka'"
		location = 6474		#Marinka
		division_template = "Brigada Dobrovolchev"
		start_experience_factor = 0.4
		start_equipment_factor = 1.0
	}
	division = {
		name = "Mariupolsko-Khinganskyi Morskaya Pekhota"
		location = 3421
		division_template = "Pekhotnaya Brigada"
		start_experience_factor = 0.5
		start_equipment_factor = 1.0
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons		 #AKM
		amount = 1500
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons1		 #AK-74
		amount = 1500
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons2		 #AK-74M
		amount = 1400
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_1		 #Fagot
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_2		 #Metis
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_2		 #Metis
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_1		 #Spiral
		#version_name = "AT-6 Spiral"
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_0
		variant_name = "T-55"
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-64"
		amount = 45
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = mbt_hull_1
		variant_name = "T-72B"
		amount = 41
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-1"
		amount = 21
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_0
		variant_name = "BMP-2"
		amount = 89
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-60"
		amount = 2
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "MT-LB"
		amount = 35
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "BTR-70"
		amount = 16
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = apc_hull_1
		variant_name = "BTR-80"
		amount = 18
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Rec_tank_0		 #BRDM-2
		amount = 9
		producer = UKR
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_0		 #Ural 4320
		amount = 150
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_1		 #UAZ-469
		amount = 70
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_3		 #Vodnik
		amount = 40
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = util_vehicle_4		 #BPM-97
		amount = 70
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = artillery_0		 #Various Soviet artys
		amount = 68
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S1 Gvozdika"
		amount = 11
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		version_name = "2S5 Giatsynt"
		amount = 1
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		version_name = "2S7 Pion"
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "2S3 Akatsiya"
		amount = 8
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "2S19 Msta"
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-21 Grad"
		amount = 19
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "BM-27 Uragan"
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "TOS-1"
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = spart_hull_1
		variant_name = "BM-30 Smerch"
		amount = 3
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0		 #Strela
		amount = 35
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1		 #Igla
		amount = 33
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_1		 #Grom
		amount = 38
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment
		amount = 890
		producer = SOV
	}
}