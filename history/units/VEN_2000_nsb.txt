﻿division_template = {
	name = "Brigadas de Blindados"

	division_names_group = VEN_ARMORED_BRIGADES

	regiments = {
		armor_Bat = { x = 0 y = 0 }
		armor_Bat = { x = 0 y = 1 }
		armor_Bat = { x = 0 y = 2 }
		Arm_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		SP_AA_Battery = { x = 0 y = 0 }
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Brigadas de Caribes"

	division_names_group = VEN_RANGER_BRIGADES

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		Special_Forces = { x = 0 y = 1 }
		Special_Forces = { x = 0 y = 2 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
	priority = 2
}

division_template = {
	name = "Brigada de Infantería de Selva"

	division_names_group = VEN_INF_BRIGADES

	regiments = {
		L_Inf_Bat = { x = 0 y = 0 }
		L_Inf_Bat = { x = 0 y = 1 }
		L_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}

division_template = {
	name = "Brigada de Infantería Motorizada"

	division_names_group = VEN_MOTORIZED_BRIGADES

	regiments = {
		Mot_Inf_Bat = { x = 0 y = 0 }
		Mot_Inf_Bat = { x = 0 y = 1 }
		Mot_Inf_Bat = { x = 0 y = 2 }
	}

	support = {
		Arty_Battery = { x = 0 y = 0 }
	}
}


division_template = {
	name = "Brigada de Paracaidistas"

	division_names_group = VEN_AIRBORNE_BRIGADES

	regiments = {
		L_Air_Inf_Bat = { x = 0 y = 0 }
		L_Air_Inf_Bat = { x = 0 y = 1 }
	}
}
division_template = {
	name = "Brigada de Infantería de Marina"

	division_names_group = VEN_MARINE_BRIGADES

	regiments = {
		L_Marine_Bat = { x = 0 y = 0 }
		L_Marine_Bat = { x = 0 y = 1 }
		L_Marine_Bat = { x = 0 y = 2 }
		L_Marine_Bat = { x = 0 y = 3 }
		Arty_Bat = { x = 0 y = 4 }
	}
}
division_template = {
	name = "Brigada de Infanteria Blindada"

	division_names_group = VEN_ARMOURED_CAV_BRIGADE

	regiments = {
		Arm_Inf_Bat = { x = 0 y = 0 }
		L_arm_Bat = { x = 0 y = 1 }
		L_arm_Bat = { x = 0 y = 2 }
	}

	support = {
		SP_Arty_Battery = { x = 0 y = 0 }
	}
}

units = {
	division = {
		name = "11 Brigada de Blindados \"Brigadier Pedro Ruiz Rondon\""
		location = 8113
		division_template = "Brigadas de Blindados"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "91 Brigada de Infanteria Blindada"
		location = 12143
		division_template = "Brigada de Infanteria Blindada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "13 Brigada de Infanteria"
		location = 4526
		division_template = "Brigada de Infantería de Selva"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "14 Brigada de Infanteria"
		location = 10823
		division_template = "Brigada de Infantería de Selva"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "15 Brigada de Infanteria"
		location = 10767
		division_template = "Brigada de Infantería de Selva"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "16 Brigada de Infanteria"
		location = 10767
		division_template = "Brigada de Infantería de Selva"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "17 Brigada de Infanteria"
		location = 10767
		division_template = "Brigada de Infantería de Selva"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "18 Brigada de Infantería Motorizada"
		location = 10767
		division_template = "Brigada de Infantería Motorizada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "19 Brigada de Infantería Motorizada"
		location = 10767
		division_template = "Brigada de Infantería Motorizada"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "42 Brigada de Paracaidistas"
		location = 7915
		division_template = "Brigada de Paracaidistas"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "12 Brigada de Ranger del Caribe"
		location = 5317
		division_template = "Brigadas de Caribes"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "32 Brigada de Ranger del Caribe"
		location = 10879
		division_template = "Brigadas de Caribes"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
	division = {
		name = "1 Brigada de Infantería de Marina"
		location = 8113
		division_template = "Brigada de Infantería de Marina"
		start_experience_factor = 0.5
		start_equipment_factor = 0.01
	}
}

instant_effect = {
	add_equipment_to_stockpile = {
		type = infantry_weapons3
		amount = 6100
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = infantry_weapons
		amount = 3100
		producer = BEL
	}
	add_equipment_to_stockpile = {
		type = Anti_tank_0
		amount = 450
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = command_control_equipment1 #C3 Equipment
		amount = 900
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Heavy_Anti_tank_0
		amount = 100
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = Anti_Air_0
		amount = 300
		producer = SOV
	}

	add_equipment_to_stockpile = {
		type = mbt_hull_0 #AMX-30
		variant_name = "AMX-30B2"
		amount = 81
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_0
		amount = 36
		producer = FRA
		variant_name = "AMX 13"
	}
	add_equipment_to_stockpile = {
		type = light_tank_hull_1
		amount = 80
		producer = ENG
		variant_name = "FV101 Scorpion"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "AMX-VCI"
		amount = 25
		producer = FRA
		#version_name = "AMX-VCI"
	}
	add_equipment_to_stockpile = {
		type = ifv_hull_1
		variant_name = "Dragoon 300"
		amount = 100
		producer = USA
		#version_name = "Dragoon 300"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "EE-11 Urutu"
		amount = 60
		producer = BRA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #Model 56
		amount = 58
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = artillery_0 #M101A1
		amount = 40
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "Mk F3"
		amount = 12
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = spart_hull_0
		variant_name = "MAR-290"
		amount = 20
		producer = ISR
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0
		variant_name = "TPZ-1 Fuchs"
		amount = 10
		producer = GER
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0 #"Henschel UR-416
		variant_name = "UR-416"
		amount = 20
		producer = GER
		#version_name = "Henschel UR-416"
	}
	add_equipment_to_stockpile = {
		type = apc_hull_0 #Type 6614
		variant_name = "CM6614"
		amount = 20
		producer = ITA
	}
}