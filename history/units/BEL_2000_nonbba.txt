﻿instant_effect = {
	#Aircraft
	add_equipment_to_stockpile = {
		type = L_Strike_fighter2		#Dassault Alpha-Jet
		amount = 29
		producer = FRA
	}
	add_equipment_to_stockpile = {
		type = MR_Fighter2			#F-16A
		amount = 110
		producer = USA
	}
	add_equipment_to_stockpile = {
		type = transport_plane1 #C-130 Super Hercules
		amount = 11
		producer = USA
	}

	### Helicopters
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AgustaWestland AW109
		amount = 46
		producer = ITA
	}
	add_equipment_to_stockpile = {
		type = transport_helicopter1 #AgustaWestland AW109
		amount = 28
		producer = FRA
	}
}