﻿instant_effect = {

	add_equipment_to_stockpile = {
		type = small_plane_airframe_2
		variant_name = "MiG-29 Fulcrum"
		amount = 30
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_fighter_airframe_2
		variant_name = "Su-27"
		amount = 25
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Su-17"
		amount = 26
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_airframe_2
		variant_name = "Su-24M"
		amount = 34
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = medium_plane_cas_airframe_2
		variant_name = "Su-25"
		amount = 20
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-26"
		amount = 14
		producer = UKR
	}
	add_equipment_to_stockpile = {
		type = large_plane_air_transport_airframe_1
		variant_name = "An-12"
		amount = 26
		producer = SOV
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_1
		variant_name = "Aero L-39"
		amount = 14
		producer = CZE
	}
}