﻿instant_effect = {
	### Aircraft ###

	add_equipment_to_stockpile = {
		variant_name = "F-6"
		type = small_plane_strike_airframe_1
		amount = 55
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "F-7 Airguard"
		type = small_plane_strike_airframe_1
		amount = 90
		producer = CHI
	}
	add_equipment_to_stockpile = {
		type = small_plane_strike_airframe_2
		amount = 32
		variant_name = "F-16C Blk 40/42"
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Mirage III"
		type = small_plane_airframe_1
		amount = 70
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Mirage 5"
		type = small_plane_strike_airframe_1
		amount = 52
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Nanchang Q-5"
		type = small_plane_strike_airframe_1
		amount = 22
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "Nanchang Q-5I"
		type = small_plane_strike_airframe_2
		amount = 20
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "C-130 Hercules"
		type = large_plane_air_transport_airframe_1
		amount = 15
		producer = USA
	}
	add_equipment_to_stockpile = {
		variant_name = "Dassault Falcon 20"
		type = large_plane_air_transport_airframe_1
		amount = 3
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "Fokker F27 Friendship"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = HOL
	}
	add_equipment_to_stockpile = {
		variant_name = "Harbin Y-12"
		type = large_plane_air_transport_airframe_1
		amount = 2
		producer = CHI
	}
	add_equipment_to_stockpile = {
		variant_name = "Br.1150 Atlantic"
		type = large_plane_maritime_patrol_airframe_1
		amount = 3
		producer = FRA
	}
	add_equipment_to_stockpile = {
		variant_name = "P-3 Orion"
		type = large_plane_maritime_patrol_airframe_1
		amount = 2
		producer = USA
	}
}